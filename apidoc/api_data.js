define({ "api": [
  // {
  //   "type": "get",
  //   "url": "/data",
  //   "title": "Get associated numbers",
  //   "header": {
  //     "fields": {
  //       "Header": [
  //         {
  //           "group": "Header",
  //           "type": "String",
  //           "optional": false,
  //           "field": "X-API-KEY",
  //           "description": "<p>access key (can be set as a parameter as well)</p>"
  //         }
  //       ]
  //     }
  //   },
  //   "name": "data_get",
  //   "group": "General",
  //   "parameter": {
  //     "fields": {
  //       "Parameter": [
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "number",
  //           "description": "<p>this users number</p>"
  //         }
  //       ]
  //     }
  //   },
  //   "success": {
  //     "fields": {
  //       "Success 200": [
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "error",
  //           "description": "<p>Error description if status is false.</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "status",
  //           "description": "<p>Success of the api call.</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "Object",
  //           "optional": false,
  //           "field": "data",
  //           "description": ""
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "data.count",
  //           "description": "<p>Number of members</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String[]",
  //           "optional": false,
  //           "field": "data.members",
  //           "description": "<p>Member numbers including current number</p>"
  //         }
  //       ]
  //     },
  //     "examples": [
  //       {
  //         "title": "Success-Response:",
  //         "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n      members: [\n          \"0111231234\",\n          \"0111231235\",\n          \"0111231236\",\n      ]\n  }\n}",
  //         "type": "json"
  //       }
  //     ]
  //   },
  //   "version": "0.0.0",
  //   "filename": "application/controllers/API.php",
  //   "groupTitle": "General"
  // },


  // {
  //   "type": "post",
  //   "url": "/data",
  //   "title": "Post data from app.",
  //   "header": {
  //     "fields": {
  //       "Header": [
  //         {
  //           "group": "Header",
  //           "type": "String",
  //           "optional": false,
  //           "field": "X-API-KEY",
  //           "description": "<p>access key (can be set as a parameter as well)</p>"
  //         }
  //       ]
  //     }
  //   },
  //   "name": "data_post",
  //   "group": "General",
  //   "parameter": {
  //     "fields": {
  //       "Parameter": [
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "imei",
  //           "description": "<p>The IMEI number of the phone</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "real_email",
  //           "description": "<p>email address reported by the phone</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "user_email",
  //           "description": "<p>email the customer enters in(could be fake)</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "name",
  //           "description": "<p>persons name (could be fake/blank)</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "number",
  //           "description": "<p>this users number</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "Object[]",
  //           "optional": false,
  //           "field": "contacts",
  //           "description": "<p>List of phone numbers for other contacts</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "gps",
  //           "description": "<p>cordinates from now</p>"
  //         }
  //       ]
  //     }
  //   },
  //   "success": {
  //     "fields": {
  //       "Success 200": [
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "error",
  //           "description": "<p>Error description if status is false.</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "status",
  //           "description": "<p>Success of the api call.</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "Object",
  //           "optional": false,
  //           "field": "data",
  //           "description": ""
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "data.count",
  //           "description": "<p>Number of members</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String[]",
  //           "optional": false,
  //           "field": "data.members",
  //           "description": "<p>Member numbers including current number</p>"
  //         }
  //       ]
  //     },
  //     "examples": [
  //       {
  //         "title": "Success-Response:",
  //         "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n      members: [\n          \"0111231234\",\n          \"0111231235\",\n          \"0111231236\",\n      ]\n  }\n}",
  //         "type": "json"
  //       }
  //     ]
  //   },
  //   "version": "0.0.0",
  //   "filename": "application/controllers/API.php",
  //   "groupTitle": "General"
  // },

  {
    "type": "get",
    "url": "/time",
    "title": "Get Server Time",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "time",
    "group": "General",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.date",
            "description": "<p>The server time in the format Y-m-d H:i:s</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n      error: \"\",\n      status: true,\n      data: {\n          date: \"2017-09-21 07:45:34\"\n      }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },


  // {
  //   "type": "post",
  //   "url": "/data_namepair",
  //   "title": "Post Name Pair Data From App.",
  //   "header": {
  //     "fields": {
  //       "Header": [
  //         {
  //           "group": "Header",
  //           "type": "String",
  //           "optional": false,
  //           "field": "X-API-KEY",
  //           "description": "<p>access key (can be set as a parameter as well)</p>"
  //         }
  //       ]
  //     }
  //   },
  //   "name": "data_namepair_post",
  //   "group": "General",
  //   "parameter": {
  //     "fields": {
  //       "Parameter": [
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "imei",
  //           "description": "<p>The IMEI number of the phone</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "real_email",
  //           "description": "<p>email address reported by the phone</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "user_email",
  //           "description": "<p>email the customer enters in(could be fake)</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "name",
  //           "description": "<p>persons name (could be fake/blank)</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "number",
  //           "description": "<p>this users number</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "Object[]",
  //           "optional": false,
  //           "field": "contacts",
  //           "description": "<p>List of phone numbers for other contacts</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "gps",
  //           "description": "<p>cordinates from now</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "contactPostFormat",
  //           "description": "<p>format contacts string will be sent in (csv , json)</p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "csvDelimiter",
  //           "description": "<p>Used to separate the name - number pair values (a,b ':' c,d) </p>"
  //         },
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "namePairDelimiter",
  //           "description": "<p>Value Separator (Name ',' Number)</p>"
  //         }
  //       ]
  //     }
  //   },
  //   "success": {
  //     "fields": {
  //       "Success 200": [
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "error",
  //           "description": "<p>Error description if status is false.</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "status",
  //           "description": "<p>Success of the api call.</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "Object",
  //           "optional": false,
  //           "field": "data",
  //           "description": ""
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "data.count",
  //           "description": "<p>Number of members</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String[]",
  //           "optional": false,
  //           "field": "data.members",
  //           "description": "<p>Member numbers including current Name, Number and Group</p>"
  //         }
  //       ]
  //     },
  //     "examples": [
  //       {
  //         "title": "Success-Response:",
  //         "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n      members: [\n          \"Name: Terence\",\n          \"Number: 0111231235\",\n          \"Group: gp190\",\n      ]\n  }\n}",
  //         "type": "json"
  //       }
  //     ]
  //   },
  //   "version": "0.0.0",
  //   "filename": "application/controllers/API.php",
  //   "groupTitle": "General"
  // },


  //   /*  Get installed name pair routine*/

  // {
  //   "type": "get",
  //   "url": "/data_installed",
  //   "title": "Get installed name pair info.",
  //   "header": {
  //     "fields": {
  //       "Header": [
  //         {
  //           "group": "Header",
  //           "type": "String",
  //           "optional": false,
  //           "field": "X-API-KEY",
  //           "description": "<p>access key (can be set as a parameter as well)</p>"
  //         }
  //       ]
  //     }
  //   },
  //   "name": "data_installed_get",
  //   "group": "General",
  //   "parameter": {
  //     "fields": {
  //       "Parameter": [
  //         {
  //           "group": "Parameter",
  //           "type": "String",
  //           "optional": false,
  //           "field": "number",
  //           "description": "<p>The number of the requesting phone</p>"
  //         }
  //       ]
  //     }
  //   },
  //   "success": {
  //     "fields": {
  //       "Success 200": [
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "error",
  //           "description": "<p>Error description if status is false.</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "status",
  //           "description": "<p>Success of the api call.</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "Object",
  //           "optional": false,
  //           "field": "data",
  //           "description": ""
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String",
  //           "optional": false,
  //           "field": "data.count",
  //           "description": "<p>Number of members</p>"
  //         },
  //         {
  //           "group": "Success 200",
  //           "type": "String[]",
  //           "optional": false,
  //           "field": "data.members",
  //           "description": "<p>Member numbers including current number and name and installed status</p>"
  //         }
  //       ]
  //     },
  //     "examples": [
  //       {
  //         "title": "Success-Response:",
  //         "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n      members: [\n          \"Name: Terence\",\n          \"Number: 0111231235\",\n          \"Installed: Yes\",\n      ]\n  }\n}",
  //         "type": "json"
  //       }
  //     ]
  //   },
  //   "version": "0.0.0",
  //   "filename": "application/controllers/API.php",
  //   "groupTitle": "General"
  // }

  /*  SETUP INITIAL GROUP INFORMATION FROM APP INSTALL*/
  {
    "type": "post",
    "url": "/setup_data",
    "title": "1. Setup Post From App.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "setup_data_post",
    "group": "General",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "imei",
            "description": "<p>The IMEI number of the phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "real_email",
            "description": "<p>email address reported by the phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_email",
            "description": "<p>email the customer enters in(could be fake)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>persons name (could be fake/blank)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "number",
            "description": "<p>this users number</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "contacts",
            "description": "<p>List of phone numbers for other contacts</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gps",
            "description": "<p>cordinates from now</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },          
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.count",
            "description": "<p>Number of members</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.groupID",
            "description": "<p>ID of new group created</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.groupName",
            "description": "<p>Name of new group created</p>"
          },                    
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data.members",
            "description": "<p>Member numbers including current number</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n  GroupID: 1,\n GroupName: GP1,\n    members: [\n       Name  \"Steve Jobs\" \n       Number  \"0112223333\" \n       GroupID  \"1\" \n       GroupName  \"GP1\" \n       Installed \"N\"           \n]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },


  /*  GET GROUP DETAILS */
    {
    "type": "get",
    "url": "/groupDetails",
    "title": "2. Get Group Details.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "data_post",
    "group": "General",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "group_id",
            "description": "<p>The ID of requested Group</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "group_name",
            "description": "<p>The name of requested Group</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": true,
            "field": "number",
            "description": "<p>Admin phone number for requested group</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },          
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.count",
            "description": "<p>Number of members</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.groupID",
            "description": "<p>ID of new group created</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.groupName",
            "description": "<p>Name of new group created</p>"
          },                    
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data.members",
            "description": "<p>Member info including installed status</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n  GroupID: 1,\n GroupName: GP1,\n    members: [\n       Name  \"Steve Jobs\" \n       Number  \"0112223333\" \n       GroupID  \"1\" \n       GroupName  \"GP1\" \n       Installed \"N\"           \n]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },


  /*  GET GROUP MEMBERSHIP DETAILS */
    {
    "type": "get",
    "url": "/groupMembershipDetails",
    "title": "3. Get Group Membership Details.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "groupMembershipDetails_get",
    "group": "General",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "memberID",
            "description": "<p>The memberID of requesting User</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "memberName",
            "description": "<p>The Name of requesting user</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "memberNumber",
            "description": "<p>The Number of requesting user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },          
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.count",
            "description": "<p>Number of groups</p>"
          },                  
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data.members",
            "description": "<p>Group Membership Info</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n  groups: [\n          groupID \"1\",\n          groupName\"GP1\",\n          memberCount \"25\",\n      ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },
  
  /*  GET COMMUNITY GROUPS DETAILS */
    {
    "type": "get",
    "url": "/communityGroups",
    "title": "4. Get Community Group.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "communityGroups_get",
    "group": "General",
    // "parameter": {
    //   "fields": {
    //     "Parameter": [
    //       {
    //         "group": "Parameter",
    //         "type": "String",
    //         "optional": true,
    //         "field": "memberID",
    //         "description": "<p>The memberID of requesting User</p>"
    //       },

    //       {
    //         "group": "Parameter",
    //         "type": "String",
    //         "optional": true,
    //         "field": "memberName",
    //         "description": "<p>The Name of requesting user</p>"
    //       },

    //       {
    //         "group": "Parameter",
    //         "type": "String",
    //         "optional": true,
    //         "field": "memberNumber",
    //         "description": "<p>The Number of requesting user</p>"
    //       }
    //     ]
    //   }
    // },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },          
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.count",
            "description": "<p>Number of groups</p>"
          },                 
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data.members",
            "description": "<p>Group IDs and Names</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n  groups: [\n          groupID \"1\",\n          groupName\"GP1\",\n          memberCount \"25\",\n      ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },

  /*  CHANGE GROUP NAME */
    {
    "type": "post",
    "url": "/changeGroupName",
    "title": "5. Change Group Name.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "changeGroupName_post",
    "group": "General",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memberID",
            "description": "<p>The Admin of Group memberID</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memberName",
            "description": "<p>The memberName of admin</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "groupID",
            "description": "<p>The groupID of group to be changed</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          }
          // },
          // {
          //   "group": "Success 200",
          //   "type": "Object",
          //   "optional": false,
          //   "field": "data",
          //   "description": ""
          // },          
          // {
          //   "group": "Success 200",
          //   "type": "String",
          //   "optional": false,
          //   "field": "data.count",
          //   "description": "<p>Number of groups</p>"
          // },                 
          // {
          //   "group": "Success 200",
          //   "type": "String[]",
          //   "optional": false,
          //   "field": "data.members",
          //   "description": "<p>Group IDs and Names</p>"
          // }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },


  /*  GROUP MAINTENANCE */
    {
    "type": "post",
    "url": "/groupMaintenance",
    "title": "6. Group Maintenance.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "groupMaintenance_post",
    "group": "General",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memberID",
            "description": "<p>The Admin of Group memberID</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memberName",
            "description": "<p>The memberName of admin</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "members[]",
            "description": "<p>Array of members with id and what change to make - id and suspend - id and delete etc</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          },
           {
             "group": "Success 200",
             "type": "Object",
             "optional": false,
             "field": "data",
             "description": ""
           },          
           {
             "group": "Success 200",
             "type": "String",
             "optional": false,
             "field": "data.count",
             "description": "<p>Number of members</p>"
           },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.groupID",
            "description": "<p>ID of returned group</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.groupName",
            "description": "<p>Name of new group returned</p>"
          },                  
           {
             "group": "Success 200",
             "type": "String[]",
             "optional": false,
             "field": "data.members",
             "description": "<p>memberIDs and Names and Status</p>"
           }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n       GroupID: 1,\n       GroupName: GP1,\n    members: [\n          memberID \"123\",\n          memberName\"Tom Jones\",\n          status \"active\",\n      ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },

  /*  GROUP ADD MEMBER */
    {
    "type": "post",
    "url": "/groupAddmember",
    "title": "7. Add Member.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "groupAddmember_post",
    "group": "General",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memberID",
            "description": "<p>The Admin of Group memberID</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memberName",
            "description": "<p>The memberName of admin</p>"
          },

          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "contact",
            "description": "<p>New Member Details</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          },
           {
             "group": "Success 200",
             "type": "Object",
             "optional": false,
             "field": "data",
             "description": ""
           },          
           {
             "group": "Success 200",
             "type": "String",
             "optional": false,
             "field": "data.count",
             "description": "<p>Number of members</p>"
           },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.groupID",
            "description": "<p>ID of returned group</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.groupName",
            "description": "<p>Name of new group returned</p>"
          },                  
           {
             "group": "Success 200",
             "type": "String[]",
             "optional": false,
             "field": "data.members",
             "description": "<p>memberIDs and Names and Status</p>"
           }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n       GroupID: 1,\n       GroupName: GP1,\n    members: [\n          memberID \"123\",\n          memberName\"Tom Jones\",\n          status \"active\",\n      ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },

  /*  SEARCH */
    {
    "type": "get",
    "url": "/search",
    "title": "8. Search Member / Group List.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "search_get",
    "group": "General",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "searchString",
            "description": "<p>Name of group - member to search for</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          },
           {
             "group": "Success 200",
             "type": "Object",
             "optional": false,
             "field": "data",
             "description": ""
           },          
           {
             "group": "Success 200",
             "type": "String",
             "optional": false,
             "field": "data.count",
             "description": "<p>Number of members</p>"
           },                 
           {
             "group": "Success 200",
             "type": "String[]",
             "optional": false,
             "field": "data.result",
             "description": "<p>type and ID and Name and Status</p>"
           }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  data: {\n      count: 1,\n       result: [\n          itemtype \"member\",\n          itemid \"1\",\n          itemname \"Neil Diamond\",\n      ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },


  /*  JOIN REQUESTS */
    {
    "type": "post",
    "url": "/joinRequests",
    "title": "9. Join Requests.",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-API-KEY",
            "description": "<p>access key (can be set as a parameter as well)</p>"
          }
        ]
      }
    },
    "name": "joinRequests_post",
    "group": "General",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "adminID",
            "description": "<p>ID of member performing request</p>"
          },{
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "joinType",
            "description": "<p>Invite or Request Type</p>"
          },{
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "groupID",
            "description": "<p>group ID to join</p>"
          },
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Error description if status is false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Success of the api call.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  error: \"\",\n  status: true,\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "application/controllers/API.php",
    "groupTitle": "General"
  },



] });
