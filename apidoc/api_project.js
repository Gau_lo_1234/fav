define({
  "name": "FA API Documentation",
  "version": "0.0.0",
  "description": "FA apiDoc for RestAPI",
  "title": "FA apiDoc for RestAPI",
  "url": "http://rfnet.co.za/fa/API",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-10-26T13:25:20.834Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
