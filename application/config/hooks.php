<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
| http://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['pre_system'][] = array(
        'class'    => 'PHPFatalError',
        'function' => 'setHandler',
        'filename' => 'PHPFatalError.php',
        'filepath' => 'hooks'
);
$hook['post_controller_constructor'][] = array(
        'class'    => 'ProfilerEnabler',
        'function' => 'EnableProfiler',
        'filename' => 'hooks.classes.php',
        'filepath' => 'hooks',
        'params'   => array()
);
$hook['post_controller_constructor'][] = array(
        'class'    => 'Bootstraper',
        'function' => 'LoadSettings',
        'filename' => 'hooks.classes.php',
        'filepath' => 'hooks',
        'params'   => array()
);
$hook['pre_controller'][] = array(
        'class'    => 'Bootstraper',
        'function' => 'LoadModuleHooks',
        'filename' => 'hooks.classes.php',
        'filepath' => 'hooks',
        'params'   => array()
);

$hook['post_controller'][] = array(
        'class'    => 'Bootstraper',
        'function' => 'ReferrerTrack',
        'filename' => 'hooks.classes.php',
        'filepath' => 'hooks',
        'params'   => array()
);


# PERFORM-TWEAK: Disable this to make your system slightly quicker
// $hook['pre_system'][] = array(
//         'class'    => 'InstallerChecker',
//         'function' => 'CheckInstalled',
//         'filename' => 'hooks.classes.php',
//         'filepath' => 'hooks',
//         'params'   => array()
// );
// $hook['post_controller_constructor'][] = array(
//         'class'    => 'InstallerChecker',
//         'function' => 'CheckVersion',
//         'filename' => 'hooks.classes.php',
//         'filepath' => 'hooks',
//         'params'   => array()
// );





/* End of file hooks.php */
/* Location: ./application/config/hooks.php */