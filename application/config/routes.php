<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "dashboard";
$route['addNew'] = "dashboard/addNew";
$route['settings'] = "dashboard/settings";
$route['users'] = "dashboard/users";
$route['deleteUser'] = "dashboard/deleteUser";
$route['logout'] = 'auth/logout';
$route['users/(:num)'] = "dashboard/users/$1";
$route['distributors'] = "dashboard/distributors";
$route['distributors/(:num)'] = "dashboard/distributors/$1";
$route['editDist/(:num)'] = "dashboard/editDist/$1";
$route['devices/(:num)'] = "dashboard/devices/$1";
$route['addNew/(:num)'] = "dashboard/addNew/$1";
$route['editOld/(:num)'] = "dashboard/editOld/$1";
$route['reports'] = "dashboard/reports";
$route['editDevice'] = "dashboard/editDevice";
$route['devices'] = "dashboard/devices";
$route['accounts'] = "dashboard/accounts";
$route['addNewUser'] = "dashboard/addNewUser";
$route['editInfo'] = "dashboard/editInfo";
$route['404_override'] = 'dashboard/pageNotFound';
$route['terms-and-conditions'] = "content/terms";
$route['checkEmailExists'] = "user/checkEmailExists";
$route['login-history'] = "dashboard/loginHistoy";
$route['login-history/(:num)'] = "dashboard/loginHistoy/$1";
$route['login-history/(:num)/(:num)'] = "user/loginHistoy/$1/$2";


/* End of file routes.php */
/* Location: ./application/config/routes.php */
