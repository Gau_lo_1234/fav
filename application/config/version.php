<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

# Place to configure where the version checking and links should go!

$config['check_url'] = 'http://www.liveguarding.com/download/check';
$config['download_url'] = 'http://www.liveguarding.com/download';
