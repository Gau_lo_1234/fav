<?php defined('BASEPATH') OR exit('No direct script access allowed');


ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * Web-services API
 *
 * @copyright Online Guarding, 2017
 * @package api.onlineguarding
 * @category API
 * @version 1.0
 * @link http://www.onlineguarding.co.za
 *
 * @access  public
 * @return  void
 *   ACTIVE TABLES:  data_capture    - Stores raw post data
 *                :  groupLinks      - member group associations
 *                :  members         - members generated through installs and/or contact lists
 *                :  groups          - private and public group listing
 *                                   - default public group is community group GP1
 *
 *
 *
 */


require APPPATH . '/libraries/REST_Controller.php';

class API extends REST_Controller {


    /**
     * Default values needed to generate API keys and restrict access to resources.
     * @var array
     */
    protected $methods = [
        'key_put' => ['level' => 10, 'limit' => 100],
        'key_delete' => ['level' => 10],
        'level_post' => ['level' => 10],
        'suspend_post' => ['level' => 10],
        'regenerate_post' => ['level' => 10],
        // 'login_post' => ['level' => 1, 'limit' => 10000],
        'time_get' => ['level' => 0],
        'data_post' => ['level' => 1],
        'data_get' => ['level' => 1],
    ];

   
    /**
     * The default value for timeouts of OTP keys.
     * @var int
     */
    private $timeout = 30;
    private $debug_output = true;
    private $bypass_pastel = false;


    /** @ignore Setting test data. */
    public function __construct() {
        parent::__construct();
        $this->load->model('m_api');
        
    }

    // 0912f582e3fe2ac92bd6c7676e7227a1ffbcd868

    /**
     * Create an API key
     *
     * Insert a key into the database.
     *
     * @access public
     * @example Status
     * @example &nbsp;&nbsp;&nbsp;&nbsp;Message "Successfully created the API key"
     * @example &nbsp;&nbsp;&nbsp;&nbsp;StatusCode 201
     * @example &nbsp;&nbsp;&nbsp;&nbsp;Data
     * @example &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Key "8aedf1fb010fc5a8b0614102488d665715daf9cb"
     * @return JSON
     * @author Kobus Myburgh
     * @see demo forms at api/rest_forms/api_index
     * @since 1.0
     * @internal This method is a POST method, with optional parameters (int) level and (int) ignore_limits, and required parameter (string) X-API-KEY.
     * @todo Create management interfaces within SM.
     */
    public function key_put() {
        $key = $this->m_api->generate_key();
        $level = $this->put('level') ? $this->put('level') : 1;
        $ignore_limits = $this->put('ignore_limits') ? $this->put('ignore_limits') : 1;
        if ($this->m_api->insert_key($key, array('level' => $level, 'ignore_limits' => $ignore_limits))) {
            $res['status'] = true;
            $res['error'] = 'Successfully created the API key';
            $res['data']['key'] = $key;
            $this->response($res, REST_Controller::HTTP_CREATED);
        } else {
            $res['status'] = false;
            $res['error'] = 'Could not save the key.';
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete an API key
     *
     * Remove a key from the database to stop it working.
     *
     * @access  public
     * @example Status
     * @example &nbsp;&nbsp;&nbsp;&nbsp;Message "API Key was deleted."
     * @example &nbsp;&nbsp;&nbsp;&nbsp;StatusCode 200
     * @return JSON
     * @author Kobus Myburgh
     * @see demo forms at api/rest_forms/api_index
     * @since 1.0
     * @internal This method is a DELETE method, with required parameter (string) key, and (string) X-API-KEY.
     * @todo Create management interfaces within SM.
     */
    public function key_delete() {
        $key = $this->delete('key');
        if (!$this->m_api->key_exists($key)) {
            $res['status'] = false;
            $res['error'] = 'Invalid API key';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        if (!$this->m_api->delete_key($key)) {
            $res['status'] = false;
            $res['error'] = 'API key could not be deleted.';
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $res['status'] = true;
            $res['error'] = 'API Key was deleted.';
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }

    /**
     * Update an API key
     *
     * Change the level of a key - lower level keys have less access that higher level keys. Usage is left for implementation of key usage, e.g., if ($key->level > 5) { ... }
     *
     * @access  public
     * @example Status
     * @example &nbsp;&nbsp;&nbsp;&nbsp;Message "API Key was updated."
     * @example &nbsp;&nbsp;&nbsp;&nbsp;StatusCode 200
     * @example Data
     * @example &nbsp;&nbsp;&nbsp;&nbsp;Level 2
     * @return JSON
     * @author Kobus Myburgh
     * @see demo forms at api/rest_forms/api_index
     * @since 1.0
     * @internal This method is a PUT method, with required parameters (string) key, (int) level, and (string) X-API-KEY.
     * @todo Create management interfaces within SM.
     */
    public function level_put() {
        $key = $this->put('key');
        $new_level = $this->put('level');
        if (!$this->m_api->key_exists($key)) {
            $res['status'] = false;
            $res['error'] = 'Invalid API key';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        if ($this->m_api->update_key($key, array('level' => $new_level))) {
            $res['Data']['Level'] = (int)$new_level;
            $res['status'] = true;
            $res['error'] = 'API Key was updated.';
            $this->response($res, REST_Controller::HTTP_OK);
        } else {
            $res['status'] = false;
            $res['error'] = 'Could not update the key level.';
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Suspend an API key
     *
     * Suspends a key from being usable by changing it's level to 0. Keys with level 0 are automatically prevented any access to resources requiring access.
     *
     * @access  public
     * @example Status
     * @example &nbsp;&nbsp;&nbsp;&nbsp;Message "API Key was suspended."
     * @example &nbsp;&nbsp;&nbsp;&nbsp;StatusCode 200
     * @return JSON
     * @author Kobus Myburgh
     * @see demo forms at api/rest_forms/api_index
     * @since 1.0
     * @internal This method is a POST method, with required parameters (string) key, and (string) X-API-KEY.
     * @todo Create management interfaces within SM.
     */
    public function suspend_post() {
        $key = $this->post('key');
        if (!$this->m_api->key_exists($key)) {
            $res['status'] = false;
            $res['error'] = 'Invalid API key';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        if ($this->m_api->update_key($key, array('level' => 0))) {
            $res['status'] = true;
            $res['error'] = 'API Key was suspended.';
            $this->response($res, REST_Controller::HTTP_OK);
        } else {
            $res['status'] = false;
            $res['error'] = 'Could not suspend the API key.';
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Regenerate Key
     *
     * Removes an old key and creates a new key with the same privileges as the old one.
     *
     * @access  public
     * @example Status
     * @example &nbsp;&nbsp;&nbsp;&nbsp;Message "API Key was regenerated."
     * @example &nbsp;&nbsp;&nbsp;&nbsp;StatusCode  201
     * @example Data
     * @example &nbsp;&nbsp;&nbsp;&nbsp;key "bf11d50d4c123844a7700dccb7beb26de8c0aa6e"
     * @return JSON
     * @author Kobus Myburgh
     * @see demo forms at api/rest_forms/api_index
     * @since 1.0
     * @internal This method is a POST method, with required parameters (string) key, and (string) X-API-KEY.
     * @todo Create management interfaces within SM.
     */
    public function regenerate_post() {
        $old_key = $this->post('key');
        $key_details = $this->m_api->get_key($old_key);
        if (!$key_details) {
            $res['status'] = false;
            $res['error'] = 'Invalid API key';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        $new_key = $this->m_api->generate_key();
        if ($this->m_api->insert_key($new_key, array('level' => $key_details->level, 'ignore_limits' => $key_details->ignore_limits))) {
            $this->m_api->update_key($old_key, array('level' => 0));
            $res['status'] = true;
            $res['error'] = 'API Key was regenerated.';
            $res['data']['key'] = $new_key;
            $this->response($res, REST_Controller::HTTP_CREATED);
        } else {
            $res['status'] = false;
            $res['error'] = 'Could not save the API key.';
            $this->response($res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * time_get
     *
     * @api {get} /time Get Server Time
     * @apiHeader {String} X-API-KEY access key (can be set as a parameter as well)
     * @apiName time
     * @apiGroup General
     *
     * @apiSuccess {String} error Error description if status is false.
     * @apiSuccess {String} status  Success of the api call.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.date  The server time in the format Y-m-d H:i:s
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           error: "",
     *           status: true,
     *           data: {
     *               date: "2017-09-21 07:45:34"
     *           }
     *     }
     *
     */
    function time_get() {
        // Check Parameters
        // $api_key = $this->get('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        // }

        $gmt_date_time = date('Y-m-d H:i:s');

        $res['status'] = true;
        $res['error'] = '';
        $res['data']['date'] = $gmt_date_time;
        $this->response($res, REST_Controller::HTTP_OK);
    }


    /**
     * setup_data_post
     *
     * @api {post} /data Post data from app.
     * @apiHeader {String} X-API-KEY access key (can be set as a parameter as well)
     * @apiName data_post
     * @apiGroup General
     *
     * @apiParam {String} imei The IMEI number of the phone
     * @apiParam {String} real_email email address reported by the phone
     * @apiParam {String} user_email email the customer enters in(could be fake)
     * @apiParam {String} name persons name (could be fake/blank)
     * @apiParam {String} number this users number
     * @apiParam {Object[]} contacts  List of phone numbers for other contacts
     * @apiParam {String} gps cordinates from now
     *
     * @apiSuccess {String} error Error description if status is false.
     * @apiSuccess {String} status  Success of the api call.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.count  Number of members
     * @apiSuccess {String[]} data.members  Member numbers including current number
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       error: "",
     *       status: true,
     *       data: {
     *           count: 1,
     *           members: [
     *               "0111231234",
     *               "0111231235",
     *               "0111231236",
     *           ]
     *       }
     *     }
     */
    function setup_data_post(){


        // Check Parameters
        // $api_key = $this->post('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        // }
        

        //util helper function to display formatted array data cleanly
        // dump($_POST);

        // exit();

        $imei = $this->post('imei');
        $real_email = $this->post('real_email');
        $user_email = $this->post('user_email');
        $name = $this->post('name');       
        $number = $this->post('number'); 
        $contacts = $this->post('contacts');      
        $postFormat = $this->post('contactPostFormat');
        $gps = $this->post('gps');




        //clean number input
        $number = preg_replace("/[^0-9]/", "", str_replace("+27", "0", $number));

        
        if(isset($_POST['csvDelimiter'])){
            $csvDelimiter = $_POST['csvDelimiter'];
        }

        if(isset($_POST['namePairDelimiter'])){
            $namePairDelimiter = $_POST['namePairDelimiter'];
        }


        if (!$imei) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: imei';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$real_email && !$user_email ) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: real_email OR user_email';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$name) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: name';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$number) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: number';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }

        if(!$gps){
            $gps = 'No gps co-ordinates supplied';
        }       
        
        // Load models
        $this->load->model('m_data_captured');

 

       if(in_array(strtolower($_POST['contactPostFormat']),array('csv','json',''))){
         


            switch(strtolower($postFormat)){
                case 'csv':
                        if(!isset($csvDelimiter) || trim(strlen($csvDelimiter)) != 1){
                            $res['status'] = false;
                            $res['error'] = 'Missing - Incorrect CSV Delimiter value';
                            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
                        };

                        if(!isset($namePairDelimiter) || trim(strlen($namePairDelimiter)) !=1){
                            $res['status'] = false;
                            $res['error'] = 'Missing - Incorrect Name Pair Value Delimiter value';
                            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);                               
                        };

                        $data['type'] = 'csv';

                        $array = explode($namePairDelimiter,$contacts);
                        $contactsCleaned= array();

                        foreach($array as $key => $val){


                           $contactCleaned[] = explode($csvDelimiter,$val);
                        }
                        

                        $contacts = $contactCleaned;

                    break;

                case 'json':

                        /*  JSON EXAMPLE DATA FORMAT

                            [
                                { "Name":"Terence", "Number":"079 220 3134" },
                                { "Name":"Marian", "Number":"074 886 4633" },
                                { "Name":"Cameron", "Number":"011 656 6565" }
                            ]

                        */

                        json_decode($contacts);
                        if(!json_last_error() == JSON_ERROR_NONE){
                            $res['status'] = false;
                            $res['error'] = 'Invalid Contact Data';
                            $this->response($res, REST_Controller::HTTP_BAD_REQUEST); 
                        }

                        $temps = json_decode($contacts);

  
                        foreach($temps as $temp){
                            
                                $contactCleaned[] = array($temp->name,$temp->Number);
                        }

                        $contacts = $contactCleaned;

                    break;
                    
                default:
                        /* rewire contact list to include blank name values */
                    
                    if (!is_array($contacts)) {

                        $contacts = explode(',',preg_replace('/\[|\]|"|\'/', '', $contacts));
                    }
            
                    $contactlist = array();
                    foreach($contacts as $contact){
                        if($contact){
                             $contactlist[] = array("",$contact);
                        }
                    }
                    
                        $contacts = $contactlist;
                break;
            }

        }

        
        // Just store the raw request....
        $request_id = $this->m_data_captured->store($imei, $real_email, $user_email, $name, $number, $contacts, $gps);


        //Is this user registered to the system
        if(!$isMember = $this->m_data_captured->check_member_status($number)){


            //register user to system
           $isMember = $this->m_data_captured->add_member($name, $number,$imei, $real_email, $user_email, $gps,true);
        };


        if($isMember){
            //check member info on system against new setup info
            if($this->m_data_captured->get_member_info($isMember)->memberName == ''){
                //update the member info to reflect new info just provided from install/setup script
                $this->m_data_captured->update_member_info($isMember,$imei, $real_email, $user_email, $name, $number, $gps,true);
            }
            
        }


        //yes they are because we either returned or created a new member profile
        if($isMember){
         // Is this person in a home group???
         $group_id = $this->m_data_captured->check_home_group($isMember);           
        }


        if (!$group_id) {
            // Create a new group. with a friendly group name that makes sense to the user:
            // GP1 does not tom clancys home group does
            $group_id = $this->m_data_captured->create_home_group($request_id,$isMember,"HOME");

            $this->m_data_captured->add_group_link($group_id, $isMember);
        }

        if (!empty($contacts)) {

            //remove old group links and create new group links
            $this->m_data_captured->remove_group_links($group_id,$isMember);
            //link numbers to group.

            // dump($contacts);
            // exit();
            foreach($contacts as $contact) {
                if ($contact) { // don't store empty ones idiot
                    //check if registered on system and create as needed before linking
                    $this->m_data_captured->add_group_name_number($group_id, $contact);

                }
            }
        }


        // Get numbers in group

        $members = $this->m_data_captured->get_group_names_numbers($group_id);
        

        $res['status'] = true;
        $res['error'] = '';
        $res['result'] = 'success';
        $res['data']['count'] = !empty($members) ? count($members) : '0';
        $res['data']['groupName'] = $members[0]['GroupName'];
        $res['data']['groupID'] = $group_id;
        $res['data']['members'] = !empty($members) ? $members : array();
        

        $this->response($res, REST_Controller::HTTP_OK);  
    }


    function groupDetails_get(){

        // Check Parameters
        // $api_key = $this->get('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        //var_dump($_GET);

         $groupName = $_GET['group_name'];
         $number = $_GET['number'];
         $groupID = $_GET['group_id'];


        if (!$groupName && !$number && ! $groupID) {
            $res['status'] = false;
            $res['error'] = 'No Lookup Parameters Received';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }


         // Load models
        $this->load->model('m_data_captured');
        $members = array();

        if($groupID){

            // great groupid supplied so get all members from the grouplinks table
            $members = $this->m_data_captured->get_groupMembers("byID",$groupID);

        }elseif($groupName){

            // great groupName supplied so get id from groups table and all members from the grouplinks table
            $members = $this->m_data_captured->get_groupMembers("byName",$groupName);
           
        }elseif($number){
            //number supplied so check if home group exists else cancel
            $members = $this->m_data_captured->get_groupMembers("byNumber",$number);
        }

        if(is_array($members) && !empty($members)){
            $res['status'] = true;
            $res['error'] = '';
            $res['result'] = 'success';
            $res['data']['count'] = !empty($members['members']) ? count($members['members']) : '0';
            $res['data']['groupID'] = $members['groupID'];
            $res['data']['members'] = !empty($members['members']) ? $members['members'] : array();
            $this->response($res, REST_Controller::HTTP_OK);  
        }else{
            $res['status'] = false;
            $res['error'] = 'Invalid Group Data';
            $res['result'] = 'failed';
            $res['data']['reason'] = "No group information found";                            
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);             
           
        }
    }


    /* RETURNS LIST OF GROUPS THIS MEMBER IS A PART OF*/
    function groupMembershipDetails_get(){
        // Check Parameters
        // $api_key = $this->get('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        //var_dump($_GET);

         $memberID = $_GET['member_id'];
         $memberName = $_GET['member_name'];
         $memberNumber = $_GET['member_number'];


        if (!$memberName && !$memberNumber && ! $memberID) {
            $res['status'] = false;
            $res['error'] = 'No Lookup Parameters Received';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }


         // Load models
        $this->load->model('m_data_captured');
        $members = array();

        if($memberID){

            // great groupid supplied so get all members from the grouplinks table
            $groups = $this->m_data_captured->get_group_by_assoc("byID",$memberID);
            //$groupMembers = $this->m_data_captured->get_groupMembers_by_assoc("byID",$memberID);
        }elseif($memberName){

            // great groupName supplied so get id from groups table and all members from the grouplinks table
            //$groups = $this->m_data_captured->get_groups("byName",$memberName);
             $groups = $this->m_data_captured->get_group_by_assoc("byName",$memberName);
            //$groupMembers = $this->m_data_captured->get_groupMembers_by_assoc("byName",$memberName);          
        }elseif($memberNumber){
            //number supplied so check if home group exists else cancel
            //$groups = $this->m_data_captured->get_groups("byNumber",$memberNumber);
            $groups = $this->m_data_captured->get_group_by_assoc("byNumber",$memberNumber);
            //$groupMembers = $this->m_data_captured->get_groupMembers_by_assoc("byNumber",$memberNumber);            
        }


        $res['status'] = true;
        $res['error'] = '';
        $res['data']['count'] = !empty($groups) ? count($groups) : '0';
        $res['data']['groupinfo'] = !empty($groups) ? $groups : array();
        $this->response($res, REST_Controller::HTTP_OK);     
    }

    function communityGroups_get(){
        // Check Parameters
        // $api_key = $this->get('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        //var_dump($_GET);

            $this->load->model('m_data_captured');

            //number supplied so check if home group exists else cancel
            $groups = $this->m_data_captured->get_communityGroups("Active");



        $res['status'] = true;
        $res['error'] = '';
        $res['data']['count'] = !empty($groups) ? count($groups) : '0';
        $res['data']['groups'] = !empty($groups) ? $groups : array();
        $this->response($res, REST_Controller::HTTP_OK);  

    }


    function changeGroupName_post(){

        // Check Parameters
        // $api_key = $this->get('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        //var_dump($_GET);


         $data['memberID'] = isset($_POST['member_id'])?$_POST['member_id']:null;
         $data['groupID'] = isset($_POST['group_id'])?$_POST['group_id']:null;
         $data['new_group_name'] = isset($_POST['new_group_name'])?$_POST['new_group_name']:null;


         //check if required member info has been passed
        if (!$data['memberID'] ){
            $res['status'] = false;
            $res['error'] = 'No Lookup Parameters Received';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }

        //check if required group identifier has been passed
        if (!$data['groupID']) {
            $res['status'] = false;
            $res['error'] = 'No Lookup Parameters Received';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }

        if (!$data['new_group_name']) {
            $res['status'] = false;
            $res['error'] = 'Invalid Group Name';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
         // Load models
        $this->load->model('m_data_captured');


        if($this->m_data_captured->changeGroupName($data)){
            $res['status'] = true;
            $res['error'] = '';
            $this->response($res, REST_Controller::HTTP_OK); 
        }else{
            $res['status'] = false;
            $res['error'] = 'No Lookup Parameters Received';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
 

    }

    function groupMaintenance_post(){

    }


    function groupAddmember_post(){

    }

    function joinRequests_post(){

    }

    /**
     * data_post
     *
     * @api {post} /data Post data from app.
     * @apiHeader {String} X-API-KEY access key (can be set as a parameter as well)
     * @apiName data_post
     * @apiGroup General
     *
     * @apiParam {String} imei The IMEI number of the phone
     * @apiParam {String} real_email email address reported by the phone
     * @apiParam {String} user_email email the customer enters in(could be fake)
     * @apiParam {String} name persons name (could be fake/blank)
     * @apiParam {String} number this users number
     * @apiParam {Object[]} contacts  List of phone numbers for other contacts
     * @apiParam {String} gps cordinates from now
     *
     * @apiSuccess {String} error Error description if status is false.
     * @apiSuccess {String} status  Success of the api call.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.count  Number of members
     * @apiSuccess {String[]} data.members  Member numbers including current number
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       error: "",
     *       status: true,
     *       data: {
     *           count: 1,
     *           members: [
     *               "0111231234",
     *               "0111231235",
     *               "0111231236",
     *           ]
     *       }
     *     }
     */
    
    function data_post() {

        // Check Parameters
        // $api_key = $this->post('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        // }
        
        

        $imei = $this->post('imei');
        $real_email = $this->post('real_email');
        $user_email = $this->post('user_email');
        $name = $this->post('name');       
        $number = $this->post('number');       
        
        if (!$imei) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: imei';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$real_email && !$user_email ) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: real_email OR user_email';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$name) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: name';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$number) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: number';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }

        

        
        // Load models
        $this->load->model('m_data_captured');

        // The brains
        $contacts = $this->post('contacts');
        $gps = $this->post('gps');
        
        // Just store the request....
        $request_id = $this->m_data_captured->store($imei, $real_email, $user_email, $name, $number, $contacts, $gps);

        // Is this person in a group???
        $group_id = $this->m_data_captured->check_group($number);
        
        if (!$group_id) {
            // Create a new group.
            $group_id = $this->m_data_captured->create_group($request_id);
            $this->m_data_captured->add_group_number($group_id, $number);
        }
        
        if (!empty($contacts)) {
            if (!is_array($contacts)) {
                $contacts = preg_replace('/\[|\]|"|\'/', '', $contacts);
                $contacts = explode(',', $contacts);
            }

            //link numbers to group.
            foreach($contacts as $contact) {
                if ($contact) { // don't store empty ones idiot
                    $this->m_data_captured->add_group_number($group_id, $contact);
                }
            }
        }

        // Get numbers in group
        $members = $this->m_data_captured->get_group_numbers($group_id);
        //$members = $this->m_data_captured->get_group_names_numbers($group_id);

        $res['status'] = true;
        $res['error'] = '';
        $res['data']['count'] = !empty($members) ? count($members) : '0';
        $res['data']['members'] = !empty($members) ? $members : array();
        
        //print_r($res['data']);
        //exit();
        $this->response($res, REST_Controller::HTTP_OK);
        
    }

    /**
     * data_namepair_post
     *
     * @api {post} /data Post data from app.
     * @apiHeader {String} X-API-KEY access key (can be set as a parameter as well)
     * @apiName data_namepair_post
     * @apiGroup General
     *
     * @apiParam {String} imei The IMEI number of the phone
     * @apiParam {String} real_email email address reported by the phone
     * @apiParam {String} user_email email the customer enters in(could be fake)
     * @apiParam {String} name persons name (could be fake/blank)
     * @apiParam {String} number this users number
     * @apiParam {Object[]} contacts  List of phone numbers for other contacts
     * @apiParam {String} gps cordinates from now
     * @apiParam {String} flag contact posting format
     * @apiParam {String} csvDelimiter for name-contact value pair
     * @apiParam {String} recordDelimiter for name - contact value pair seperation
     * @apiSuccess {String} error Error description if status is false.
     * @apiSuccess {String} status  Success of the api call.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.count  Number of members
     * @apiSuccess {String[]} data.members  Member numbers including current number
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       error: "",
     *       status: true,
     *       data: {
     *           count: 1,
     *           members: [
     *               "Name: 0111231234",
     *               "Number: 0111231235",
     *               "Group: 0111231236",
     *           ]
     *       }
     *     }
     */

    function data_namepair_post(){
        // Check Parameters
        // $api_key = $this->post('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        // }
        

        //util helper function to display formatted array data cleanly
        //dump($_POST);

        //exit();

        $imei = $this->post('imei');
        $real_email = $this->post('real_email');
        $user_email = $this->post('user_email');
        $name = $this->post('name');       
        $number = $this->post('number'); 
        $contacts = $this->post('contacts');      
        $postFormat = $this->post('contactPostFormat');
        $gps = $this->post('gps');




        //clean number input
        $number = preg_replace("/[^0-9]/", "", str_replace("+27", "0", $number));


        if(isset($_POST['csvDelimiter'])){
            $csvDelimiter = $_POST['csvDelimiter'];
        }

        if(isset($_POST['namePairDelimiter'])){
            $namePairDelimiter = $_POST['namePairDelimiter'];
        }


        if (!$imei) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: imei';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$real_email && !$user_email ) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: real_email OR user_email';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$name) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: name';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }
        

        if (!$number) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: number';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }

        

        
        // Load models
        $this->load->model('m_data_captured');


 
        

       if(in_array(strtolower($_POST['contactPostFormat']),array('csv','json',''))){
         


            switch(strtolower($contactPostFormat)){
                case 'csv':
                        if(!isset($csvDelimiter) || trim(strlen($csvDelimiter)) != 1){
                            $res['status'] = false;
                            $res['error'] = 'Missing CSV Delimiter value';
                            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
                        };

                        if(!isset($namePairDelimiter) || trim(strlen($namePairDelimiter)) !=1){
                            $res['status'] = false;
                            $res['error'] = 'Missing Name Pair Value Delimiter value';
                            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);                               
                        };

                        $data['type'] = 'csv';

                        $array = explode($namePairDelimiter,$contacts);
                        $contactsCleaned= array();

                        foreach($array as $key => $val){
                            //dump($val);
                            //if(is_string($val)){
                            //    echo $val."<br/>";
                            //};

                           $contactCleaned[] = explode($csvDelimiter,$val);
                        }
                        

                        $contacts = $contactCleaned;

                    break;

                case 'json':

                        /*  JSON EXAMPLE DATA FORMAT

                            [
                                { "Name":"Terence", "Number":"079 220 3134" },
                                { "Name":"Marian", "Number":"074 886 4633" },
                                { "Name":"Cameron", "Number":"011 656 6565" }
                            ]

                        */

                        json_decode($contacts);
                        if(!json_last_error() == JSON_ERROR_NONE){
                            $res['status'] = false;
                            $res['error'] = 'Invalid Contact Data';
                            $this->response($res, REST_Controller::HTTP_BAD_REQUEST); 
                        }

                        $temps = json_decode($contacts);

  
                        foreach($temps as $temp){

                                $contactCleaned[] = array($temp->Name,$temp->Number);

                        }

                        $contacts = $contactCleaned;

                    break;
                    
                default:
                        /* rewire contact list to include blank name values */
                    
                    if (!is_array($contacts)) {

                        $contacts = explode(',',preg_replace('/\[|\]|"|\'/', '', $contacts));
                    }
            
                    $contactlist = array();
                    foreach($contacts as $contact){
                        if($contact){
                             $contactlist[] = array("",$contact);
                        }
                    }
                    
                        $contacts = $contactlist;
                break;
            }

        }



        
        // Just store the request....
        $request_id = $this->m_data_captured->store($imei, $real_email, $user_email, $name, $number, $contacts, $gps);

        // Is this person in a group???
        $group_id = $this->m_data_captured->check_group($number);
        

        if (!$group_id) {
            // Create a new group.
            $group_id = $this->m_data_captured->create_group($request_id);
            $this->m_data_captured->add_group_number($group_id, $number,$name);
        }
        



        if (!empty($contacts)) {


            //link numbers to group.
            foreach($contacts as $contact) {
                if ($contact) { // don't store empty ones idiot
                    $this->m_data_captured->add_group_name_number($group_id, $contact);
                }
            }
        }


        // Get numbers in group

        $members = $this->m_data_captured->get_group_names_numbers($group_id);


        $res['status'] = true;
        $res['error'] = '';
        $res['data']['count'] = !empty($members) ? count($members) : '0';
        $res['data']['members'] = !empty($members) ? $members : array();
        
        //print_r($res['data']);
        //exit();
        $this->response($res, REST_Controller::HTTP_OK);      
    }
    
    
    /**
     * data_get
     *
     * @api {get} /data Get associated numbers
     * @apiHeader {String} X-API-KEY access key (can be set as a parameter as well)
     * @apiName data_get
     * @apiGroup General
     *
     * @apiParam {String} number this users number
     *
     * @apiSuccess {String} error Error description if status is false.
     * @apiSuccess {String} status  Success of the api call.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.count  Number of members
     * @apiSuccess {String[]} data.members  Member numbers including current number
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       error: "",
     *       status: true,
     *       data: {
     *           count: 1,
     *           members: [
     *               "0111231234",
     *               "0111231235",
     *               "0111231236",
     *           ]
     *       }
     *     }
     */
    function data_get() {

        // Check Parameters
        // $api_key = $this->get('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        // }

        $number = $this->get('number');
        if (!$number) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: number';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }

        // Load models
        $this->load->model('m_data_captured');

        $members = array();

        // Is this person in a group???
        $group_id = $this->m_data_captured->check_group($number);
        if ($group_id) {
            // Get numbers in group
            $members = $this->m_data_captured->get_group_numbers($group_id);
        }

        $res['status'] = true;
        $res['error'] = '';
        $res['data']['count'] = !empty($members) ? count($members) : '0';
        $res['data']['members'] = !empty($members) ? $members : array();
        $this->response($res, REST_Controller::HTTP_OK);
    }


    /**
     * data_installed_get
     *
     * @api {get} /data Get associated names, numbers and installed flag
     * @apiHeader {String} X-API-KEY access key (can be set as a parameter as well)
     * @apiName data_get
     * @apiGroup General
     *
     * @apiParam {String} number this users number
     *
     * @apiSuccess {String} error Error description if status is false.
     * @apiSuccess {String} status  Success of the api call.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.count  Number of members
     * @apiSuccess {String[]} data.members  Member numbers including current number
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       error: "",
     *       status: true,
     *       data: {
     *           count: 1,
     *           members: [
     *                  Name    "Lessink",
     *                  Number  "0824692527",
     *                  Group   "GP149"
     *           
     *       }
     *     }
     */
    function data_installed_get(){

        // Check Parameters
        // $api_key = $this->get('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        // }

        $number = $this->get('number');
        if (!$number) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: number';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }

        // Load models
        $this->load->model('m_data_captured');

        $members = array();

        // Is this person in a group???
        $group_id = $this->m_data_captured->check_group($number);
        
        if ($group_id) {
            // Get numbers in group
            $members = $this->m_data_captured->get_group_numbers_installs($group_id);
        }

        $res['status'] = true;
        $res['error'] = '';
        $res['data']['count'] = !empty($members) ? count($members) : '0';
        $res['data']['members'] = !empty($members) ? $members : array();
        $this->response($res, REST_Controller::HTTP_OK);
    }

    function data_name_number_get() {

        // Check Parameters
        // $api_key = $this->get('X-API-KEY');
        // if (!$api_key) {
        //     $res['status'] = false;
        //     $res['error'] = 'Invalid API key';
        //     $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        // }

        $number = $this->get('number');
        if (!$number) {
            $res['status'] = false;
            $res['error'] = 'Parameter required: number';
            $this->response($res, REST_Controller::HTTP_BAD_REQUEST);
        }

        // Load models
        $this->load->model('m_data_captured');

        $members = array();

        // Is this person in a group???
        $group_id = $this->m_data_captured->check_group($number);
        if ($group_id) {
            // Get numbers in group
            //$members = $this->m_data_captured->get_group_numbers($group_id);
            $members = $this->m_data_captured->get_group_names_numbers($group_id);
        }

        $res['status'] = true;
        $res['error'] = '';
        $res['data']['count'] = !empty($members) ? count($members) : '0';
        $res['data']['members'] = !empty($members) ? $members : array();
        $this->response($res, REST_Controller::HTTP_OK);
    }

}