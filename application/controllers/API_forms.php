<?php if (!defined('BASEPATH')) exit('No direct script access allowed');





class API_forms extends MY_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        // $this->load->model('m_company');
        // $this->load->model('m_residence');
        // $this->load->model('m_student_types');
        $this->load->model('m_data_captured');

    }

    function index() {

        /* Page Setup */
        $data['page_heading'] = 'API function index';

        /* Render */
        $this->layout->view('api/rest_index', $data);
    }

    function api_auth() {

        /* Page Setup */
        $data['page_heading'] = 'API authentication example';
        $data['form_action'] = 'login';
        $data['form_method'] = 'post';
        $data['form_submit'] = 'Authenticate';
        $data['api_key'] = 'boguskey';

        /* Render */
        $this->layout->view('api/rest_form_auth', $data);
    }

    /*first api call that will be made on fresh install of the app*/
    function app_setup_post(){
         $data['page_heading'] = 'API Setup Post data example';
         $data['form_action'] = 'setup_data';
         $data['form_method'] = 'post';
         $data['form_submit'] = 'Post data';
         $data['api_key'] = 'boguskey';
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


        /* Render */
         $this->layout->view('api/rest_form_setup_post', $data);       
    }

    /*first api call that will be made on fresh install of the app*/
    function app_device_post(){
        $data['page_heading'] = 'Add Device ';
        $data['form_action'] = 'setup_data';
        $data['form_method'] = 'post';
        $data['form_submit'] = 'Post data';
        $data['api_key'] = 'boguskey';
        // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


        /* Render */
        $this->layout->view('api/rest_form_device_post', $data);
    }
    function app_groupDetails_get(){
         $data['page_heading'] = 'API Get Group Details';
         $data['form_action'] = 'groupDetails';
         $data['form_method'] = 'get';
         $data['form_submit'] = 'Get Group Details';
         $data['api_key'] = 'boguskey';
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


        /* Render */
         $this->layout->view('api/rest_groupDetails_get', $data);         
    }

    function app_groupMemberDetails_get(){
         $data['page_heading'] = 'API Group Membership Details';
         $data['form_action'] = 'groupMembershipDetails';
         $data['form_method'] = 'get';
         $data['form_submit'] = 'Get Group Membership Details';
         $data['api_key'] = 'boguskey';
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


        /* Render */
         $this->layout->view('api/rest_groupMembershipDetails_get', $data);         
    }


    function app_communityGroups_get(){
         $data['page_heading'] = 'API Get Community Group List';
         $data['form_action'] = 'communityGroups';
         $data['form_method'] = 'get';
         $data['form_submit'] = 'Get Community Group List';
         $data['api_key'] = 'boguskey';
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';

        $this->layout->view('api/rest_communityGroups_get', $data);          
    }


    function app_changeGroupName_post(){
         $data['page_heading'] = 'API Change Group Name';
         $data['form_action'] = 'changeGroupName';
         $data['form_method'] = 'post';
         $data['form_submit'] = 'Change Group Name';
         $data['api_key'] = 'boguskey';
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';

         //Redirect('API/communityGroups');
        /* Render */
         $this->layout->view('api/rest_changeGroupName_get', $data);            
    }

    function app_groupMaintenance_post(){
         $data['page_heading'] = 'API Group Maintenance';
         $data['form_action'] = 'groupMaintenance';
         $data['form_method'] = 'post';
         $data['form_submit'] = 'Submit Changes';
         $data['api_key'] = 'boguskey';
         $data['member'] = $this->m_data_captured->get_member_info(1);
         $data['groups'] = $this->m_data_captured->get_group_by_assoc("byID",1);
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';

         //Redirect('API/communityGroups');
        /* Render */
         $this->layout->view('api/rest_groupMaintenance_get', $data); 
    }

    function app_groupAddmember_post(){
         $data['page_heading'] = 'API Add Group Member';
         $data['form_action'] = 'groupAddmember';
         $data['form_method'] = 'post';
         $data['form_submit'] = 'Add Member';
         $data['api_key'] = 'boguskey';
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';

         //Redirect('API/communityGroups');
        /* Render */
         $this->layout->view('api/rest_groupAddmember_post', $data); 
    }

    function app_Search_post(){
          $data['page_heading'] = 'API Search Groups / Members';
         $data['form_action'] = 'search';
         $data['form_method'] = 'post';
         $data['form_submit'] = 'Search';
         $data['api_key'] = 'boguskey';
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';

         //Redirect('API/communityGroups');
        /* Render */
         $this->layout->view('api/rest_search_post', $data);        
    }

    function app_joinRequests_post(){
         $data['page_heading'] = 'API Join Requests';
         $data['form_action'] = 'joinRequests';
         $data['form_method'] = 'post';
         $data['form_submit'] = 'Submit Request';
         $data['api_key'] = 'boguskey';
         // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';

         //Redirect('API/communityGroups');
        /* Render */
         $this->layout->view('api/rest_joinRequests_post', $data);       
    }
    // function api_data_post() {

    //     /* Page Setup */
    //     $data['page_heading'] = 'API Post data example';
    //     $data['form_action'] = 'data';
    //     $data['form_method'] = 'post';
    //     $data['form_submit'] = 'Post data';
    //     $data['api_key'] = 'boguskey';
    //     // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


    //     /* Render */
    //     $this->layout->view('api/rest_form_data_post', $data);
    // }

    // function api_data_namepair_post() {

    //     /* Page Setup */
    //     $data['page_heading'] = 'API Post data example';
    //     $data['form_action'] = 'data_namepair';
    //     $data['form_method'] = 'post';
    //     $data['form_submit'] = 'Post data';
    //     $data['api_key'] = 'boguskey';
    //     // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


    //     /* Render */
    //     $this->layout->view('api/rest_form_namepair_post', $data);
    // }
    
    
    // function api_data_get() {

    //     /* Page Setup */
    //     $data['page_heading'] = 'API Get data example';
    //     $data['form_action'] = 'data';
    //     $data['form_method'] = 'get';
    //     $data['form_submit'] = 'Get data';
    //     $data['api_key'] = 'boguskey';
    //     // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


    //     /* Render */
    //     $this->layout->view('api/rest_form_data_get', $data);
    // }

    // function api_assoc_installs_get() {

    //     /* Page Setup */
    //     $data['page_heading'] = 'API Get data example';
    //     $data['form_action'] = 'data_installed';
    //     $data['form_method'] = 'get';
    //     $data['form_submit'] = 'Get data';
    //     $data['api_key'] = 'boguskey';
    //     // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


    //     /* Render */
    //     $this->layout->view('api/rest_form_data_get', $data);
    // }

    // function api_data_name_number_get() {

    //     /* Page Setup */
    //     $data['page_heading'] = 'API Get data example';
    //     $data['form_action'] = 'data_name_number';
    //     $data['form_method'] = 'get';
    //     $data['form_submit'] = 'Get data';
    //     $data['api_key'] = 'boguskey';
    //     // $data['api_key'] = '95e2c57cea55c5e78a24f8be26f8b581296b3395';


    //     /* Render */
    //     $this->layout->view('api/rest_form_data_get', $data);
    // }
    
    function api_key_add() {

        /* Page Setup */
        $data['page_heading'] = 'Add a new API key';
        $data['form_action'] = 'key';
        $data['form_method'] = 'post';
        $data['form_submit'] = 'Add API key';
        $data['api_key'] = 'boguskey';

        /* Render */
        $this->layout->view('api/rest_form_add_api_key', $data);
    }

    function api_key_regenerate() {

        /* Page Setup */
        $data['page_heading'] = 'Regenerate an API key';
        $data['form_action'] = 'regenerate';
        $data['form_method'] = 'post';
        $data['form_submit'] = 'Regenerate API key';
        $data['api_key'] = 'boguskey';

        /* Render */
        $this->layout->view('api/rest_form_regenerate_api_key', $data);
    }

    function api_key_delete() {

        /* Page Setup */
        $data['page_heading'] = 'Delete an API key';
        $data['form_action'] = 'key';
        $data['form_method'] = 'post';
        $data['form_submit'] = 'Delete API key';
        $data['api_key'] = 'boguskey';

        /* Render */
        $this->layout->view('api/rest_form_delete_api_key', $data);
    }

    function api_key_level_update() {

        /* Page Setup */
        $data['page_heading'] = 'Update an API key level';
        $data['form_action'] = 'level';
        $data['form_method'] = 'post';
        $data['form_submit'] = 'Update API key';
        $data['api_key'] = 'boguskey';

        /* Render */
        $this->layout->view('api/rest_form_update_key_level', $data);
    }

    function api_key_suspend() {

        /* Page Setup */
        $data['page_heading'] = 'Suspend an API key level';
        $data['form_action'] = 'suspend';
        $data['form_method'] = 'post';
        $data['form_submit'] = 'Suspend API key';
        $data['api_key'] = 'boguskey';

        /* Render */
        $this->layout->view('api/rest_form_suspend_api_key', $data);
    }
}

/* End of file rest_forms.php */
/* Location: ./application/controllers/api/rest_forms.php */

