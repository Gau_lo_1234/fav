<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* api_management
*
* @uses     MY_Controller
*
* @category api_management
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
Class api_management extends MY_Controller {
    var $data;

    function __construct() {
        parent::__construct();
        $this->page_title =  ucfirst($this->lang->line('nav_dashboard'))." - ".$this->config->item('website_name', 'tank_auth');

        $this->load->model('m_api');
        $this->load->config('rest');

        $this->load->library('form_validation');
    }

    function Index() {
        redirect('/');
    }

    function keys() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

         $this->data['page_heading'] = 'API Keys';


        $this->data['api_keys'] = $this->m_api->get_key_list();

        $this->layout->view('keys', $this->data);
    }

    function logs() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }


         $this->data['page_heading'] = 'API Logs';


        $this->data['api_logs'] = $this->m_api->get_log_list();

        $this->layout->view('logs', $this->data);
    }

    function edit_key($key) {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        $this->data['key'] = $this->m_api->get_key($key);

        $this->data['page_heading'] = 'Edit API Key: '.$key;

        $this->form_validation->set_rules('level', 'level', 'trim|required|xss_clean');
        $this->form_validation->set_rules('ignore_limits', 'ignore_limits', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $this->m_api->update_key($key, array(
                'level' => $this->form_validation->set_value('level'),
                'ignore_limits' => $this->form_validation->set_value('ignore_limits')
                ));
            $this->session->set_flashdata('message', 'Key Updated');
            $this->session->set_flashdata('message_type', 'success');
            redirect('/api_management/keys');
        }

        $this->layout->view('key_edit', $this->data);
    }

    function generate_key() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }


        $key = $this->m_api->generate_key();
        $level = 1;
        $ignore_limits = 1;
        if ($this->m_api->insert_key($key, array('level' => $level, 'ignore_limits' => $ignore_limits))) {
            $this->session->set_flashdata('message', 'Key Created ');
            $this->session->set_flashdata('message_type', 'success');
            redirect('/api_management/keys');
        } else {
            $this->session->set_flashdata('message', 'Problem creating key');
            $this->session->set_flashdata('message_type', 'error');
            redirect('/api_management/keys');

        }
    }
}

/* End of file requests.php */
/* Location: ./application/controllers/requests.php */