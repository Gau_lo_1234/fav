<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

define('REGISTER_REPORT_NUMBER', 10);

/**
* Auth
*
* @uses     MY_Controller
*
* @category Authentication
* @package  Tank_Auth
*/
class Auth extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
        $this->lang->load('og');

        $this->layout->setLayout('layouts/auth');
    }

    function index() {
//      if ($message = $this->session->flashdata('message')) {
//                        $data['message']['text'] = $message;
//                        $data['message']['type'] = 'notice';
//          $this->layout->view('auth/login', $data);
//      } else {
        redirect('/auth/login/');
//      }
    }

    /**
     * Login as a user on the site
     *
     * @return void
     */
    function login_as($username) {
        if (!$this->tank_auth->is_logged_in()) {                                    // logged in
            redirect('');
        }

        $return = $this->input->post('return') ? $this->input->post('return') : '/';

        $this->form_validation->set_rules('reason', 'reason', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {

            $this->load->model('tankauth/users');
            $user = $this->users->get_user_by_username($username);
            if (is_null($user)) {
                $this->session->set_flashdata('message', 'Problem logging in as user: '.$username);
                $this->session->set_flashdata('message_type', 'error');
                redirect($return);
            } else {
                check_permissions(array('super', 'distrib', 'admin'), $this->tank_auth->get_user_id());

                $this->m_logger->log(
                    $this->tank_auth->get_user_id(),
                    'login_as',
                    'Successfully logged in as: '.$username,
                    array('user_id'=>$user->id,
                        'reason'=>$this->form_validation->set_value('reason')
                    ));

                // Logout
                $this->session->set_userdata(array('user_id' => '', 'username' => '', 'status' => '', 'role' => ''));
                // Login
                $this->tank_auth->refresh_session($user);

                $this->session->set_flashdata('message', 'Successfully logged in as: '.$username);
                redirect('');
            }
        } else {
            $this->session->set_flashdata('message', 'Problem logging in as user: '.$username.'<br/>Reason must be specified.');
            $this->session->set_flashdata('message_type', 'error');
            redirect($return);
        }
    }

    /**
     * Login user on the site
     *
     * @return void
     */
    function login() {
        // hack for marc's app so it can login with a get command
        if (!empty($_GET)) {
            $_POST = $_GET;
        }

        if ($this->tank_auth->is_logged_in()) {                                 // logged in
            if ($this->session->flashdata('message')) {
                    $message['text'] = $this->session->flashdata('message');
                    $message['type'] = ($this->session->flashdata('message_type') ? $this->session->flashdata('message_type') : 'notice');
                    $this->session->set_flashdata('message', $message['text']);
                    $this->session->set_flashdata('message_type', $message['type']);
            }
            redirect('');
        } elseif ($this->tank_auth->is_logged_in(FALSE)) {                      // logged in, not activated
            redirect('/auth/send_again/');

        } else {
            if ($this->session->userdata('return_URL')) {
                $data['return_URL'] = $this->session->userdata('return_URL');
                $this->session->unset_userdata('return_URL');
                            //clear it so we don't confuse the issue...
            }

            if (isset($_REQUEST['login'])) {
                $data['selected_login'] = $_REQUEST['login'];
            }

            $data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND
                $this->config->item('use_username', 'tank_auth'));
            $data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

            $this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('remember', 'Remember me', 'integer');
            $this->form_validation->set_rules('return_URL', 'Return URL', 'trim|xss_clean');

            // Get login for counting attempts to login
            if ($this->config->item('login_count_attempts', 'tank_auth') AND
                ($login = $this->input->post('login'))) {
                $login = $this->security->xss_clean($login);
            } else {
                $login = '';
            }


            $data['use_recaptcha'] = $this->config->item('use_recaptcha', 'tank_auth');
            if ($this->tank_auth->is_max_login_attempts_exceeded($login)) {
                if ($data['use_recaptcha']) {
                    $this->form_validation->set_rules('g-recaptcha-response', 'Confirmation Code', 'trim|xss_clean|required|check_recaptcha');
                } else {
                    $this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
                }
            }
            $data['errors'] = array();

            if ($this->form_validation->run()) {                                // validation ok
                if ($this->tank_auth->login(
                    $this->form_validation->set_value('login'),
                    $this->form_validation->set_value('password'),
                    $this->form_validation->set_value('remember'),
                    $data['login_by_username'],
                        $data['login_by_email'])) {                             // success

                    $this->m_logger->log(
                        $this->tank_auth->get_user_id(),
                        'login',
                        'User logged in.',
                        array('login'=>$this->form_validation->set_value('login'))
                        );

                if ($this->form_validation->set_value('return_URL')) {
                    redirect( $this->form_validation->set_value('return_URL') );
                } else {
                    redirect('');
                }

            } else {
                $errors = $this->tank_auth->get_error_message();
                    if (isset($errors['banned'])) {                             // banned user
                        $data['message']['text'] = $this->lang->line('auth_message_banned').'<br/>'.$errors['banned'];
                        $data['message']['type'] = 'error';
                    } elseif (isset($errors['not_activated'])) {                // not activated user
                        redirect('/auth/send_again/');
                    } elseif (isset($errors['expired'])) {
                        $data['message']['text'] = $this->lang->line($errors['expired']);
                        $data['message']['type'] = 'error';
                    } else {                                                    // fail
                        foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                    }
                }
            }
            $data['show_captcha'] = FALSE;
            if ($this->tank_auth->is_max_login_attempts_exceeded($login)) {
                $data['show_captcha'] = TRUE;
                if ($data['use_recaptcha']) {
                    $data['recaptcha_html'] = $this->_create_recaptcha();
                } else {
                    $data['captcha_html'] = $this->_create_captcha();
                }
            }
            $this->layout->view('auth/login_form', $data);
        }
    }

    /**
     * Logout user
     *
     * @return void
     */
    function logout() {
        $this->m_logger->log(
            $this->tank_auth->get_user_id(),
            'logout',
            'User logged out.',
            array()
            );

        $this->hooks->call('logout', array('user_id'=>$this->tank_auth->get_user_id()));

        $this->tank_auth->logout();

        $this->_show_message($this->lang->line('auth_message_logged_out'));
    }

    /**
     * Register user on the site
     *
     * @return void
     */
    function register($distributor_key='') {

        if ($this->tank_auth->is_logged_in()) {                                 // logged in
            redirect('');

        } elseif ($this->tank_auth->is_logged_in(FALSE)) {                      // logged in, not activated
            redirect('/auth/send_again/');

        } elseif (!$this->config->item('allow_registration', 'tank_auth')) {    // registration is off
            $this->_show_message($this->lang->line('auth_message_registration_disabled'));

        } else {

            $this->pixie_javascript->add_script('register.js', ASSET_PATH.'js/custom/');

            $data = array();

            $this->form_validation->set_rules('code', 'Registration Code', 'trim|xss_clean|required');

            $distributor_id = 0;
            if (!empty($_POST)) {
                if ($this->input->post('code')) {
                    $this->load->model('m_distributor');
                    $d = $this->m_distributor->get_by_registration_code($this->input->post('code'));
                    if (empty($d)) {
                        $data['errors']['code'] = "Invalid registration code. Please contact your distributor for assistance.";
                    } else {
                        $distributor_id = $d->id;
                    }
                }
            }

            $this->load->helper('register');
            $data = process_register_form($distributor_id, $data);

            $data['use_code'] = TRUE;

            $this->layout->view('auth/register_form', $data);
        }
    }

    /**
     * Send activation email again, to the same or new email address
     *
     * @return void
     */
    function send_again()
    {
        if (!$this->tank_auth->is_logged_in(FALSE)) {                           // not logged in or activated
            redirect('/auth/login/');

        } else {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

            $data['errors'] = array();

            if ($this->form_validation->run()) {                                // validation ok
                if (!is_null($data = $this->tank_auth->change_email(
                        $this->form_validation->set_value('email')))) {         // success

                    $data['site_name']  = $this->config->item('website_name', 'tank_auth');
                    $data['activation_period'] = display_seconds($this->config->item('email_activation_expire', 'tank_auth'));

                    $this->_send_email('activate', $data['email'], $data);

                    $this->_show_message(sprintf($this->lang->line('auth_message_activation_email_sent'), $data['email']));

                } else {
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->layout->view('auth/send_again_form', $data);
        }
    }

    /**
     * Activate user account.
     * User is verified by user_id and authentication code in the URL.
     * Can be called by clicking on link in mail.
     *
     * @return void
     */
    function activate()
    {
        $user_id        = $this->uri->segment(3);
        $new_email_key  = $this->uri->segment(4);

        // Activate user
        if ($this->tank_auth->activate_user($user_id, $new_email_key)) {        // success
            $this->tank_auth->logout();
            $this->session->sess_create();
            $this->session->set_flashdata('message', $this->lang->line('auth_message_activation_completed'));
            $this->session->set_flashdata('message_type', 'notice');
        } else {                                                                // fail
            $this->session->set_flashdata('message', $this->lang->line('auth_message_activation_failed'));
            $this->session->set_flashdata('message_type', 'error');
        }
        redirect('auth/login');
    }

    function activate_password()
    {
        $user_id        = $this->uri->segment(3);
        $new_email_key  = $this->uri->segment(4);

        $this->tank_auth->logout();

        // Check activation key valid???
        if (! $this->users->check_activation_key($user_id, $new_email_key, TRUE)) {
            $this->session->sess_create();
            $this->session->set_flashdata('message', $this->lang->line('auth_message_activation_failed'));
            $this->session->set_flashdata('message_type', 'error');
            redirect('auth/login');
        }

        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

        $data['errors'] = array();


        if ($this->form_validation->run()) {                                // validation ok

            // Activate user
            if ($this->tank_auth->activate_user($user_id, $new_email_key)) {        // success

                    //SET PASSWORD!!!
                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                    // Hash new password using phpass
                $hashed_password = $hasher->HashPassword($this->form_validation->set_value('new_password'));

                    // Replace old password with new one
                $this->users->change_password($user_id, $hashed_password);

                $this->session->set_flashdata('message', $this->lang->line('auth_message_activation_completed'));
                $this->session->set_flashdata('message_type', 'notice');
                redirect('/auth/login');
            } else {                                                                // fail
                $this->session->set_flashdata('message', $this->lang->line('auth_message_activation_failed'));
                $this->session->set_flashdata('message_type', 'error');
                redirect('/auth/login');
            }

        }

        $this->layout->view('auth/set_password_form', $data);
    }

    /**
     * Generate reset code (to change password) and send it to user
     *
     * @return void
     */
    function forgot_password()
    {
        if ($this->tank_auth->is_logged_in()) {                                 // logged in
            redirect('');

        } elseif ($this->tank_auth->is_logged_in(FALSE)) {                      // logged in, not activated
            redirect('/auth/send_again/');

        } else {
            $this->form_validation->set_rules('login', 'Email or login', 'trim|required|xss_clean');

            $data['errors'] = array();

            if (isset($_GET['login'])) {
                $data['selected_login'] = $_GET['login'];
            }

            if ($this->form_validation->run()) {                                // validation ok
                if (!is_null($data = $this->tank_auth->forgot_password(
                    $this->form_validation->set_value('login')))) {

                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                    // Send email with password activation link
                $this->_send_email('forgot_password', $data['email'], $data);

                $this->_show_message($this->lang->line('auth_message_new_password_sent'));

                } else {
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->layout->view('auth/forgot_password_form', $data);
        }
    }

    /**
     * Replace user password (forgotten) with a new one (set by user).
     * User is verified by user_id and authentication code in the URL.
     * Can be called by clicking on link in mail.
     *
     * @return void
     */
    function reset_password()
    {
        $user_id        = $this->uri->segment(3);
        $new_pass_key   = $this->uri->segment(4);

        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

        $data['errors'] = array();

        if ($this->form_validation->run()) {                                // validation ok
            if (!is_null($data = $this->tank_auth->reset_password(
                $user_id, $new_pass_key,
                    $this->form_validation->set_value('new_password')))) {  // success

                $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                // Send email with new password
            $this->_send_email('reset_password', $data['email'], $data);

            $this->_show_message($this->lang->line('auth_message_new_password_activated').' '.anchor('/auth/login/', 'Login'));

            } else {                                                        // fail
                $this->_show_message($this->lang->line('auth_message_new_password_failed'));
            }
        } else {
            // Try to activate user by password key (if not activated yet)
            if ($this->config->item('email_activation', 'tank_auth')) {
                $this->tank_auth->activate_user($user_id, $new_pass_key, FALSE);
            }

            if (!$this->tank_auth->can_reset_password($user_id, $new_pass_key)) {
                $this->_show_message($this->lang->line('auth_message_new_password_failed'));
            }
        }
        $this->layout->view('auth/reset_password_form', $data);
    }

    /**
     * Change user password
     *
     * @return void
     */
    function change_password()
    {
        if (!$this->tank_auth->is_logged_in()) {                                // not logged in or not activated
            redirect('/auth/login/');

        } else {
            $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
            $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

            $data['errors'] = array();

            if ($this->form_validation->run()) {                                // validation ok
                if ($this->tank_auth->change_password(
                    $this->form_validation->set_value('old_password'),
                        $this->form_validation->set_value('new_password'))) {   // success
                    $this->_show_message($this->lang->line('auth_message_password_changed'));

                } else {                                                        // fail
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->layout->view('auth/change_password_form', $data);
        }
    }

    /**
     * Change user email
     *
     * @return void
     */
    function change_email()
    {
        if (!$this->tank_auth->is_logged_in()) {                                // not logged in or not activated
            redirect('/auth/login/');

        } else {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

            $data['errors'] = array();

            if ($this->form_validation->run()) {                                // validation ok
                if (!is_null($data = $this->tank_auth->set_new_email(
                    $this->form_validation->set_value('email'),
                        $this->form_validation->set_value('password')))) {          // success

                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                    // Send email with new email address and its activation link
                $this->_send_email('change_email', $data['new_email'], $data);

                $this->_show_message(sprintf($this->lang->line('auth_message_new_email_sent'), $data['new_email']));

                } else {
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->layout->view('auth/change_email_form', $data);
        }
    }

    /**
     * Replace user email with a new one.
     * User is verified by user_id and authentication code in the URL.
     * Can be called by clicking on link in mail.
     *
     * @return void
     */
    function reset_email()
    {
        $user_id        = $this->uri->segment(3);
        $new_email_key  = $this->uri->segment(4);

        // Reset email
        if ($this->tank_auth->activate_new_email($user_id, $new_email_key)) {   // success
            $this->tank_auth->logout();
            $data['message']['text'] =$this->lang->line('auth_message_new_email_activated').' '.anchor('/auth/login/', 'Login');
            $data['message']['type'] = 'notice';

        } else {                                                                // fail
            $data['message']['text'] = $this->lang->line('auth_message_new_email_failed');
            $data['message']['type'] = 'error';
        }
        $this->layout->view('auth/general_message', $data);
    }

    /**_show_message
     * Delete user from the site (only when user is logged in)
     *
     * @return void
     */
    function unregister()
    {
        if (!$this->tank_auth->is_logged_in()) {                                // not logged in or not activated
            redirect('/auth/login/');

        } else {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

            $data['errors'] = array();

            if ($this->form_validation->run()) {                                // validation ok
                if ($this->tank_auth->delete_user(
                        $this->form_validation->set_value('password'))) {       // success
                    $this->_show_message($this->lang->line('auth_message_unregistered'));

                } else {                                                        // fail
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                }
            }
            $this->layout->view('auth/unregister_form', $data);
        }
    }

    /**
     * Show info message
     *
     * @param   string
     * @return  void
     */
    function _show_message($message)
    {
        $this->session->set_flashdata('message', $message);
        $this->session->set_flashdata('message_type', 'notice');
        redirect('/auth/login');
    }

    /**
     * Send email message of given type (activate, forgot_password, etc.)
     *
     * @param   string
     * @param   string
     * @param   array
     * @return  void
     */
    function _send_email($type, $email, &$data)
    {

        $this->load->helper('email');

        $subject = sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth'));
        $message = $this->load->view('email/'.$type.'-html', $data, TRUE);
        $alt_message = $this->load->view('email/'.$type.'-txt', $data, TRUE);

        sendMail($email, $subject, $message, $alt_message);
    }


    /**
     * Callback function. Check if CAPTCHA test is passed.
     *
     * @param   string
     * @return  bool
     */
    function _check_captcha($code)
    {
        $time = $this->session->flashdata('captcha_time');
        $word = $this->session->flashdata('captcha_word');

        list($usec, $sec) = explode(" ", microtime());
        $now = ((float)$usec + (float)$sec);

        if ($now - $time > $this->config->item('captcha_expire', 'tank_auth')) {
            $this->form_validation->set_message('_check_captcha', $this->lang->line('auth_captcha_expired'));
            return FALSE;

        } elseif (($this->config->item('captcha_case_sensitive', 'tank_auth') AND
            $code != $word) OR
        strtolower($code) != strtolower($word)) {
            $this->form_validation->set_message('_check_captcha', $this->lang->line('auth_incorrect_captcha'));
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Create reCAPTCHA JS and non-JS HTML to verify user as a human
     *
     * @return  string
     */
    function _create_recaptcha()
    {
        $this->load->helper('recaptcha2');

        recaptcha_add_js();

        // Get reCAPTCHA JS and non-JS HTML
        $html = recaptcha_get_html($this->config->item('recaptcha_public_key', 'tank_auth'));

        return $html;
    }
    /**
     * Create CAPTCHA image to verify user as a human
     *
     * @return  string
     */
    function _create_captcha()
    {
        $CI = &get_instance();
        $CI->load->helper('captcha');

        $cap = create_captcha(array(
            'img_path'      => './'.$CI->config->item('captcha_path', 'tank_auth'),
            'img_url'       => base_url().$CI->config->item('captcha_path', 'tank_auth'),
            'font_path'     => './'.$CI->config->item('captcha_fonts_path', 'tank_auth'),
            'font_size'     => $CI->config->item('captcha_font_size', 'tank_auth'),
            'img_width'     => $CI->config->item('captcha_width', 'tank_auth'),
            'img_height'    => $CI->config->item('captcha_height', 'tank_auth'),
            'show_grid'     => $CI->config->item('captcha_grid', 'tank_auth'),
            'expiration'    => $CI->config->item('captcha_expire', 'tank_auth'),
            ));

            // Save captcha params in session
        $CI->session->set_flashdata(array(
            'captcha_word' => $cap['word'],
            'captcha_time' => $cap['time'],
            ));

        return $cap['image'];
    }

    function _check_register_code($code) {
            // Get code from database
        $register_code = $this->m_settings->get_by_key('register_code');

        if ($register_code->value == $code) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
