<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Dashboard
*
* @uses     MY_Controller
*
* @category Dashboard
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
Class Dashboard extends MY_Controller {
    var $data;

var $role;
    function __construct() {
        parent::__construct();
        $this->load->model('m_data_captured');
        $this->page_title =  ucfirst($this->lang->line('nav_dashboard'))." - ".$this->config->item('website_name', 'tank_auth');
        $this->data['role']=1;
    }

    function Index() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        //$this->data['role']=1;
        $this->load->model('m_data_captured');
        $this->data['request_logs_counts'] = $this->m_data_captured->get_count();
        $this->data['groups_counts'] = $this->m_data_captured->get_group_count();
        $this->data['numbers_counts'] = $this->m_data_captured->get_number_count();
        $this->data['distributor_counts'] = $this->m_data_captured->get_distributor_count();
        $this->data['devices_counts'] = $this->m_data_captured->get_devices_count();

        $this->layout->view('dashboard', $this->data);

    }

    function reports() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        $this->load->model('m_data_captured');


        $this->data['request_logs_counts'] = $this->m_data_captured->get_count();
        $this->data['groups_counts'] = $this->m_data_captured->get_group_count();
        $this->data['numbers_counts'] = $this->m_data_captured->get_number_count();
        $this->data['distributor_counts'] = $this->m_data_captured->get_distributor_count();
        $this->data['devices_counts'] = $this->m_data_captured->get_devices_count();

        $this->layout->view('reports', $this->data);

    }


    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        else
        {
            $this->load->model('m_data_captured');
            $data['roles'] = $this->m_data_captured->getUserRoles();

            $this->global['pageTitle'] = 'MagTouch Electronics : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    /**
     *This function allows you to add  a new user, it call the add new function in the m_data_captured model
     */
function addNewUser()
{
    if (!$this->tank_auth->is_logged_in()) {
        redirect('/auth/login/');
    } else {
    $this->load->model('m_data_captured');
    $data['roles'] = $this->m_data_captured->getUserRoles();
        $this->m_data_captured->addNewUser();
        $data['message'] = 'Data Inserted Successfully';
    $this->loadViews("addNew", $this->global, $data, NULL);
}
}

/**
     * This function is used to load the add new form
     */
    function editInfo()
    {
        $id=isset($_GET['id']);
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        else
        {

            $this->load->model('m_data_captured');
            //$data['roles'] = $this->m_data_captured->getUserRoles();
            $data['results'] = $this->m_data_captured->get_devices_find();
            $this->m_data_captured->editDevice();
            $this->global['pageTitle'] = 'MagTouch Electronics : Edit User';

            $this->loadViews("editInfo", $this->global, $data, $this->data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function editDevice()
    {
        $id=isset($_GET['id']);
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        else
        {

            $this->load->model('m_data_captured');

            $this->global['pageTitle'] = 'MagTouch Electronics : Edit User';
            $this->layout->view('editInfo', $this->data);
        }
    }

    function settings(){
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        $this->layout->view('settings', $this->data);

    }
    function accounts() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        $this->load->model('m_data_captured');

        $this->data['page_heading'] = 'accounts';

        $searchText = $this->security->xss_clean($this->input->post('searchText'));
        $data['searchText'] = $searchText;

        $this->load->library('pagination');

        $count = $this->m_data_captured->deviceListingCount($searchText);

        $returns = $this->paginationCompress ( "accounts/", $count, 10 );

        $data['userRecords'] = $this->m_data_captured->deviceListing($searchText, $returns["page"], $returns["segment"]);

        //$this->global['pageTitle'] = 'MagTouch Electronics : User Listing';

        $this->loadViews("accounts", $this->global, $data, NULL);
        //$this->data['distributor_data'] = $this->m_data_captured->get_distributor_data();

        //$this->layout->view("users", $this->global, $data, NULL);
    }

    function users() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        $this->load->model('m_data_captured');

        $this->data['page_heading'] = 'users';

        $searchText = $this->security->xss_clean($this->input->post('searchText'));
        $data['searchText'] = $searchText;

        $this->load->library('pagination');

        $count = $this->m_data_captured->get_user_count($searchText);

        $returns = $this->paginationCompress ( "users/", $count, 5 );

        $data['userRecords'] = $this->m_data_captured->userListing($searchText, $returns["page"], $returns["segment"]);

        //$this->global['pageTitle'] = 'MagTouch Electronics : User Listing';

        $this->loadViews("users", $this->global, $data, NULL);
        //$this->data['distributor_data'] = $this->m_data_captured->get_distributor_data();

        //$this->layout->view("users", $this->global, $data, NULL);
    }

    /**
     * The following function loads all the distributors
     */
    function distributors() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    else {
    $this->load->model('m_data_captured');
    $this->load->library('pagination');
    $this->data['page_heading'] = 'distributors';
    $searchText = $this->security->xss_clean($this->input->post('searchText'));
    $data['searchText'] = $searchText;
    $count = $this->m_data_captured->get_distributor_count($searchText);
    $returns = $this->paginationCompress("distributors/", $count, 10);
    $this->load->library('pagination');
    $this->load->helper('url');


    $data['userRecords'] = $this->m_data_captured->distributorListing($searchText, $returns["page"], $returns["segment"]);
    //$this->global['pageTitle'] = 'MagTouch Electronics : User Listing';
    $this->loadViews("distributors", $this->global, $data, NULL);
    //$this->data['distributor_data'] = $this->m_data_captured->get_distributor_data();

    //$this->layout->view("users", $this->global, $data, NULL);
        if(isset($_GET['delid'])){
$delid=$_GET['delid'];
$this->m_data_captured->update_distributor($delid,$use);
echo "<script>
alert('Deleted');
</script>";
        }
}
    }


    /**
     *
     *
     */
    function devices() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
else {
    $this->load->model('m_data_captured');

    $this->data['page_heading'] = 'Devices';

    $searchText = $this->security->xss_clean($this->input->post('searchText'));
    $data['searchText'] = $searchText;

    $this->load->library('pagination');

    $count = $this->m_data_captured->deviceListingCount($searchText);
    $numbers=    $count = $this->m_data_captured->get_devices_count();


    $returns = $this->paginationCompress("devices/", $count, 10);
    $data['count']=$numbers;

    $data['userRecords'] = $this->m_data_captured->deviceListing($searchText, $returns["page"], $returns["segment"]);

    //$this->global['pageTitle'] = 'MagTouch Electronics : User Listing';

    $this->loadViews("devices", $this->global, $data, $count, NULL);
    //$this->data['distributor_data'] = $this->m_data_captured->get_distributor_data();

    //$this->layout->view("users", $this->global, $data, NULL);
}
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->m_data_captured->checkEmailExists($email);
        } else {
            $result = $this->m_data_captured->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }

    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        else
        {
            if($userId == null)
            {
                redirect('users');
            }

            $data['roles'] = $this->m_data_captured->getUserRoles();
            $data['userInfo'] = $this->m_data_captured->getUserInfo($userId);

            $this->global['pageTitle'] = 'MagTouch Electronics : Edit User';

            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editDist($userId = NULL)
    {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        else
        {
            if($userId == null)
            {
                redirect('users');
            }

            $data['roles'] = $this->m_data_captured->getUserRoles();
            $data['userInfo'] = $this->m_data_captured->getDistInfo($userId);
            $this->global['pageTitle'] = 'MagTouch Electronics : Edit User';
            $this->loadViews("editDist", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if (!$this->tank_auth->is_logged_in()) {
            echo(json_encode(array('status'=>'access')));
            redirect('/auth/login/');
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            $result = $this->m_data_captured->deleteUser($userId, $userInfo);

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        $this->global['pageTitle'] = 'MagTouch Electronics : Change Password';

        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }


    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');

        if($this->form_validation->run() == FALSE)
        {
            $this->loadChangePass();
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');

            $resultPas = $this->m_data_captured->matchOldPassword($this->vendorId, $oldPassword);

            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                    'updatedDtm'=>date('Y-m-d H:i:s'));

                $result = $this->m_data_captured->changePassword($this->vendorId, $usersData);

                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }

                redirect('loadChangePass');
            }
        }
    }

    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'MagTouch Electronics : 404 - Page Not Found';

        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        else
        {
            $userId = ($userId == NULL ? $this->session->userdata("userId") : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->m_data_captured->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;

            $this->load->library('pagination');
            $count = $this->m_data_captured->loginHistoryCount($userId, $searchText, $fromDate, $toDate);
            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 5, 3);
            $data['userRecords'] = $this->m_data_captured->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'MagTouch Electronics : User Login History';
            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }
    }




}



/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */