<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Error
*
* @uses     CI_Controller
*
* @category ERROR
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
class Error extends MX_Controller {

        /**
        * error_404
        *
        * @access public
        *
        * @return void
        */
        function error_404() {
                $this->output->set_status_header('404');
                $data['error_content'] ='';
                $data['error_content'] .= '<h2>Woops! Page Not Found</h2>';
                $data['error_content'] .= '<p>The page you requested was not found.</p>';
                if ($_SERVER['REQUEST_URI'] != '/') {
                        $data['error_content'] .= '<br class="clrflt"/><a href="'.site_url().'" class="button"><img src="'.ASSET_URL.'images/back.png" /> Go back to Dashboard</a></p>';
                }

                $this->load->view('error', $data);
        }

        /**
        * installer_missing
        *
        * @access public
        *
        * @return void
        */
        function installer_missing() {
                $data['error_content'] = '';
                $data['error_content'] .= "<h1>Fatal Error Encountered.</h1>";
                $data['error_content'] .= "<p>The installer folder could not be found!</p>";
                $data['error_content'] .= "<p>Your code version and installed version do not match. </p>";
                $data['error_content'] .= "<p>Please rectify this error.</p>";
                $this->load->view('error', $data);
        }

        /**
        * upgrade_application
        *
        * @access public
        *
        * @return void
        */
        function upgrade_application() {
                $data['error_content'] = '';
                $data['error_content'] .= "<h1>Upgrade Application.</h1>";
                $data['error_content'] .= "<p>This application's code version and installed version do not match.</p><p>Please notify your system administrator to rectify the problem</p>";
                $this->load->view('error', $data);
        }


        /**
        * js_error_handler
        *
        *  AJAX. Sends an email with javascript error.
        *
        * @access public
        *
        * @return void
        */
        function js_error_handler() {
                // Ignore simple "Script errors." This is caused by cross domain poopy things
                // that I don't think is on our side.
                if (strpos(strtolower($_GET['message']), 'script error') !== FALSE) { return; }
                if (strpos(strtolower($_GET['message']), 'expected identifier') !== FALSE) { return; }
                // Ignore errors if certain information is not available.
                if ($_GET['user'] == '' || $_GET['url'] == '' || $_GET['url'] = 'undefined' || $_GET['page'] == '') { return; }

                $this->load->helper('email');
                $error_email = $this->m_settings->get_by_key('error_email');
                if ($error_email != null) {
                        $to = $error_email->value;
                } else {
                        $to = "elsabe@lessink.co.za";
                }
                if ($_GET['message'] != 'Expected identifier') {
                        $subject = 'A javascript error has been detected on '. $_GET['website'];
                        $message = 'Error: '. $_GET['message']. '<br />';
                        $message .= 'Url/File: '. $_GET['url']. '<br />';
                        $message .= 'Line: '. $_GET['line']. '<br />';
                        $message .= 'UserAgent: '. $_GET['userAgent']. '<br />';
                        $message .= 'User: '. $_GET['user']. '<br />';
                        $message .= 'Page: '. $_GET['page']. '<br />';
                        $x = sendMail($to, $subject, $message, $message);
                }
        }
}