<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Groups
*
* @uses     MY_Controller
*
* @category Groups
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
Class Groups extends MY_Controller {
    var $data;

    function __construct() {
        parent::__construct();
        $this->page_title =  ucfirst($this->lang->line('nav_dashboard'))." - ".$this->config->item('website_name', 'tank_auth');
    }

    function Index() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

         $this->load->model('m_data_captured');

         $this->data['page_heading'] = 'Groups';


        $this->data['group_data'] = $this->m_data_captured->get_group_data();

        $this->layout->view('groups', $this->data);
    }
}

/* End of file requests.php */
/* Location: ./application/controllers/requests.php */