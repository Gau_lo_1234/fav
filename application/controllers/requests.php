<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Requests
*
* @uses     MY_Controller
*
* @category Requests
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
Class Requests extends MY_Controller {
    var $data;

    function __construct() {
        parent::__construct();
        $this->page_title =  ucfirst($this->lang->line('nav_dashboard'))." - ".$this->config->item('website_name', 'tank_auth');
    }

    function Index() {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

         $this->load->model('m_data_captured');

         $this->data['page_heading'] = 'Requests Logged';


        $this->data['request_data'] = $this->m_data_captured->get_data();

        $this->layout->view('requests', $this->data);
    }
}

/* End of file requests.php */
/* Location: ./application/controllers/requests.php */