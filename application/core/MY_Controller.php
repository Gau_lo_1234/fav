<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* MY_Controller
*
* @uses     MX_Controller
*
* @category Core
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
class  MY_Controller  extends  MX_Controller  {
    protected $global = array ();

    function __construct()  {
        parent::__construct();
        // Session not in autoload because we don't want it
        // running for the
        $this->load->library( array('session', 'tank_auth', 'layout', 'pixie_javascript', 'pixie_css'));

        if (!$this->database_status->isOnline() && $this->router->class != "auth") {
            redirect('/auth/login/');
            exit();
        }

        /**
         * Language + user login
         */
        if (!$this->tank_auth->is_logged_in() && !defined('CRON')) {

            if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') === TRUE) {
                // AJAX consideration.
                $response = array('action'=>'access_denied', 'status'=>3, 'error'=>'Login is required.');
                echo json_encode($response);
                exit(0);
            } else {
                if ($this->router->class != "auth" && $this->router->class != 'content' && $this->router->class !='dashboard') {
                    $qs = '';
                    if (!empty($_GET)) {
                        $qs = '?'.http_build_query($_GET);
                    }
                    $this->session->set_userdata('return_URL', uri_string().$qs);
                    redirect('/');
                    exit();
                }
            }
        } else {
             $this->lang->load('og', $this->tank_auth->get_user_language());
        }
        $this->lang->load('tank_auth');

    }


    public function response($data = NULL) {
        $this->output->set_status_header ( 200 )->set_content_type ( 'application/json', 'utf-8' )->set_output ( json_encode ( $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) )->_display ();
        exit ();
    }

    function loadViews($viewName = "", $headerInfo = NULL, $pageInfo = NULL, $footerInfo = NULL){

       $this->load->view('elements/header', $headerInfo);
        $this->load->view($viewName, $pageInfo);
       $this->load->view('elements/footer', $footerInfo);
    }

    /**
     * This function used provide the pagination resources
     * @param {string} $link : This is page link
     * @param {number} $count : This is page count
     * @param {number} $perPage : This is records per page limit
     * @return {mixed} $result : This is array of records and pagination data
     */
    function paginationCompress($link, $count, $perPage = 10, $segment = SEGMENT) {
        $this->load->library ( 'pagination' );

        $config ['base_url'] = base_url () . $link;
        $config ['total_rows'] = $count;
        $config ['uri_segment'] = $segment;
        $config ['per_page'] = $perPage;
        $config ['num_links'] = 5;
        $config ['full_tag_open'] = '<nav><ul class="pagination">';
        $config ['full_tag_close'] = '</ul></nav>';
        $config ['first_tag_open'] = '<li class="arrow">';
        $config ['first_link'] = 'First';
        $config ['first_tag_close'] = '</li>';
        $config ['prev_link'] = 'Previous';
        $config ['prev_tag_open'] = '<li class="arrow">';
        $config ['prev_tag_close'] = '</li>';
        $config ['next_link'] = 'Next';
        $config ['next_tag_open'] = '<li class="arrow">';
        $config ['next_tag_close'] = '</li>';
        $config ['cur_tag_open'] = '<li class="active"><a href="#">';
        $config ['cur_tag_close'] = '</a></li>';
        $config ['num_tag_open'] = '<li>';
        $config ['num_tag_close'] = '</li>';
        $config ['last_tag_open'] = '<li class="arrow">';
        $config ['last_link'] = 'Last';
        $config ['last_tag_close'] = '</li>';

        $this->pagination->initialize ( $config );
        $page = $config ['per_page'];
        $segment = $this->uri->segment ( $segment );

        return array (
                "page" => $page,
                "segment" => $segment
        );
    }

} //class

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */