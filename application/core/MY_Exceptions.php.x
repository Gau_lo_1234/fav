<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {
    public function __construct() {
        parent::__construct();
    }
    function log_exception($severity, $message, $filepath, $line) {
        $severity = ( ! isset($this->levels[$severity])) ? $severity : $this->levels[$severity];
        log_message('error', 'Severity: '.$severity.'  --> '.$message. ' '.$filepath.' '.$line, TRUE);

        if (strtolower($severity) != 'notice' &&
                (strpos($message, 'virtual_control_room.php') === false && strpos($message, '171') === false) // Ignore VCR header error
            ) {
            $error_msg = "<p>message: $message </p>";
            $error_msg .= "<p>severity: $severity </p>";
            $error_msg .= "<p>filepath: $filepath </p>";
            $error_msg .= "<p>line: $line </p>";
            if (function_exists('current_url')) {
                $error_msg .= "<p>curent url: ".current_url()." </p>";
            }

            $this->_mail_error($error_msg);
        }
    }

    function show_error($heading, $message, $template = 'error_general', $status_code = 500) {
        if (is_array($message)) {
            $error_msg = "<p>message: ".implode('; ',$message)." </p>";
        } else {
            $error_msg = "<p>message: $message </p>";
        }
        $error_msg .= "<p>heading: $heading </p>";
        $mail_success = $this->_mail_error($error_msg);

         set_status_header($status_code);

        $CI = get_instance();
        $debug_print=$CI->config->item('debug_print');

        $message = '<p>'.implode('</p><p>', ( ! is_array($message)) ? array($message) : $message).'</p>';

        if (ob_get_level() > $this->ob_level + 1)
        {
            ob_end_flush();
        }
        ob_start();
        include(APPPATH.'errors/'.$template.EXT);
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }

    function _mail_error($error_msg) {
        if (error_reporting() != 0) { // suppressed errors should not be emailed

            if (defined('CRON') && CRON) {
                $include_path = APPPATH;
            } else {
                $include_path = str_replace("/system", "", BASEPATH).APPPATH;
            }

           $ci = &get_instance();

            // send email
            include($include_path.'helpers/MY_email_helper'.EXT);
            $subject = site_url()." Error Encountered";

            if (isset($ci->tank_auth)) {
                $user_info = $ci->tank_auth->get_user_id();
            } else {
                $user_info = 'not available';
            }
            $mail_message = "<p>Server: ".site_url()."<p>Time: ".date('c')."</p><p>User: $user_info </p>";
            $mail_message .= $error_msg;
            if (function_exists('current_url')) {
                $mail_message .= "<p>curent url: ".current_url()." </p>";
            }

            if (isset($ci->m_settings)) {
                $ci->db->_reset_write(); // prevent recursive db errror
                $setting = $ci->m_settings->get_by_key('error_email');
                if ($setting != null) {
                        $email = $setting->value;
                } else {
                        $email = "elsabe@lessink.co.za";
                }
            } else {
                        $email = "elsabe@lessink.co.za";
            }

            if (sendMail($email, $subject, $mail_message, $mail_message)) {
                return "Sent to: ".$email;
            } else {
                return "Error sending to: ".$email;
            }
        }

    }
}
