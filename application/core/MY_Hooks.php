<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package     CodeIgniter
 * @author      ExpressionEngine Dev Team
 * @copyright       Copyright (c) 2006, EllisLab, Inc.
 * @license     http://codeigniter.com/user_guide/license.html
 * @link        http://codeigniter.com
 * @since       Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter MY_Hooks Class
 *
 * Provides a mechanism to extend the base plugin system.  This class
 * allows other plugin hooks to be created and called without messing
 * with the core system hooks.
 *
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category        Libraries
 * @author      David @ <xeoncross.com>
 * @link        http://codeigniter.com/forums/viewthread/67697/
 */
class MY_Hooks extends CI_Hooks {

    //Added - an array of objects that can be re-used
    var $objects        = array();

    /**
     * Add hooks, this is to be used by the modules!
     * @param <type> $hooks
     */
    function add_hooks($hooks) {
        foreach ($hooks as $k => $a) {
            foreach ($a as $v) {
                $this->hooks[$k][] = $v;
            }
        }
    }


    /**
     * Filter Hook
     *
     * Similar to call() but alows data to be filtered
     * by multible hooks and the result is returned.
     *
     *  The function called by the hook should cehck arg list for
     * 'filter_value' and use that so that functions can be chained.
     * 
     * @access  public
     * @param   string  the hook name
     * @param   mixed   the data to filter
     * @return  mixed
     */
    function filter($which = '', $data=null, $cron = false) {
        if (!$this->enabled OR !isset($this->hooks[$which])) {
            return FALSE;
        }

        if (isset($this->hooks[$which][0]) AND is_array($this->hooks[$which][0])) {

            //For each registered hook...
            foreach ($this->hooks[$which] as $val) {
                //Ask that hook to filter the data
                $data['filter_value'] = $this->_run_hook($val, $data, $cron, true);
            }

            //return the result
            return $data['filter_value'];

        } else {
            //return the result since there is only one hook
            return $this->_run_hook($this->hooks[$which], $data, $cron, true);
        }
    }

    /**
     * Call Hook
     *
     *
     * @access  public
     * @param   string  the hook name
     * @param   mixed   the data to filter
     * @return  mixed
     */
    function call($which = '', $data=null, $cron = false) {

        if (!$this->enabled OR !isset($this->hooks[$which])) {
            return FALSE;
        }

        if (isset($this->hooks[$which][0]) AND is_array($this->hooks[$which][0])) {

        //For each registered hook...
            foreach ($this->hooks[$which] as $val) {
            //Ask that hook to filter the data
                $this->_run_hook($val, $data, $cron);
            }
        } else {
        //return the result since there is only one hook
            $this->_run_hook($this->hooks[$which], $data, $cron);
        }
        return true;
    }

    // --------------------------------------------------------------------

    /**
     * Run Hook
     *
     * Runs a particular hook
     *
     * @access  private
     * @param   array   the hook details
     * @return  bool
        */
    function _run_hook($hook, $data=null, $cron=false, $filter=false) {
        if (!is_array($hook)) {
            return FALSE;
        }

        // -----------------------------------
        // Safety - Prevents run-away loops
        // -----------------------------------

        // If the script being called happens to have the same
        // hook call within it a loop can happen
        
        if ($this->in_progress == TRUE) {
            return; 
        }

        // -----------------------------------
        // Set file path
        // -----------------------------------
        
        if (!isset($hook['filename'])) {
            return FALSE;
        }

        //If the hook file path is NOT set - default to "hooks"
        if(!isset($hook['filepath'])) {
            $hook['filepath'] = 'hooks';
        }
        
        $filepath = APPPATH.$hook['filepath'].'/'.$hook['filename'];

        if (!file_exists($filepath)){
            return FALSE;
        }
        
        // -----------------------------------
        // Set class/function name
        // -----------------------------------
        
        $class      = FALSE;
        $function   = FALSE;
        $params     = '';
        
        if (isset($hook['class']) AND $hook['class'] != '') {
            $class = $hook['class'];
        }

        if (isset($hook['function'])) {
            $function = $hook['function'];
        }

        if (isset($hook['params'])) {
            $params = $hook['params'];
        }
        
        if ($class === FALSE AND $function === FALSE) {
            return FALSE;
        }

        // -----------------------------------
        // Set the in_progress flag
        // -----------------------------------

        if (!$cron) { /* Bit of a hack to get hooks inside
         *                      module cron functions to run!!
         *                      Cron flag should not be used anywhere
         *                      else, we might get infinite loops....
         */
            $this->in_progress = TRUE;
        }
        // -----------------------------------
        // Call the requested class and/or function
        // -----------------------------------

        //Changed to support multible hooks

        if ($class !== FALSE) {
            if ( ! class_exists($class)) {
                require($filepath);
            }

            $HOOK = new $class;
            $x = $HOOK->$function($params, $data);

        } else {
            if (!function_exists($function)) {
                require($filepath);
            }
            $x = $function($params, $data);
        }

        $this->in_progress = FALSE;

        if ($filter)
            return $x;
        else 
            return TRUE;
    }


    /**
     * Remove Hook
     *
     * remove a particular function from the given hook
     *
     * @access  private
     * @param   string  the hook name
     * @param   string  the function name
     * @return  void
     */
    function remove($hook_name = '', $function='') {
        foreach($this->hooks[$hook_name] as $key => $hook) {
            if($hook['function'] == $function) {
                unset($this->hooks[$hook_name][$key]);
                return;
            }
        }
    }
    
}

// END MY_Hooks class

/* End of file Hooks.php */
/* Location: application/libraries/MY_Hooks.php */