<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH."third_party/MX/Router.php";

class MY_Router extends MX_Router {

	var $error_controller = 'error';
	var $error_method_404 = 'error_404';

        function  __construct() {
            parent::__construct();
        }

	// this is just the same method as in Router.php, with show_404() replaced by $this->error_404();
	function _validate_request($segments) {

                if (count($segments) == 0) return $segments;

		/* locate module controller */
		if ($located = $this->locate($segments)) return $located;

		/* use a default 404_override controller */
		if (isset($this->routes['404_override']) AND $this->routes['404_override']) {
			$segments = explode('/', $this->routes['404_override']);
			if ($located = $this->locate($segments)) return $located;
		}

		// Can't find the requested controller...
		return $this->error_404();
	}

	function error_404()
	{
		$this->directory = "";
		$segments = array();
		$segments[] = $this->error_controller;
		$segments[] = $this->error_method_404;
		return $segments;
	}

	function fetch_class()
	{
		// if method doesn't exist in class, change
		// class to error and method to error_404
		$this->check_method();

		return $this->class;
	}

	function check_method()
	{
		$ignore_remap = true;

		$class = $this->class;
		if (class_exists($class))
		{
			// methods for this class
			$class_methods = array_map('strtolower', get_class_methods($class));

			// ignore controllers using _remap()
			if($ignore_remap && in_array('_remap', $class_methods))
			{
				return;
			}

			if (! in_array(strtolower($this->method), $class_methods))
			{
				$this->directory = "";
				$this->class = $this->error_controller;
				$this->method = $this->error_method_404;
				include(APPPATH.'controllers/'.$this->fetch_directory().$this->error_controller.EXT);
			}
		}
	}

	function show_404()
	{
		include(APPPATH.'controllers/'.$this->fetch_directory().$this->error_controller.EXT);
		call_user_func(array($this->error_controller, $this->error_method_404));
	}

}

/* End of file MY_Router.php */
/* Location: ./system/application/libraries/MY_Router.php */