<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Session extends CI_Session {
   /*
    * Do not update an existing session on ajax calls
    *
    * http://stackoverflow.com/questions/6856960/codeigniter-session-expires-frequently
    * http://ellislab.com/forums/viewthread/172415/
    *
    * @access    public
    * @return    void
    */
    public function sess_update() {
        if ( ! $this->input->is_ajax_request() ) {
            parent::sess_update();
        }
    }

    function sess_destroy() {
        parent::sess_destroy();
        $this->userdata = array();
    }
} 