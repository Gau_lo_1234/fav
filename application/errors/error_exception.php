<!DOCTYPE html>
<html>
<head>
  <title>Error</title>
  <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/style.css" />
  <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/jquery_ui_themes/base/jquery.ui.all.css" />
  <script type="text/javascript" src="<?php echo ASSET_URL ?>js/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSET_URL ?>js/jquery-ui-1.9.1.custom.min.js"></script>
  <script>
  $(function () {
    $('.button').button();
  });
  </script>
</head>
<body>
  <div id="wrapper">
    <div id="header">
    <a href="/"><img src="<?php echo ASSET_URL ?>images/logo.png"/></a>
    </div><!--header-->

    <div id="master_container">

        <div id="content">
            <h2>Something unexpected happened.</h2>
            <?php echo $um; ?>
            <p>We have found some error please try again later.</p>
            <?php if ($_SERVER['REQUEST_URI'] != '/') { ?>
                <a href="<?php echo site_url() ?>" class="button"><img src="<?php echo ASSET_URL ?>images/back.png" /> Go back to Dashboard</a></p>
            <?php } ?>

            <?php if ($debug_print) { ?>
                <br/>
                <h3>Error Details</h3>
                <?php echo $msg; ?>
                <br/>
                <br/>
                <p><b>Email:</b> <?php echo $mail_success; ?></p>
                <?php 
                // foreach ($error as $key=>$value) { 
                //     echo $key." - ".$value."<br/>";
                // }
            }
            ?>
        </div>

        <br class="clrflt"/>
        <br class="clrflt"/>
    </div>
    <div id="footer">

    <p><?php echo "Copyright Onlineguarding .".date('Y'); ?></p>
    </div><!--footer-->
  </div><!-- wrapper -->

</body>
</html>

