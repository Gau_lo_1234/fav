<!DOCTYPE html>
<html>
<head>
  <title>Error</title>
  <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/style.css" />
  <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/jquery_ui_themes/base/jquery.ui.all.css" />

</head>
<body>
  <div id="wrapper">
    <div id="header">
	<a href="/"><img src="<?php echo ASSET_URL ?>images/logo.png"/></a>
    </div><!--header-->

    <div id="navigation">
	<?php
	//echo $this->load->view('elements/navigation');
        ?>
    </div><!-- navigation -->
    <div id="master_container">

	<div id="content">
		<h2><?php echo $heading; ?></h2>
		<?php echo $message; ?>
	</div>

        <br class="clrflt"/>
        <br class="clrflt"/>
    </div>
    <div id="footer">

	<p><?php echo "Copyright Onlineguarding .".date('Y'); ?></p>
    </div><!--footer-->
  </div><!-- wrapper -->

</body>
</html>