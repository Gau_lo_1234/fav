<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists("get_last_day")) {
/**
 * get_last_day
 * 
 * Returns the last day of the current month. Or month specified.
 * 
 * @param int $year (optional)
 * @param int $month (optional)
 *
 * @access public
 *
 * @return int - unixtimestamp
 */
function get_last_day($year=null, $month=null){
    if (is_null($year)) {
        $year = date('Y');
    }
    if (is_null($month)) {
        $month = date('m');
    }

    $timestamp = strtotime("$year-$month-01");
    $number_of_days = date('t',$timestamp);
    return $number_of_days;
}
}

if (!function_exists("get_last_day_ts")) {
/**
 * get_last_day_ts
 * 
 * @param int $year (optional)
 * @param int $month (optional)
 *
 * @access public
 *
 * @return int - unixtimestamp
 */
function get_last_day_ts($year=null, $month=null){
    if (is_null($year)) {
        $year = date('Y');
    }
    if (is_null($month)) {
        $month = date('m');
    }

    $timestamp = strtotime("$year-$month-01");
    $number_of_days = date('t',$timestamp)-1;

    return $timestamp+$number_of_days*24*60*60;
}
}

if (!function_exists("get_last_day_next_month_ts")) {
/**
 * get_last_day_next_month_ts
 * 
 * @param int $year (optional)
 * @param int $month (optional)
 *
 * @access public
 *
 * @return int - unixtimestamp
 */
function get_last_day_next_month_ts($year=null, $month=null){
    if (is_null($year)) {
        $year = date('Y');
    }
    if (is_null($month)) {
        $month = date('m')+1;
        if ($month == 13) {
            $month = 1;
            $year++;
        }
    }

    $timestamp = strtotime("$year-$month-01");
    $number_of_days = date('t',$timestamp)-1;

    return $timestamp+$number_of_days*24*60*60;
}
}

if (!function_exists("get_last_day_prev_month_ts")) {
/**
 * get_last_day_next_month_ts
 * 
 * @param int $year (optional)
 * @param int $month (optional)
 *
 * @access public
 *
 * @return int - unixtimestamp
 */
function get_last_day_prev_month_ts($year=null, $month=null){
    if (is_null($year)) {
        $year = date('Y');
    }
    if (is_null($month)) {
        $month = date('m')-1;
        if ($month <= 0) {
            $month = 12;
            $year--;
        }
    }

    $timestamp = strtotime("$year-$month-01");
    $number_of_days = date('t',$timestamp)-1;

    return $timestamp+$number_of_days*24*60*60;
}
}


if (!function_exists("display_seconds")) {
    /**
     * display_seconds
     * 
     * @param int $seconds unixtimestamp
     *
     * @access public
     *
     * @return x day/s H:i:s
     */
    function display_seconds ($seconds) {
        //http://codeaid.net/php/convert-seconds-to-hours-minutes-and-seconds-%28php%29
        $days = floor ($seconds / 86400);
        $diff = $seconds - ($days*86400);
        if ($days > 1) // 2 days+, we need days to be in plural
        {
            return $days . ' days ' . ($diff > 0 ? gmdate ('H:i:s', $seconds) : '' );
        }
        else if ($days > 0) // 1 day+, day in singular
        {
            return $days . ' day ' . ($diff > 0 ? gmdate ('H:i:s', $seconds) : '' );
        }

        return gmdate ('H:i:s', $seconds);
    }
}

if (!function_exists("show_ordinal")) {
    /**
     * show_ordinal
     * 
     * @param int  $num        
     * @param string $tags_open  (optional).
     * @param string $tags_close (optional).
     *
     * @access public
     *
     * @return 1st || 2nd || etc
     */
    function show_ordinal($num, $tags_open="<sup>", $tags_close="</sup>") {
        $the_num = (string) $num;
        $last_digit = substr($the_num, -1, 1);
        switch($last_digit) {
            case "1":
                $the_num.=$tags_open."st".$tags_close;
                break;
            case "2":
                $the_num.=$tags_open."nd".$tags_close;
                break;
            case "3":
                $the_num.=$tags_open."rd".$tags_close;
                break;
            default:
                $the_num.=$tags_open."th".$tags_close;
        }
        return $the_num;
    }
}

/* End of file MY_date_helper.php */
/* Location: ./application/helpers/MY_date_helper.php */