<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!defined("TEST_EMAIL_ACTIVE")) { define("TEST_EMAIL_ACTIVE", 1); }
if (!defined("TEST_EMAIL_TO")) { define("TEST_EMAIL_TO", "elsabe"); }

/**
 * sendMail
 *
 *  Just a wrapper so we don't need to config email library everywhere
 *
 * @param string $email     To address
 * @param string $subject     Subject of email
 * @param string $message     HTML message body.
 * @param string $alt_message   Plain text message
 * @param array $attachments    (optional)
 *
 * @access public
 *
 * @return boolean
 */
if (!function_exists('sendMail')) {
function sendMail($email, $subject, $message, $alt_message, $attachments=array(), $debugger=false) {
    $ci = & get_instance();

    $config = array('mailtype'=>'html');
    if (isset($ci->m_settings)) {
        $email_settings = $ci->m_settings->get_by_group('email');
        $newline_character = (isset($email_settings['mail_newline']) && $email_settings['mail_newline'] == '\r\n' ? "\r\n" : "\n");

        if (isset($email_settings['protocol']) && $email_settings['protocol'] == 'smtp') {
            $config = array(
                          'mailtype'=>'html',
                          'protocol' => $email_settings['protocol'],
                          'smtp_host' => $email_settings['smtp_host'],
                          'smtp_port' => $email_settings['smtp_port'],
                          'smtp_user' => $email_settings['smtp_user'],
                          'smtp_pass' => $email_settings['smtp_pass'],
            );
        }
    } else {
        $email_settings['webmaster'] = 'OG';
        $newline_character = "\r\n";
    }

    if (!isset($ci->email)) {
            $ci->load->library('email', $config);
            $ci->email->set_newline($newline_character);
    }

    $mailer = new CI_Email($config);
    $mailer->set_newline($newline_character);

    $mailer->from($email_settings['webmaster'], $ci->config->item('website_name', 'tank_auth'));
    $mailer->reply_to($email_settings['webmaster'], $ci->config->item('website_name', 'tank_auth'));

    if (TEST_EMAIL_ACTIVE == 1) {
        $mailer->to(TEST_EMAIL_TO);
        $message .= "\n\n Originally to:".$email;
    } else {
        $mailer->to($email);
    }
    $mailer->subject($subject);
    $mailer->message($message);
    $mailer->set_alt_message($alt_message);

    if (count($attachments)) {
        foreach($attachments as $attachment) {
            $mailer->attach($attachment);
        }
    }

    $result = $mailer->send();
    if ($debugger && !$result ) {
            return $mailer->print_debugger();
    }
    return $result;
}
}

/**
 * can_email_user
 *
 * Checks if user has a verfied account /email address.
 *
 * @param object $data Object containing user detail.
 *
 * @access public
 *
 * @return boolean
 */
if (!function_exists('can_email_user')) {
function can_email_user($data) {
    if ($data->activated) {
            if ($data->banned) {
                return false;
            } else if (!$data->verified || $data->verified == 255) {
                return false;
            } else if ($data->new_email && $data->email != $data->new_email) {
                return false;
            } else {
                return true;
            }
        }
        return false;
}
}

if (!function_exists('generate_email_key')) {
function generate_email_key($data) {
    $s = serialize($data);
    return rtrim(base64_encode($s), '=');
}
}

if (!function_exists('decode_email_key')) {
function decode_email_key($key) {
    $s = base64_decode($key);
    return unserialize($s);
}
}

/* End of file MY_email_helper.php */
/* Location: ./application/helpers/MY_email_helper.php */