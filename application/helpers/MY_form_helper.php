<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * is_valid_phone
 * 
 * To cater for multiple countries we just check that the
 * $str is a bunch of numbers. A plus as the first character
 * is permissable.
 * 
 * @param string $str Possible telephone number that needs to be checked.
 *
 * @access public
 *
 * @return boolean
 */
function is_valid_phone($str) {
    $tmp = preg_replace('/\s/', '', $str);
    // if (preg_match( "/(^\+27[0-9]{9})$|^([0-9]{10})$/",$tmp) != 0) {
    if (is_numeric($tmp) && strlen($tmp) < 20) { //globalizing
        return TRUE;
    } else {
        return FALSE;
    }
}

/* End of file MY_form_helper.php */
/* Location: ./application/helpers/MY_form_helper.php */