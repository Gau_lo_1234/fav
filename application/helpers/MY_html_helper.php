<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * show_message_box
 * 
 * @param string $type          Either error or notice
 * @param string $message       The message that will be displayed in the box.
 * @param boolean $return_string    Return string or echo out (default)
 *
 * @access public
 *
 * @return string  or echoes string
 */
function show_message_box($type, $message, $return_string=FALSE) {
    $s = '<div class="flash">
            <div class="message '.$type.'"><p>'.$message.'</p></div>
            </div>';
    if ($return_string) {
        return $s;
    } else {
        echo $s;
    }
}

/**
 * data_uri
 * 
 * Return the Data URI for a image or other resource.
 * SEE: http://en.wikipedia.org/wiki/Data_URI_scheme
 * 
 * @param string $file The filename/path to the file to be encoded
 * @param string $mime 
 *
 * @access public
 *
 * @return string data uri
 */
function data_uri($file, $mime) {
    $contents = file_get_contents($file);
    $base64 = base64_encode($contents);
    return "data:$mime;base64,$base64";
}

/* End of file MY_html_helper.php */
/* Location: ./application/helpers/MY_html_helper.php */