<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Contains functions to handle running a scrip in the background.
 * Resource: http://nsaunders.wordpress.com/2007/01/12/running-a-background-process-in-php/
 */

/**
 * run_in_background
 * 
 * Run a command in a seperate process.
 * 
 * @param string $Command  The command that needs to be run.
 * @param int   $Priority Sets the priority level to run at.
 *
 * @access public
 *
 * @return int PID
 */
function run_in_background($Command, $Priority = 0) {
   if($Priority)
       $PID = shell_exec("nohup nice -n $Priority $Command > /dev/null 2> /dev/null & echo $!");
   else
       $PID = shell_exec("nohup $Command > /dev/null 2> /dev/null & echo $!");
   return($PID);
}

/**
 * is_process_running
 * 
 * Check if a PID is still running.
 * 
 * @param int $PID Process ID
 *
 * @access public
 *
 * @return boolean
 */
function is_process_running($PID) {
   exec("ps $PID", $ProcessState);
   return(count($ProcessState) >= 2);
}


/* End of file background_helper.php */
/* Location: ./application/helpers/background_helper.php */