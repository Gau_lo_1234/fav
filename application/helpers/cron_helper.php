<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('LOCK_DIR', APPPATH.'cache/');
define('LOCK_SUFFIX', '.lock');

/**
* CronHelper
*
* Locking functionality that is used in conjunction with cron scripts.
* 
* @category Cron
* @package  OnlineGuarding
*/
class CronHelper {
    private static $pid;

    function __construct() {}

    function __clone() {}

    private static function isrunning() {
        $pids = explode(PHP_EOL, `ps -e | awk '{print $1}'`);
        if(in_array(self::$pid, $pids))
        return TRUE;
        return FALSE;
    }

    public static function lock($identifier) {
        $lock_file = LOCK_DIR.$identifier.LOCK_SUFFIX;

        if(file_exists($lock_file)) {
            //return FALSE;

            // Is running?
            self::$pid = file_get_contents($lock_file);
            if(self::isrunning()) {
                error_log("==".self::$pid."== Already in progress...");
                return FALSE;
            }
            else {
                error_log("==".self::$pid."== Previous job died abruptly...");
            }
        } else {
            $newfile = TRUE;
        }

        self::$pid = getmypid();

        file_put_contents($lock_file, self::$pid);
        if (isset($newfile) AND $newfile === TRUE) {
            @chmod($lock_file, FILE_WRITE_MODE);
        }

        error_log("==".self::$pid."== Lock acquired, processing the job...");
        return self::$pid;
    }

    public static function unlock($identifier) {
        $lock_file = LOCK_DIR.$identifier.LOCK_SUFFIX;

        if(file_exists($lock_file)) {
            unlink($lock_file);
        }

        error_log("==".self::$pid."== Releasing lock...");
        return TRUE;
    }

}


/* End of file cron_helper.php */
/* Location: ./application/helpers/cron_helper.php */