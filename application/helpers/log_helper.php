<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists("write_log")) {
    /**
     * write_log
     *
     * @param string  $filename    Filename of the log file.
     * @param string $log_message The content written to the log file
     *
     * @access public
     *
     * @return mixed Value.
     */
    function write_log($filename, $log_message="") {
        // open log file
        if (!file_exists($filename)) {
            $log_message = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>\n\n".$log_message;
        }
        $fh = fopen($filename, "a+");
        if ($fh) {
            fwrite($fh, $log_message);
            fclose($fh);
        } else {
            echo "No log file: \n";
            echo $log_message;
        }
    }
}

if (!function_exists("get_memory_usage")) {
    function get_memory_usage()
    {
        $size = memory_get_usage(true);
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }
}