<?php
/*
 * Helper to use with Google's Recaptcha V2
 *
 * Date: 22/06/2017
 * Documentation: https://developers.google.com/recaptcha/docs/
 */

function recaptcha_add_js() {
  $CI =& get_instance();
  $CI->pixie_javascript->add_script('api.js', 'https://www.google.com/recaptcha', true);
}

/**
 * This is called from the browser, and the resulting reCAPTCHA HTML widget
 * is embedded within the HTML form it was called from.
 * @param string $pubkey A public key for reCAPTCHA
 * @return string - The HTML to be embedded in the user's form.
 */
function recaptcha_get_html($pubkey) {
    if ($pubkey == null || $pubkey == '') {
        die ("To use reCAPTCHA you must get an API key from <a href='https://www.google.com/recaptcha/'>https://www.google.com/recaptcha/</a>");
    }
    return '<div class="g-recaptcha" data-sitekey="'.$pubkey.'"></div>';
}


/**
 * A ReCaptchaResponse is returned from recaptcha_check_answer()
 */
class ReCaptchaResponse {
    var $is_valid;
    var $error;
}


/**
  * Calls an HTTP POST function to verify if the user's guess was correct
  * @param string $secret
  * @param string $remoteip
  * @param string $response
  * @return ReCaptchaResponse
  */
function recaptcha_verify_response($secret, $remoteip, $response) {
    if ($secret == null || $secret == '') {
        die ("To use reCAPTCHA you must get an API key from <a href='https://www.google.com/recaptcha/'>https://www.google.com/recaptcha/</a>");
    }

    if ($remoteip == null || $remoteip == '') {
        die ("For security reasons, you must pass the remote ip to reCAPTCHA");
    }

    $response = _recaptcha_http_post("https://www.google.com/recaptcha/api/siteverify",
        array(
            'secret'=>$secret,
            'response'=>$response,
            'remoteip'=>$remoteip
        )
    );
    // print_r($response);

    $recaptcha_response = new ReCaptchaResponse();

    if ($response['success']) {
        $recaptcha_response->is_valid = true;
    } else {
        $recaptcha_response->is_valid = false;
        $recaptcha_response->error = implode('<br/>', $response['error-codes']);
    }
    return $recaptcha_response;
}

function _recaptcha_http_post($url, $post_data) {
    // Create cURL
    $ch = curl_init();
    // If we're shortening a URL...
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$post_data);
    // curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    // Execute the post
    $result = curl_exec($ch);

    // Close the connection
    curl_close($ch);
    // Return the result
    return json_decode($result,true);
}


?>
