<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * process_register_form
 *
 * @param int $distributor_id
 * @param array $data
 *
 * @access public
 *
 * @return array
 */
function process_register_form($distributor_id, $data) {
    $CI = &get_instance();
    $CI->load->model('m_client');
    $CI->load->model('m_distributor');
    $CI->load->model('m_site');
    $CI->load->model('m_report_type');

    $CI->load->helper('email');
    $CI->load->helper('util');

    /* User rules */
    $use_username = $CI->config->item('use_username', 'tank_auth');
    // if ($use_username) {
    //         $CI->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length['.$CI->config->item('username_min_length', 'tank_auth').']|max_length['.$CI->config->item('username_max_length', 'tank_auth').']|alpha_dash');
    // }
    $CI->form_validation->set_rules('client_name', $CI->lang->line('client_name'), 'trim|required|xss_clean');
    $CI->form_validation->set_rules('contact_person', $CI->lang->line('contact_person'), 'trim|xss_clean|required');
    $CI->form_validation->set_rules('cellphone', $CI->lang->line('cell'), 'trim|xss_clean|required');
    $CI->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
    $CI->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length['.$CI->config->item('password_min_length', 'tank_auth').']|max_length['.$CI->config->item('password_max_length', 'tank_auth').']|alpha_dash');
    $CI->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]');

    /* Site */
    $CI->form_validation->set_rules('site_name', $CI->lang->line('site_name'), 'trim|required|xss_clean');
    $CI->form_validation->set_rules('magcell_code', $CI->lang->line('magcell_code'), 'trim|xss_clean|required|check_magcell_code');

    /* Reports */
    $CI->form_validation->set_rules('additional_emails', $CI->lang->line('additional_emails'), 'trim|xss_clean');
    if ($CI->input->post('additional_emails') > 0) {
        for ($i=1; $i <= $CI->input->post('additional_emails'); $i++) {
            $CI->form_validation->set_rules('report_emails_'.$i, 'email address', 'trim|xss_clean|required|valid_email');
        }
    }

    $captcha_registration = $CI->config->item('captcha_registration', 'tank_auth');
    $use_recaptcha = $CI->config->item('use_recaptcha', 'tank_auth');
    if ($captcha_registration) {
            if ($use_recaptcha) {
                    $CI->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
            } else {
                    $CI->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
            }
    }
    if (!isset($data['errors'])) $data['errors'] = array();

    $email_activation = $CI->config->item('email_activation', 'tank_auth');

    if (!empty($_POST)) {
        if ($CI->input->post('cellphone') &&
                !is_valid_phone($CI->input->post('cellphone'))) {
            $data['errors']['cellphone'] = $CI->lang->line('auth_invalid_cellphone');
        }
        if ($CI->input->post('client_telephone') &&
                !is_valid_phone($CI->input->post('client_telephone'))) {
            $data['errors']['client_telephone'] = $CI->lang->line('auth_invalid_cellphone');
        }
        if ($CI->input->post('client_alt_telephone') &&
                !is_valid_phone($CI->input->post('client_alt_telephone'))) {
            $data['errors']['client_alt_telephone'] = $CI->lang->line('auth_invalid_cellphone');
        }

        if ($CI->input->post('email') &&
                !$CI->tank_auth->is_email_available($CI->input->post('email'))) {
            $data['errors']['email'] = $CI->lang->line('auth_email_in_use');

            $data['email_exists'] = true;
        }

        if ($CI->input->post('additional_emails') > 0) {
            for ($i=1; $i <= $CI->input->post('additional_emails'); $i++) {
                if ($CI->input->post('report_emails_'.$i) &&
                        !$CI->tank_auth->is_email_available($CI->input->post('report_emails_'.$i))) {
                    $data['errors']['report_emails_'.$i] = 'Email is already used by another user. Please choose another email. ';
                }
            }
        }
    }


    if ($CI->form_validation->run() && empty($data['errors'])) {	// validation ok
        if (!isset($distributor_id)) $distributor_id = $CI->form_validation->set_value('distributor_id');

            if ($use_username) {
                $username = create_handle($CI->form_validation->set_value('contact_person'));
            } else {
                $username = '';
            }

            if (!is_null($data = $CI->tank_auth->create_user(
                        $username,
                        $CI->form_validation->set_value('email'),
                        $CI->form_validation->set_value('password'),
                        $CI->form_validation->set_value('cellphone'),
                        $email_activation,
                        'user',
                        $distributor_id
                ))) {									// success

                $data['site_name'] = $CI->config->item('website_name', 'tank_auth');

                // ADD CLIENT CREATION
                $client_id = $CI->m_client->create(
                        $CI->form_validation->set_value('client_name'),
                        '',//$CI->form_validation->set_value('client_desc'),
                        $CI->form_validation->set_value('contact_person'),
                        $CI->form_validation->set_value('cellphone'),
                        '',//$CI->form_validation->set_value('client_alt_telephone'),
                        $CI->form_validation->set_value('email'),
                        $distributor_id
                 );

                // create a link between the user and client
                $CI->load->model('m_users_clients');
                $CI->m_users_clients->add_link($data['user_id'], $client_id);

                if ($CI->form_validation->set_value('magcell_code')) {
                    $CI->m_client->add_magcell_code($client_id, $CI->form_validation->set_value('magcell_code'));
                    $codes = array($CI->form_validation->set_value('magcell_code'));
                } else {
                    $codes = array();
                }

                $CI->hooks->call('client_add', array('client_id'=>$client_id));

                // ADD SITE CREATION
                $site_id = $CI->m_site->create(
                         $CI->form_validation->set_value('site_name'),
                         $codes,
                         $client_id);

                // ADD REPORTS CREATION
                $detail_report = $CI->m_report_type->get_by_handle('detail');
                $overview_report = $CI->m_report_type->get_by_handle('overview');

                $CI->m_site_email->create(
                                    $site_id,
                                    $detail_report->id,
                                    $data['user_id'],
                                    1,//daily
                                    7,0,0,null
                                    );
                $CI->m_site_email->create(
                                    $site_id,
                                    $overview_report->id,
                                    $data['user_id'],
                                    1,//daily
                                    7,0,0,null
                                    );

                if ($CI->input->post('additional_emails') > 0) {
                    for ($i=1; $i <= $CI->input->post('additional_emails'); $i++) {
                        if ($CI->input->post('report_emails_'.$i) &&
                                !$CI->tank_auth->is_email_available($CI->input->post('report_emails_'.$i))) {
                            $data['errors']['report_emails_'.$i] = 'Email is already used by another user. Please choose another email. ';
                        }
                    }
                }

                 if ($CI->input->post('additional_emails') > 0) {
                    $username_base = create_handle($CI->form_validation->set_value('client_name'));
                    for ($i=1; $i <= $CI->input->post('additional_emails'); $i++) {
                        $field = 'report_emails_'.$i;

                        if ($CI->form_validation->set_value($field)) {
                            $username = $username_base.'_'.$i;

                            $password = uniqid(); // just some random temporary password.

                            // Create email user
                            if (!is_null($email_user = $CI->tank_auth->create_user(
                                       $use_username ? $username : '',
                                       $CI->form_validation->set_value($field),
                                       $password,
                                       '', //cellphone
                                       $email_activation,
                                       'email_user',
                                       $distributor_id
                                       ))) {
                                // success
                                $CI->m_users_clients->add_link($email_user['user_id'], $client_id);

                                if ($email_activation) {                                    // send "activate" email
                                    $email_user['activation_period'] = display_seconds($CI->config->item('email_activation_expire', 'tank_auth'));
                                    $email_user['site_name'] = $CI->config->item('website_name', 'tank_auth');

                                    if ($CI->m_permissions_roles->check_role('no_password', 'email_user') ) {
                                        $email_type = 'activate_no_password';
                                    } else {
                                        $email_type = 'activate_add_user';
                                    }

                                    $subject = sprintf($CI->lang->line('auth_subject_'.$email_type), $CI->config->item('website_name', 'tank_auth'));
                                    $message = $CI->load->view('email/'.$email_type.'-html', $email_user, TRUE);
                                    $alt_message = $CI->load->view('email/'.$email_type.'-txt', $email_user, TRUE);

                                    sendMail($email_user['email'], $subject, $message, $alt_message);

                                    unset($email_user['password']); // Clear password (just for any case)
                                }// email_activation

                                // Add reports to email user
                                $CI->m_site_email->create(
                                                    $site_id,
                                                    $detail_report->id,
                                                    $email_user['user_id'],
                                                    1,//daily
                                                    7,0,0,null
                                                    );
                                $CI->m_site_email->create(
                                                    $site_id,
                                                    $overview_report->id,
                                                    $email_user['user_id'],
                                                    1,//daily
                                                    7,0,0,null
                                                    );
                            }//if create users
                        }//if not emprtyfield
                    }//for
                }

                $CI->m_logger->log(
                        $data['user_id'],
                        'regsiter_new_user',
                        'New user registered and new client created',
                        array('client_id'=>$client_id),
                        null,
                        $client_id
                );

                // SEND EMAIL TO DISTRIBUTOR

                $data['client'] = array(
                    'id'=>$client_id,
                    "name"=>$CI->form_validation->set_value('client_name'),
                    "desc"=>$CI->form_validation->set_value('client_desc'),
                    "contact_person"=>$CI->form_validation->set_value('client_contact_person'),
                    "telephone"=>$CI->form_validation->set_value('client_telephone'),
                    "alt_telephone"=>$CI->form_validation->set_value('client_alt_telephone'),
                    "email"=>$CI->form_validation->set_value('client_email'),
                );

                // Get email for distributor.
                // 1. Distributor email OR all distributor users.
                $users = $CI->users->get_list_by_distributor($distributor_id, true);
                if (isset($users['distrib'])) { // we have distributor users
                    foreach($users['distrib'] as $distrib) {

                        $CI->load->helper('email');

                        $type = 'distributor_registration';
                        $subject = sprintf($CI->lang->line('auth_subject_'.$type), $CI->config->item('website_name', 'tank_auth'));
                        $message = $CI->load->view('email/'.$type.'-html', $data, TRUE);
                        $alt_message = $CI->load->view('email/'.$type.'-txt', $data, TRUE);

                        sendMail($distrib->email, $subject, $message, $alt_message);
                    }
                }
                // we'll send it to the email of the distributor.
                if (!isset($distributor)) {
                    $distributor = $CI->m_distributor->get_by_id($distributor_id);
                }
                if ($distributor->email) {

                    $CI->load->helper('email');

                    $type = 'distributor_registration';
                    $subject = sprintf($CI->lang->line('auth_subject_'.$type), $CI->config->item('website_name', 'tank_auth'));
                    $message = $CI->load->view('email/'.$type.'-html', $data, TRUE);
                    $alt_message = $CI->load->view('email/'.$type.'-txt', $data, TRUE);

                    sendMail($distributor->email, $subject, $message, $alt_message);

                }

                if ($email_activation) {									// send "activate" email
                        $data['activation_period'] = $CI->config->item('email_activation_expire', 'tank_auth') / 3600;

                        $CI->load->helper('email');

                        $type = 'activate';
                        $subject = sprintf($CI->lang->line('auth_subject_'.$type), $CI->config->item('website_name', 'tank_auth'));
                        $message = $CI->load->view('email/'.$type.'-html', $data, TRUE);
                        $alt_message = $CI->load->view('email/'.$type.'-txt', $data, TRUE);

                        sendMail($data['email'], $subject, $message, $alt_message);

                        unset($data['password']); // Clear password (just for any case)

                        _show_message($CI->lang->line('auth_message_registration_completed_1'));

                } else {
                        if ($CI->config->item('email_account_details', 'tank_auth')) {	// send "welcome" email

                                $CI->load->helper('email');

                                $type = 'welcome';
                                $subject = sprintf($CI->lang->line('auth_subject_'.$type), $CI->config->item('website_name', 'tank_auth'));
                                $message = $CI->load->view('email/'.$type.'-html', $data, TRUE);
                                $alt_message = $CI->load->view('email/'.$type.'-txt', $data, TRUE);

                                sendMail($data['email'], $subject, $message, $alt_message);
                        }
                        unset($data['password']); // Clear password (just for any case)

                        _show_message($CI->lang->line('auth_message_registration_completed_2').' '.anchor('/auth/login/', 'Login'));
                }

        } else {
                $errors = $CI->tank_auth->get_error_message();
                foreach ($errors as $k => $v)	$data['errors'][$k] = $CI->lang->line($v);
        }

    }// run form_validation
    if ($captcha_registration) {
            $data['captcha_html'] = _create_captcha();
    }

    $data['use_username'] = $use_username;
    $data['captcha_registration'] = $captcha_registration;
    $data['use_recaptcha'] = $use_recaptcha;

    return $data;
}

/**
 * Create CAPTCHA image to verify user as a human
 *
 * @return	string
 */
function _create_captcha()
{
        $CI = &get_instance();
        $CI->load->helper('captcha');

        $cap = create_captcha(array(
                'img_path'		=> './'.$CI->config->item('captcha_path', 'tank_auth'),
                'img_url'		=> base_url().$CI->config->item('captcha_path', 'tank_auth'),
                'font_path'		=> './'.$CI->config->item('captcha_fonts_path', 'tank_auth'),
                'font_size'		=> $CI->config->item('captcha_font_size', 'tank_auth'),
                'img_width'		=> $CI->config->item('captcha_width', 'tank_auth'),
                'img_height'	=> $CI->config->item('captcha_height', 'tank_auth'),
                'show_grid'		=> $CI->config->item('captcha_grid', 'tank_auth'),
                'expiration'	=> $CI->config->item('captcha_expire', 'tank_auth'),
        ));

        // Save captcha params in session
        $CI->session->set_flashdata(array(
                        'captcha_word' => $cap['word'],
                        'captcha_time' => $cap['time'],
        ));

        return $cap['image'];
}

/**
 * Show info message
 *
 * @param	string
 * @return	void
 */
function _show_message($message)
{
        $CI = &get_instance();
        $CI->session->set_flashdata('message', $message);
        $CI->session->set_flashdata('message_type', 'notice');
        redirect('/auth/login');
}

/* End of file register_helper.php */
/* Location: ./application/helpers/register_helper.php */