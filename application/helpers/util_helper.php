<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
*
*   Function to display var_dump in a clean format
* @param array $var      array of data to format
* @param string $label   label to give the resulting output header
* @param string $echo    This is the output type to return
* @return echo's output or returns string for further display
*/

    function dump ($var, $label = 'Dump', $echo = TRUE){
      // Store dump in variable 
      ob_start();
      var_dump($var);
      $output = ob_get_clean();
      
      // Add formatting
      $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
      $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';
      
      // Output
      if ($echo == TRUE) {
          echo $output;
      }
      else {
          return $output;
      }
    }


    function isValidJson($data){
          json_decode($data);

          if(json_last_error()===JSON_ERROR_NONE){
            return true;
          }else{
            return false;
          }      
    }

  if (!function_exists("create_handle")) {
    /**
     * create_handle
     *
     * Removes non-alpha-numeric and whitespace
     * replaced with _
     *
     * @param string $str
     *
     * @access public
     *
     * @return string
     */
    function create_handle($str) {
        $patterns = array (
          '/\W+/', // match any non-alpha-numeric character sequence, except underscores
          '/\s+/',  // match any number of white spaces
           '/_+/',  // match any number of underscores
        );

        $replaces = array (
          '_', // remove
          '_', // remove
          '_' // leave only 1 space
        );

        return strtolower(trim(preg_replace($patterns, $replaces, $str) ));
    }
  }

if (!function_exists("display_name_from_handle")) {
  /**
   * display_name_from_handle
   *
   * Capatalizes and removes _
   *
   * @param string $str
   *
   * @access public
   *
   * @return string
   */
function display_name_from_handle($str) {
    $patterns = array (
       '/_+/',  // match any number of underscores
    );

    $replaces = array (
      ' ' // leave only 1 space
    );

    return ucwords(preg_replace($patterns, $replaces, $str) );
}
}

if (!function_exists("query_string_to_array")) {
  /**
   * query_string_to_array
   *
   * Takes a query string and returns an array.
   * Used mainly by hte magcell munch, the string is weird.
   * parse_string doens't behave correctly, hence this
   * function
   *
   * KNOWN DEFECT: If content contains '=' this will only
   * read the piece of content before the =
   *
   * @param string $postdata query_string.
   *
   * @access public
   *
   * @return Array
   */
function query_string_to_array($postdata) {
    $data = array();
    $pieces = explode('&', $postdata);
    foreach ($pieces as $piece) {
        $x = explode('=', $piece);
        $data[$x[0]] = isset($x[1]) ? trim($x[1]) : '';
    }
    return $data;
}
}

if (!function_exists("ends_with")) {
    /**
     * ends_with
     *
     * Checks if $Haystack ends with $Needle
     *
     * @param string $Haystack
     * @param string $Needle
     *
     * @access public
     *
     * @return boolean
     */
function ends_with($Haystack, $Needle){
    // Recommended version, using strpos
    return strrpos($Haystack, $Needle) === strlen($Haystack)-strlen($Needle);
}
}

if (!function_exists("add_char_to_string")) {
    /**
     * add_char_to_string
     *
     *  Adds $str_to_insert  to $oldstr at position $pos.
     *
     * @param string $oldstr        Description.
     * @param string $str_to_insert Description.
     * @param ing $pos           Description.
     *
     * @access public
     *
     * @return string
     */
function add_char_to_string($oldstr, $str_to_insert, $pos) {
  return substr($oldstr, 0, $pos) . $str_to_insert . substr($oldstr, $pos);
}
}

/**
   *
   * Function preint()
   *
   * @description Formats a string to a specific class, preserving white space. Useful for printing out arrays.
   * @access      public
   * @param     $data (string), $silent (boolean), $xmp (boolean), $class (string)
   * @return      formatted string.
   *
   */
if(!function_exists('preint')){
function preint($data, $xmp = true, $silent = false, $class = '', $style = '') {
    $o = "<div class='" . $class . "' style='" . $style . "'>";

    // Makes output silent (in <!-- --> tags)
    if ($silent) {
        $o .= "<!--";
    }

    // Favours XMP over PRE.
    if ($xmp) {
        $o .= "<xmp>";
    } else {
        $o .= "<pre>";
    }

    // Does the data check.
    if (isset($data)) {
        $o .= print_r($data, true);
    }

    // Favours XMP over PRE (default).
    if ($xmp) {
        $o .= "</xmp>";
    } else {
        $o .= "</pre>";
    }

    // Makes output silent (in <!-- --> tags)
    if ($silent) {
        $o .= "-->";
    }
    $o .= '</div>';
    print $o;
}
}

function disp_dump($data = array()){
    return highlight_string("<?php\n\$data =\n" . var_export($data, true) . ";\n?>");
}

/* End of file util_helper.php */
/* Location: ./application/helpers/util_helper.php */