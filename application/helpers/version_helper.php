<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * check_version
 * 
 * Pings the update url set in the config file.
 * 
 * @access public
 *
 * @return Array ( bool Success, string Message )
 */
function check_version() {
    $ci = &get_instance();
    $current = $ci->m_settings->get_by_group('ver');
    $tz = $ci->m_settings->get_by_key('system_timezone');
    $check_url = $ci->config->item('check_url', 'version')."?version=".$current['version']."&site_url=".urlencode(site_url('/'))."&tz=".urlencode($tz->value);
    $response = @file_get_contents($check_url);

//    echo $check_url."<br/>";
//    print_r($response);
    $data = array('false', $ci->lang->line('version_check_error_no_response'));

    if ($response) {
        $version_data = unserialize($response);
        if (isset($version_data['timezone']) && $version_data['timezone'] != '') {
            $ci->m_settings->set_key('system_timezone', $version_data['timezone'], 'sys');
        }
        if ($version_data['release_date'] > $current['release_date']) {
            $ci->m_settings->set_key('new_version', $version_data['version'], 'ver');
            $data = array(true, '');
        } else {
            $ci->m_settings->set_key('new_version', '', 'ver');
            $data = array(true, $ci->lang->line('version_check_no'));
        }
    } else {
        $error = $http_response_header[0];
        $data = array(false, sprintf($ci->lang->line('error_version_check'), $error));
    }

    return $data;

}

/* End of file version_helper.php */
/* Location: ./application/helpers/version_helper.php */