<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PHPFatalError {
    public function setHandler() {
        register_shutdown_function('handleShutdown');
    }
}

function handleShutdown() {
    $IGNORE_ERROR = false;
    if (($error = error_get_last())) {

        if (defined('CRON') && CRON) {
            $include_path = APPPATH;
        } else {
            $include_path = str_replace("/system", "", BASEPATH).APPPATH;
        }

        $buffer = ob_get_contents();
        @ob_clean();
        # raport the event, send email etc.

        $msg= $buffer;

        $um ='';
        ob_start();
        $data=array();
        $CI = get_instance();

        $debug_print=$CI->config->item('debug_print');

        //Send mail
        include($include_path.'helpers/MY_email_helper'.EXT);

        $subject = site_url()." Error Encountered";

        if (isset($CI->tank_auth)) {
            $user_info = $CI->tank_auth->get_user_id();
        } else {
            $user_info = 'not available';
        }
        $message = "<p>Server: ".site_url()."<p>Time: ".date('c')."</p><p>User: $user_info </p><p>Buffer Message : $msg </p>";
        foreach ($error as $key=>$value) {
            if (strpos($value, 'virtual_control_room.php') !== false && strpos($value, '171') !== false) { // ignore the vcr error.
                $IGNORE_ERROR = TRUE;
            } else {
            }
            $message .= "<p>$key: $value</p>";
        }

        if (!$IGNORE_ERROR) {

            if (isset($CI->m_settings)) {
                $setting = $CI->m_settings->get_by_key('error_email');
                if ($setting) {
                    $email = $setting->value;
                } else {
                    $email = 'elsabe@lessink.co.za';
                }
            } else {
                $email = 'elsabe@lessink.co.za';
            }

            if (sendMail($email, $subject, $message, $message)) {
                $mail_success = "Sent to: ".$email;
            } else {
                $mail_success =  "Error sending to: ".$email;
            }

        }

        if ($msg) {
            // include(str_replace("/system", "", BASEPATH).APPPATH.'errors/error_exception'.EXT);
            include($include_path.'errors/error_exception'.EXT);
            $buffer = ob_get_contents();
            ob_end_clean();
            echo $buffer;
        }

        exit();
        # from /error-capture, you can use another redirect, to e.g. home page
    }
}
