<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProfilerEnabler {
    function EnableProfiler() {
        $CI =& get_instance();
        $CI->output->enable_profiler( config_item('enable_profiling') );
    }
}

class Bootstraper {
    function LoadSettings() {
        $CI =& get_instance();
        $tz = '';
        if (isset($CI->m_settings)) {
            $tz = $CI->m_settings->get_by_key('system_timezone');
            if (is_null($tz)) {
                $CI->m_settings->set_key('system_timezone', 'GMT', 'sys');
                $tz = 'GMT';
            } else {
                $tz = $tz->value;
            }
        }
        // Set the site to default all date functions to Custom Setting!!
        // just making sure we do set a timezone.
        date_default_timezone_set ( $tz != '' ? $tz : 'GMT' );

        // Set database to GMT
        // TODO make it so it uses the $tz above, just setting it to UTC for now
        $CI->db->query('SET time_zone = "+00:00";');
    }

    function LoadModuleHooks() {
        $CI =& get_instance();
        $CI->hooks = load_class('Hooks');

        // Get active modules...
        // $modules = $CI->m_modules->get_active();
        // if ($modules) {
        //     foreach($modules as $module) {
        //         // check for hooks...
        //         $hook_file = APPPATH.'/modules/'.$module->module."/config/hooks.php";
        //         if (file_exists($hook_file)) {
        //             @include($hook_file);
        //         }
        //     }

        //     if ( ! isset($hook) OR ! is_array($hook)) {
        //             return;
        //     }
        //     $CI->hooks->add_hooks($hook);
        // }

        // Call the module initialize hook!
        //    $CI->hooks->call('module_initialize');
        // The above code was placed in Codeigniter.php after pre-controller call
        // the anti-looping feature prevents the hook from being called here.

    }

    function ReferrerTrack() {
        $CI =& get_instance();

        // Setup the referer session variable.
        if (!$CI->input->is_ajax_request() && isset($CI->session) && isset($CI->tank_auth) && $CI->tank_auth->is_logged_in()) {
            $CI->session->set_flashdata('referrer', current_url());
        }
    }

}

class InstallerChecker {
    function CheckInstalled() {
        // No database file? AAAGHHHH!
        if (isset($_SERVER["REQUEST_URI"])  && strpos($_SERVER["REQUEST_URI"], 'install') === FALSE) {
            if ( ! file_exists(APPPATH.'config/database'.EXT)) {
                    header('Location: '. BASE_URL.'installer/');
                    exit;
            }
        }
    }

    function CheckVersion() {
        $CI =& get_instance();
        if (strpos(uri_string(), "error") === false) {
            if (isset($CI->tank_auth) && $CI->tank_auth->is_logged_in()) {
                // CHECK IF VERSION file changed.. then go to installer too

                $installed_version = $CI->m_settings->get_by_group('ver');

                // Get version so we can run update?
                $fh = fopen("version", "r");
                if ($fh) {
                    $code_version = fgets($fh);
                    fclose($fh);
                }

                if ($installed_version['version'] != $code_version) {


                    if ( ! file_exists(getcwd()."/installer")) {
                        redirect('/error/installer_missing');
                    }

                    //only super admin should be able to upgrade....
                    $role = $CI->tank_auth->get_user_role();
                    if ($role == 'super') {
                        $CI->tank_auth->logout();
                        header('Location: '. BASE_URL.'installer');
                    } else {
                        $CI->tank_auth->logout();
                        redirect('/error/upgrade_application');
                    }

                    exit;
                }

            }
        }
    }
}


/* End of file hooks.classes.php */
/* Location: ./application/hooks/hooks.classes.php */
