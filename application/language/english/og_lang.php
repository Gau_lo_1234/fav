<?php
/*
    <?php echo  $this->lang->line('about_page_title'); ?>
 */

$lang['welcome'] = "Welcome, %s";

$lang['installer_remove_dir'] = 'For security reason you should delete the "installer" folder in the root of the website.';
$lang['copyright'] = 'Online Guarding &copy; %s';

$lang['nav_dashboard'] = 'Dashboard';
$lang['nav_reports'] = 'Reports';
$lang['nav_clients'] = 'Clients';
$lang['nav_client_profile'] = 'Client Profile';
$lang['nav_sites'] = 'Sites';
$lang['nav_users'] = 'Users';
$lang['nav_settings'] = 'Settings';
$lang['nav_distributors'] = 'Distributors';
$lang['nav_support'] = 'Support';

$lang['active'] = 'Active';
$lang['inactive'] = 'Inactive';
$lang['save'] = "Save";
$lang['cancel'] = "Cancel";
$lang['edit'] = "Edit";
$lang['login_as'] = "Login As";
$lang['delete'] = "Delete";

$lang['site'] = 'Site';
$lang['sites'] = 'Sites';
$lang['site_name'] = 'Site Name';
$lang['site_code'] = 'Site Code/s';
$lang['magcell_code'] = 'Magcell Code/s';
$lang['site_last_recevied'] = 'Last Signal Received';
$lang['site_description'] = 'Description';
$lang['site_telephone'] = 'Telephone';
$lang['site_alt_telephone'] = 'Alternative Telephone';

$lang['unique_magcell_code'] = 'Mag-Cell code/s';
$lang['additional_information'] = 'Additional information';

$lang['distributor'] = 'Distributor';
$lang['distributor_name'] = 'Distributor Name';
$lang['distributor_telephone'] = 'Telephone';
$lang['distributor_fax'] = 'Fax number';
$lang['distributor_email'] = 'Email Address';
$lang['distributor_website'] = 'Website';
$lang['distributor_physical_address'] = 'Physical Address';
$lang['distributor_postal_address'] = 'Postal Address';

$lang['distributor_contact_person'] = 'Contact Person';
$lang['distributor_contact_person_email'] = 'Email Address';
$lang['distributor_contact_person_telephone'] = 'Telephone';

$lang['distributor_accounts_person'] = 'Accounts Person';
$lang['distributor_accounts_person_email'] = 'Email Address';
$lang['distributor_accounts_person_telephone'] = 'Telephone';

//$lang['distributor_help_name'] = 'Distributor Name';
$lang['distributor_help_telephone'] = 'Example: +27 11 123 1234 or 082 123 1234';
$lang['distributor_help_fax'] = 'Example: +27 11 123 1234 or 082 123 1234';
//$lang['distributor_help_email'] = 'Email Address';
//$lang['distributor_help_website'] = 'Website';
//$lang['distributor_help_physical_address'] = 'Physical Address';
//$lang['distributor_help_postal_address'] = 'Postal Address';
//$lang['distributor_help_contact_person'] = 'Contact Person';
//$lang['distributor_help_contact_person_email'] = 'Email Address';
$lang['distributor_help_contact_person_telephone'] = 'Example: +27 11 123 1234 or 082 123 1234';
//$lang['distributor_help_accounts_person'] = 'Accounts Person';
//$lang['distributor_help_accounts_person_email'] = 'Email Address';
$lang['distributor_help_accounts_person_telephone'] = 'Example: +27 11 123 1234 or 082 123 1234';

$lang['clients'] = 'Client/s';
$lang['client'] = 'Client';
$lang['client_name'] = 'Client Name';
$lang['client_description'] = 'Description';
$lang['contact_person'] = 'Contact Person';
$lang['email'] = 'Email';
$lang['cell'] = 'Cellphone Number';
$lang['tel'] = 'Contact Number';
$lang['alt_tel'] = 'Alternative Contact Number';

$lang['username'] = 'Username';
$lang['role'] = 'User Role';
$lang['password'] = 'Password';
$lang['password_new'] = 'New Password';
$lang['password_confirm'] = 'Confirm Password';

$lang['last_login'] = 'Last Login';
$lang['status'] = 'Status';

$lang['language'] = 'Language';
$lang['timezone'] = 'Timezone';

$lang['point_name'] = 'Point Name';
$lang['point_code'] = 'Point Code';
$lang['duplicates'] = 'Clockings';
$lang['point_type'] = 'Point Type';

$lang['point_group'] = 'Point Group';
$lang['point_group_name'] = 'Point Group Name';
$lang['edit_points'] = 'Edit Points';
$lang['edit_point'] = 'Edit Point';


$lang['clocking_point'] = 'Clocking Points';
$lang['clocking_point_hint'] = "<h4>What is this?</h4><p>When batons are uploaded, the points they have clocked will automatically be imported into the system.</p>".
        "<h4>Sort points</h4><p>Points can be sorted by code or name by clicking on the heading, or points can be drag 'n dropped into a specific order. This will be shown on the reports.</p>".
        "<h4><img src='".base_url()."assets/images/edit.png'> Rename points</h4><p>To identify the site points more easily, the sytem allows you to give each point a new name. Click on the pencil icon <img src='".base_url()."assets/images/edit.png'> next to the point to rename it.</p>".
        "<h4><img src='".base_url()."assets/images/delete.png'/> De/activate points</h4><p>Click on the red icon <img src='".base_url()."assets/images/delete.png'/> next to the point you want to deactivate. This point will no longer be visiblle on the system. To reactivate the point click on 'Show/Hide Inactive Points' and the click on the <img src='".base_url()."assets/images/retry.png'/> icon.</p>".
        "<h4>Group Points</h4><p>To make managing points easier, create a new point group and simple drag 'n drop the point into the new group. You can sort points to be displayed in a certan order.</p>".
        "<h4><img src='".base_url()."assets/images/next.png'/> Create a site from a point group</h4><p>To create a new site from a point group simply click on the <img src='".base_url()."assets/images/next.png'/> icon. The correct site code will be used automatically and all points imported to the new site.</p><br/>";
$lang['rename_point'] = 'Rename Points';
$lang['assign_site_point'] = 'Assign Point/s to Site';
$lang['points'] = 'Points';

$lang['date_clocked'] = 'Date Clocked';
$lang['date_uploaded'] = 'Date Uploaded';
$lang['date_received'] = 'Date Received';
$lang['date_created'] = 'Date Created';
$lang['date_sent'] = 'Date Sent';
$lang['dwell_time'] = 'Dwell Time';
$lang['time_between_points'] = 'Time Between Points';

$lang['symbol'] = 'Symbol';
$lang['symbol_name'] = 'Name';
$lang['ip'] = 'IP';
$lang['battery_level'] = 'Battery Level';
$lang['battery_temperature'] = 'Battery Temperature';
$lang['temperature'] = 'Unit Temperature';
$lang['input_voltage'] = 'Input Voltage';
$lang['simcard_used'] = 'Simcard';
$lang['signal_strength'] = 'Signal Strength';

$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';

$lang['hourly'] = 'Hourly';
$lang['daily'] = 'Daily';
$lang['weekly'] = 'Weekly';
$lang['monthly'] = 'Monthly';
$lang['after_upload'] = 'After Upload';
$lang['never'] = 'Never';
$lang['alert'] = 'Alert';
$lang['alert_label'] =  'Alert when event occurs.';
$lang['alert_remind'] = "Resend every hour until fixed.";

$lang['report'] = 'Report';
$lang['last_sent'] = 'Last Sent';
$lang['last_clocked'] = 'Last Clocked';

/* Dashboard */
$lang['dashboard_help_inactive_sites'] = "<h4>What is this?</h4><p>Any site that has not sent any kind of signal through to Online Guarding for at least 3-hours will be considered as inactive.</p>";

$lang['dashboard_site_overview'] = "Sites Overview";
$lang['dashboard_content_site_overview'] = "<p>Any sites that have not recieved data will be listed here.</p>";
$lang['dashboard_no_active_sites'] = "No sites have recevied data for the last 3 hours.";
$lang['dashboard_no_inactive_stes'] =  "<p>No inactive sites.</p>";

/* Reports */
$lang['generate_report'] = 'Generate';

$lang['report_logs_help'] = 'All activity which changes data is recorded in the sytem.';
$lang['report_logs_description'] = 'A view on user activity on the system.';

$lang['report_magcell_serials_help'] = 'Shows which IMEI numbers are currently using server side generated serial numbers.';
$lang['report_magcell_serials_description'] = 'List of system generated serial numbers.';


$lang['report_detail_title'] = 'Report: Clocking details';
$lang['report_detail_help'] = 'This report type is excellent for detailed clocking information, but not so great for getting an overall picture. This report is best suited for analysing patrol over a short period, like an hour or up to a day.';
$lang['report_detail_description'] = 'A detailed list of clockings in chronological order.';
$lang['report_detail_uploaded'] = 'Uploaded %s';

$lang['report_overview_title'] = 'Report: Clocking overview';
$lang['report_overview_help'] = 'This report is ideal for getting an immediate impression of the clocking information for your site.';
$lang['report_overview_description'] = 'A "birds-eye-view" of the clocking data for your sites.';

$lang['report_overview_date_range'] = "Maximum date range of 14 days.";

$lang['report_overview_power_up'] = 'Power-up';
$lang['report_overview_upload'] = 'Upload';
$lang['report_overview_baton_comm'] = 'Baton communication';
$lang['report_overview_touch_port'] = 'Touch port activity';
$lang['report_overview_keypad'] = 'Keypad event';
$lang['report_overview_panic'] = 'Call button';
$lang['report_overview_tamper'] = 'Tamper';
$lang['report_overview_mains'] = 'Mains fail';
$lang['report_overview_mains_restore'] = 'Mains restore';
$lang['report_overview_patrol'] = 'Patrols';
$lang['report_overview_patrol_guard'] = 'Patrols Guard';
$lang['report_overview_patrol_fail'] = 'Patrol Fail';
$lang['report_overview_patrol_missed'] = 'Patrol Missed';
$lang['report_overview_patrol_irregular'] = 'Patrol Irregular';
$lang['report_overview_patrol_too_fast'] = 'Patrol Too Fast';
$lang['report_overview_patrol_okay'] = 'Patrol Okay';
$lang['report_overview_patrol_fail_guard'] = 'Patrol Incomplete';
$lang['report_overview_patrol_missed_guard'] = 'Patrol Incomplete';
$lang['report_overview_patrol_okay_guard'] = 'Patrol Complete';


$lang['report_signals_title'] = 'Report: Signals';
$lang['report_signals_help'] = 'Shows a list of all signals sent to the server. This list can be filtered to see specific signal types for a timeframe.';
$lang['report_signals_description'] = 'A list of signals sent from Magcell units.';

$lang['report_export_csv'] = 'Export to CSV';
$lang['report_export_pdf'] = 'Download Report';
$lang['report_email_now'] = "Email Report Now";
$lang['report_no_data'] = "<p>No data to generate report with.</p>";

$lang['report_automated_help_site_inactive'] = "Note: A site is seen as inactivte by the system if no signals have been received within 3 hours. If it remains inactive this mailer will be sent every hour.";


/* buttons */
$lang['add_a_site'] = 'Add a Site';
$lang['edit_site'] = 'Edit Site';
$lang['deactivate_site'] = 'Deactivate Site';
$lang['activate_site'] = 'Activate Site';
$lang['delete_site'] = 'Delete Site';
$lang['add_a_site'] = 'Add a Site';

$lang['add_a_distributor'] = "Add Distributor";
$lang['edit_distributor'] = "Edit Distributor";
$lang['delete_distributor'] = "Delete Distributor";

$lang['add_a_client'] = "Add a Client";
$lang['edit_client'] = "Edit Profile";
$lang['delete_client'] = "Delete Profile";
$lang['disable_client'] = "Disable Profile";
$lang['enable_client'] = "Enable Profile";
$lang['add_a_user'] = "Add a User";

$lang['edit_site_name'] = "Edit Site %s";
$lang['edit_user_name'] = "Edit User %s";
$lang['login_as_name'] = 'Login as %s';

/*users */
$lang['users_user_hint'] = 'These users are assigned to a client and can only see the sites for that client.';
$lang['users_admin_hint'] = 'These users only have access to Clients and sites that they have added or that they have been assigned to.';
$lang['users_super_hint'] = 'These users have full access to the system, that is access to system settings, all clients and all sites.';

$lang['no_users'] = 'No users added.';
$lang['users_title'] = 'Users';
$lang['users_edit_title'] = 'Edit User: %s';
$lang['users_add_title'] = 'Add New User';
$lang['users_disabled'] = 'Disabled';
$lang['users_disabled_title'] = 'User account has been disabled.';
$lang['users_changed_email'] = 'Changed Email';
$lang['users_changed_email_title'] = 'New email address has been requested but not verified.';
$lang['users_unverified_email'] = 'Email address has not yet been verified.';
$lang['users_unactive'] = 'Unactivated';
$lang['users_unactive_title'] = 'User account has not been activated.';
$lang['users_back'] = 'Back to Users';
$lang['users_not_yet_activated'] = 'This user has not yet been activated. ';
$lang['users_not_yet_verified'] = 'This user has not yet verified their email address. ';
$lang['users_resend_activation'] = 'Resend Activation Email';
$lang['users_new_email_requested'] = 'A new email address has been requested (%s). ';
$lang['users_resend_verification'] = 'Resend Verification email';
$lang['users_delete_text'] = 'This will delete the user completely from the system.';

$lang['users_send_email'] = 'Send email to user';
$lang['users_disable_reason'] = 'Reason for Disabling User';
$lang['users_optional_message'] = 'Optional Message to attach to email';
$lang['enable_user'] = 'Enable User';
$lang['disable_user'] = 'Disable User';

$lang['users_details'] = 'User Details';
$lang['users_preferences'] = 'Preferences';
$lang['users_email'] = 'Change Email';
$lang['users_password'] = 'Change Password';
$lang['users_suspension'] = 'Suspension';
$lang['users_delete'] = 'Delete User';

$lang['users_all_clients'] = 'Access to all Clients.';

$lang['users_delete_success'] = 'User has been deleted.';
$lang['users_delete_error'] = 'There was a problem deleting the user.';
$lang['users_added_success'] = 'New user %s has been succesfully created.';
$lang['users_added_email_success'] = 'New user %s has been succesfully created. Activation email has been sent.';
$lang['users_added_email_error'] = 'New user %s has been succesfully created. But there was a problem sending the activation email, please check your mail settings and resend.';

$lang['users_change_email_success'] = 'Email address has been changed to: %s. An activation email has been sent to confirm that it is valid.';
$lang['users_change_email_error'] = 'There was a problem sending the email. Please check your mail settings and try again.';
$lang['users_change_password_success'] = 'Password has been changed';
$lang['users_edit_success'] = 'User has been edited.';
$lang['users_pref_success'] = 'Preferences have been updated.';

$lang['users_ban_success'] = 'User has been banned.';
$lang['users_ban_email_success'] = 'User has been banned and email has been sent to notify user.';
$lang['users_ban_email_error'] = 'User has been banned but there was a problem sending the email. Please check your mail settings and try again.';

$lang['users_unban_success'] ='User has been unbanned.';
$lang['users_unban_email_success'] = 'User has been unbanned and email has been sent to notify user.';
$lang['users_unban_email_error'] = 'User has been unbanned but there was a problem sending the email. Please check your mail settings and try again.';

$lang['users_add_needs_client'] = 'In order to add a new user you must first add a client.';

$lang['profile_title'] = 'Profile';
$lang['profile_pref_success'] = 'Preferences have been updated.';
$lang['profile_email_success'] = 'Email address has been changed to: %s';
$lang['profile_password_success'] = 'Password has been changed';

/* Distributors */
$lang['distributor_title'] = "Distributor";
$lang['distributor_add_title'] = 'Add Distributor';
$lang['distributor_add_success'] = 'New distributor %s has been added.';
$lang['distributor_add_error'] = 'Error adding distributor.';
$lang['distributor_edit_title'] = 'Edit distributor: %s';
$lang['distributor_edit_success'] =  'Distributor %s has been edited.';
$lang['distributor_edit_error'] = 'Error editing distributor.';
$lang['distributor_remove_success'] = 'Distributor has been removed.';

/* Clients */
$lang['client_title'] = "Client";
$lang['client_add_title'] = 'Add Client';
$lang['client_add_success'] = 'New client %s has been added.';
$lang['client_add_error'] = 'Error adding client.';
$lang['client_edit_title'] = 'Edit Client: %s';
$lang['client_edit_success'] =  'Client %s has been edited.';
$lang['client_edit_error'] = 'Error editing client.';
$lang['client_assign_title'] = 'Assign User to Client';
$lang['client_assign_success'] = 'Assignment complete.';
$lang['client_remove_success'] = 'Client has been removed.';

$lang['client_edit_magcell'] = 'Edit Device';

$lang['client_disable_title'] = 'Disable Client: %s';
$lang['client_disable_content'] = '<p>This will prevent users from logging into the system and viewing their data.</p> <br/>';
$lang['client_disable_user_title'] = ' <h3>Users that will be disabled:</h3>';
$lang['client_disable_reason'] = 'Reason for Disabling Client';
$lang['client_disable_success'] = 'Client has been disabled.';

$lang['client_enable_title'] = 'Enable Client: %s';
$lang['client_enable_content'] = '<p>This will allow users to log into the system and view client data.</p><br/>';
$lang['client_enable_user_title'] = '<h3>Users that will be enabled: </h3>';
$lang['client_enable_success'] = 'Client has been enabled.';
$lang['client_magcell_edit'] = 'Device updated succesfully.';
$lang['magcell_name'] = 'Device Name:';

$lang['client_help_name'] = 'The name of the new Client';
$lang['client_help_desc'] = 'You can include anything from special instructions to the clients\'s physical address in here.';
$lang['client_help_contact'] = 'Name of the contact person at the client.';
$lang['client_help_tel'] = 'This is the number you would use if you needed to contact the client directly.';
$lang['client_help_alt_tel'] = 'The number you would use to contact the client if the primary number is engaged.';
$lang['client_help_email'] = 'Email address that can be used to contact client.';

$lang['clients_sites_hint'] = "<h4>What is a site?</h4><p>Each site has a unique magcell code. All information sent to the server from that specific magcell will be viewable under the site that has been created for it.</p>
    <h4>Last received</h4><p>This date is the last time a magcell signal has been received by our server. If you're having trouble getting your magcell sending signals to the server, please contact your distributor.</p>
    <h4>View more information</h4><p>To view more information for a site click on the site name.</p>
    <h4><img src='".base_url()."assets/images/edit.png'/> Edit a site</h4><p>Click on the pencil icon <img src='".base_url()."assets/images/edit.png'/> next to the site you want to edit. Alternatively go to the Sites tab above for complete management of your sites.</p>";
$lang['clients_users_hint'] = "<h4>What is this?</h4><p>This is a overview of users who have access to this specific client. The user status shows if a user has been activated and their email verified, if not their access to the system will be limited.</p>
    <h4><img src='".base_url()."assets/images/edit.png'/> Edit a user</h4><p>You can change a user's email, password and other data by clicking on the pencil icon <img src='".base_url()."assets/images/edit.png'/> next to the user you want to edit. Alternatively go to the Users tab above for complete list of users.</p>
    <h4><img src='".base_url()."assets/images/key.png'/> Login as a User</h4><p>The system allows certain user types the ability to login as a different user on the system, cimply click on the key icon next to the user <img src='".base_url()."assets/images/key.png'/>. This allows you to view the application as that user would view it.</p>";


$lang['users'] = 'Users';
$lang['user'] = 'User';
$lang['frequency'] = 'Frequency';
$lang['admin_users'] = 'Admin Users';

$lang['client_no_sites'] = 'This client has no sites.';
$lang['client_no_users'] = 'This client has no users.';
$lang['client_no_admins'] = 'This client has no admin.';

/*sites */
$lang['site_title'] = "Sites";
$lang['site_add_title'] = "Add Site";
$lang['site_add_success'] = 'New site %s has been added.';
$lang['site_add_error'] = 'Error adding site.';
$lang['site_edit_title'] = "Edit Site: %s";
$lang['site_edit_success'] = 'Site %s has been edited.';
$lang['site_edit_error'] = 'Error editing site.';
$lang['site_remove_success'] = "Site has been removed.";

$lang['site_deactivate_success'] = "Site has been deactivated.";
$lang['site_activate_success'] = "Site has been activated.";

$lang['site_help_rename_point'] = 'Choose a descriptive name so that you can easily identify the site.';

$lang['automated_emails'] = 'Automated Emails';
$lang['automated_emails_recipients'] = 'Automated Email Recipients';
$lang['automated_emails_manage'] = 'Manage Automated Emails';
$lang['create_mailer'] = 'Add Automated Email';
$lang['create_report_mailer'] = 'Add Automated Report Email';
$lang['create_alert_mailer'] = 'Manage Automated Alert Email';



$lang['automated_emails_no_users'] = 'No users found on system yet. You need to create a new user to setup automatic emails.';
$lang['automated_emails_select_user'] = 'Select user';
$lang['automated_emails_remove_success'] = 'Recepient has been removed';
$lang['automated_emails_edit_success'] = 'Email Recipient %s has been updated.';
$lang['automated_emails_create_error'] = 'An automated email with these settings already exists.';

$lang['automated_emails_hint'] =  "<h4>What is this?</h4><p>These are emails that are sent to you at specified times or when an alert is triggered. There are two types of automated emails. </p>
    <p><strong>Report automated emails</strong> are reports that are emailed to you at the times specified. eg. A monthly report will be sent monthly and contain a month worth of data. </p>
    <p><strong>Alert automated emails</strong> are emails that are sent when a specified alert is triggered. eg. A mains failed alert email will be sent when the system receives a mains failed signal from the site.</p>
    <h4><img src='".base_url()."assets/images/add.png'/> Add Automated email</h4><p>Click on the 'Manage Automated Emails' button.</p>
    <h4><img src='".base_url()."assets/images/edit.png'/> Edit Automated email</h4><p>Click on the pencil icon <img src='".base_url()."assets/images/edit.png'/> next to the automated email you want to edit.</p>
    <h4><img src='".base_url()."assets/images/delete.png'/> Remove Automated email</h4><p>Click on the red icon <img src='".base_url()."assets/images/delete.png'/> next to the automated email  you want to remove.</p>";

$lang['site_email_select_user'] = 'Select user';
$lang['site_email_view'] = 'View to Edit';
$lang['site_email_help'] = "<p>Hint: If you don't see the email address you're looking for in the
            drop down, you will need to %s
             to the
            system first.
        </p>";

$lang['site_no_clients'] = 'In order to add a new site you must first add a client.';
$lang['site_no_points'] = 'No points have been imported yet.';
$lang['site_no_emails'] = 'No automated emails have been set up yet.';

$lang['site_point_renamed'] = 'Point has been updated.';
$lang['site_point_removed'] = 'Site Point has been removed.';
$lang['site_point_assigned'] = 'Site Point has been assigned.';

$lang['assign_user'] = 'Assign As User Point';


$lang['settings_ver_title'] = 'Version Info';
$lang['settings_app_title'] = 'Application Settings';
$lang['settings_mail_title'] = 'Mail Settings';
$lang['settings_sys_title'] = 'System Settings';
$lang['settings_modules_title'] = "Modules";
$lang['settings_acl_title'] = "Access control list";
$lang['settings_munch_title'] = "Magcell Settings";

$lang['settings_save_success'] = 'Settings have been saved.';

$lang['settings_app_register_code'] = 'Registration Code';
$lang['settings_app_register_code_note'] = 'This code is required for someone to self-register on the application. Min length is 6 characters.';
$lang['settings_app_timezone'] = 'Default Timezone';
$lang['settings_app_timezone_note'] = '';
$lang['settings_app_storage_period'] = 'Store magcell data for';
$lang['settings_app_storage_period_note'] = 'Any signals in the database older than specified time will be deleted permanently.';
$lang['settings_app_bkp_path'] = 'Backup path';
$lang['settings_app_bkp_path_note'] = 'Path relative to index.php';
$lang['settings_app_ads_left'] = 'Left';
$lang['settings_app_ads_right'] = 'Right';

$lang['settings_app_error_email'] = 'Error Email Address';
$lang['settings_app_error_email_note'] = 'Email address where errors will be emailed. Additional emails should be seperated by commas.';

$lang['settings_mail_webmaster_addr'] = 'Webmaster Emaill Address';
$lang['settings_mail_webmaster_addr_note'] = 'Email address that will be used for the "From" field in emails.';
$lang['settings_mail_protocol'] = 'Protocol';
$lang['settings_mail_path'] = 'Mail Path';
$lang['settings_mail_path_note'] = 'Path to the mail function to be used.';
$lang['settings_mail_smtp_host'] = 'SMTP Host';
$lang['settings_mail_smtp_user'] = 'SMTP User';
$lang['settings_mail_smtp_password'] = 'SMTP Password';
$lang['settings_mail_smtp_port'] = 'SMTP Port';
$lang['settings_mail_smtp_port_note'] = 'Default smtp port is 25. Over SSL/TSL 465 is used.';
$lang['settings_mail_newline'] = 'Newline Characters.';
$lang['settings_mail_newline_note'] = 'This can either be \n or \r\n.';

$lang['settings_mail_smtp_timeout'] = 'SMTP Timeout';
$lang['settings_mail_smtp_timeout_note'] = 'Value in seconds. Default of 300 seconds ( 5 minutes ) should be enough.';

$lang['settings_sys_timezone'] = 'Timezone setting of magcell uploads';

$lang['settings_acl_permission'] = 'Permission';
$lang['settings_acl_permission_group'] = 'Group';
$lang['settings_acl_add'] = 'Add Permission';

$lang['settings_munch_battery_threshold'] ='Battery voltage threshold';
$lang['settings_munch_battery_threshold_help'] ='Above this voltage would be considered "good" below would be "bad". 3.8V - 4.2V';
$lang['settings_munch_battery_good'] ='Queue Length when battery GOOD';
$lang['settings_munch_battery_good_help'] ='Range: 1 to 25 events';
$lang['settings_munch_battery_bad'] ='Queue Length when battery BAD';
$lang['settings_munch_battery_bad_help'] ='Range: 1 to 25 events';
$lang['settings_munch_gps_coordinates_interval'] ='Seconds between GPS co-ordinates';
$lang['settings_munch_gps_coordinates_interval_help'] ='Range: 1 to 65535 seconds';


$lang['settings_modules_available'] = "Available modules:";
$lang['settings_modules_permissions'] = "Permissions Settings";
$lang['settings_modules_view_permissions'] = "Manage Permissions";

$lang['settings_modules'] = "Modules";


$lang['module_not_enabled'] = "Module has not been activated.";

$lang['version'] = 'Version';
$lang['release'] = 'Release Date';
$lang['install'] = 'Install Date';

$lang['check_new_version'] = 'Check for New Version';
$lang['version_available'] = 'Version %s now available for download!';
$lang['version_download'] = 'Get it now!';

$lang['dialog_delete_title'] = "Permenantly Delete Item?";
$lang['dialog_delete_title_user'] = "Permenantly Delete User?";
$lang['dialog_delete_body'] = 'Are you sure you want to continue?';

$lang['error_load_client'] = 'Error loading client.';

$lang['error_load_site'] = 'Error loading site.';
$lang['error_site_code_used'] = 'This site code is already in use.';
$lang['error_site_name_used'] = 'This site name is already in use.';

$lang['error_send_email'] = "An error occured trying to send email. Please check your mail settings and try again.";

$lang['error_version_check'] = "An error occured trying to check for a new version.<br/> %s";

$lang['version_check_yes'] = 'New version available for download!<br/>Version %s released %s.';
$lang['version_check_no'] = 'No new versions found.';
$lang['version_check_error_no_response'] = "Something went wrong trying to talk to home base.";

/* MAILER */
$lang['mailer_footer'] = "Regards\nThe %s Team";
$lang['unsubscribe_email_html'] = "<p>Don't want to reviece these emails anymore? <a href=\"%s\">Unsubscribe now</a></p>";
$lang['unsubscribe_email_txt'] = "Don't want to reviece these emails anymore? Unsubscribe: %s\n";

$lang['report_mailer_content'] = "Attached is an automatically generated email for the period of: %s - %s.";
$lang['report_mailer_content_html'] = "Attached is an automatically generated email for the period of:
<br />
<big style=\"font: 16px/18px Arial, Helvetica, sans-serif;\"><b>%s - %s.</b></big><br />";

$lang['report_mailer_content_after_upload'] = "Attached is an automatically generated report after a baton upload on: %s";
$lang['report_mailer_content_after_upload_html'] = "Attached is an automatically generated report after a baton upload on:
<br />
<big style=\"font: 16px/18px Arial, Helvetica, sans-serif;\"><b>%s</b></big><br />";

$lang['hourly_mailer_subject'] = "Hourly %s Report for Site: %s";
$lang['daily_mailer_subject'] = "Daily %s Report for Site: %s";
$lang['monthly_mailer_subject'] = "Monthly %s Report for Site: %s";
$lang['weekly_mailer_subject'] = "Weekly %s Report for Site: %s";

$lang['after_upload_mailer_subject'] = "After Upload %s Report for Site: %s";
$lang['alerts_mailer_subject'] = "Alerts Triggered for: %s";

$lang['inactive_mailer_subject'] = "Site inactive: %s";

$lang['inactive_mailer_content'] = "The site, %s, has been inactive since %s.";
$lang['inactive_mailer_content_html'] = "The site, %s, has been inactive since %s.";

$lang['inactive_mailer_content_never'] = "The site, %s, has never been active.";
$lang['inactive_mailer_content_never_html'] = "The site, %s, has never been active.";

$lang['signal_c_subject'] = "Tamper detected at Site: %s";
$lang['signal_c_mailer_content'] = "A tamper was detected at site, %s, at %s.";
$lang['signal_c_mailer_content_html'] = "A tamper was detected at site, %s, at %s.";

$lang['signal_b_up_subject'] = "Mains restored at Site: %s";
$lang['signal_b_up_mailer_content'] = "The mains have be restored at site, %s, at %s.";
$lang['signal_b_up_mailer_content_html'] = "The mains have be restored at site, %s, at %s.";

$lang['signal_b_subject'] = "Mains fail at Site: %s";
$lang['signal_b_mailer_content'] = "There has been a mains fail at site, %s, at %s.";
$lang['signal_b_mailer_content_html'] = "There has been a mains fail at site, %s, at %s.";

$lang['signal_a_subject'] = "Call button activated at Site: %s";
$lang['signal_a_mailer_content'] = "The call button at site, %s, was activated at %s.";
$lang['signal_a_mailer_content_html'] = "The call button at site, %s, was activated at %s.";

$lang['alert_point_subject'] = "Alert point clocked : %s";
$lang['alert_point_mailer_content'] = "An alert point has been clocked, %s, was activated at %s.";
$lang['alert_point_mailer_content_html'] = "An alert point has been clocked, %s, was activated at %s.";

$lang['monday'] = 'Monday';
$lang['tuesday'] = 'Tuesday';
$lang['wednesday'] = 'Wednesday';
$lang['thursday'] = 'Thursday';
$lang['friday'] = 'Friday';
$lang['saturday'] = 'Saturday';
$lang['sunday'] = 'Sunday';

/*18October*/
$lang['report_alerts_title'] = 'Report: Alerts Triggered';
$lang['report_alerts_help'] = 'This report contains a list of alerts that have been triggered in the system.  This includes Mains fail, tamper and call button activations.';
$lang['report_alerts_description'] = 'A list of alerts that have been triggered.';

$lang['alert_name'] = 'Alert Type';
$lang['alert_date'] = 'Date Triggered';

/* user roles */
$lang['user_role_super_desc'] = '<p>The <b><em>%s</em></b> users have full access to the system. All distributor, client and site data
	as well as system settings.
            </p>';
$lang['user_role_distrib_desc'] = '<p>The <b><em>%s</em></b> users have access to all client and site data under the distributor
	they have been assigned to. This user also has administrator rights to edit distributor information.
            </p>';
$lang['user_role_admin_desc'] = '<p>The <b><em>%s</em></b> users have access to multiple clients from a distributor.
</p>';
$lang['user_role_user_desc'] = '<p>A <b><em>%s</em></b> is assigned to a single client and only has access to those sites.
</p>';


/* End of file og_lang.php */
/* Location: ./application/language/english/og_lang.php */
