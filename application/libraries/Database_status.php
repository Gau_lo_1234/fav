<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* Database_status
*
* Library to test DB credentials and see if we can connect to database.
*
*
* @uses
*
* @category Category
* @package  Package
* @author    <>
* @license
* @link
*/
class Database_status {
    private $online;
    private $CI;

    function __construct() {
        $this->ci =& get_instance();
        // $this->ci->load->config('database');

        $hostname = $this->ci->db->hostname;
        $username = $this->ci->db->username;
        $password = $this->ci->db->password;
        $port     = $this->ci->db->port;

        /* DEPRECATED IN 5.5
        return mysql_connect("$hostname:$port", $username, $password);
        */
        $dsn = 'mysql:host='.$hostname.';port='.$port;
        try {
            $dbh = new PDO($dsn, $username, $password);
            $this->online = true;
        } catch (PDOException $e) {
            // echo 'Connection failed: ' . $e->getMessage();
             $this->online =  false;
        }
    }

    function isOnline() {
        return $this->online;
    }

}

/* End of file Database_status.php */
/* Location: ./application/libraries/Database_status.php */