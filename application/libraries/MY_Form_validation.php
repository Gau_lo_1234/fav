<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* MY_Form_validation
*
* @uses     MX_Controller
*
* @category Core
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
class MY_Form_validation extends CI_Form_validation{
    public $CI;

     function __construct($config = array()){
          parent::__construct($config);
          $this->CI =& get_instance();
     }

     function check_recaptcha($value){

        if ( $this->CI->config->item('use_recaptcha', 'tank_auth') ) {
            $this->CI->load->helper('recaptcha2');

            $resp = recaptcha_verify_response($this->CI->config->item('recaptcha_private_key', 'tank_auth'),
                $_SERVER['REMOTE_ADDR'],
                $value);

            if (!$resp->is_valid) {
                $this->set_message('check_recaptcha', $this->CI->lang->line('auth_incorrect_recaptcha'));
                return FALSE;
            }
            return TRUE;

        }
    }

    /**
     * check_magcell_code
     *
     * A magcell code is a 6 digit HEX string.
     *
     * @param string $value
     *
     * @access public
     *
     * @return boolean
     */
    function check_magcell_code($value){
        if (preg_match('/^[0-9A-F]{6}$/i', $value) == 0) {
            $this->set_message('check_magcell_code', 'The code you entered is invalid. The code is the 6 digit number provided with your device.');
            return FALSE;
        }
        return TRUE;
    }
}