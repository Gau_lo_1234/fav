<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pixie_css {
    function __construct($params = array()) {
        $this->path = (isset($params['path']) ? $params['path'] : 'assets');
        // he follwoing to is for the onload function start and end.
        // so we can use more than just jquery :)
        $this->scripts = (isset($params['scripts']) && is_array($params['scripts']) ? $params['scripts'] :array() );
    }

    function set_path($path) {
        $this->path = $path;
    }

    function add_script($script_name, $path=null) {
        if (is_null($path)) $path = $this->path;
        $this->scripts[] = array($path, $script_name);
    }

    function html() {
        $html = '';
        foreach ( $this->scripts as $s) {
             $html .= "  ".link_tag( base_url().$s[0].'/'.$s[1] )."\n";
        }//foreach
        return $html;
    }
}

/* End of file Pixie_css.php */
/* Location: ./application/libraries/Pixie_css.php */