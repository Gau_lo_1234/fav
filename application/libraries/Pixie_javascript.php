<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pixie_javascript {

    function __construct($params = array()) {
        $this->path = (isset($params['path']) ? $params['path'] : '/js');
        // he follwoing to is for the onload function start and end.
        // so we can use more than just jquery :)
        $this->onload_wrapper_start = '<script type="text/javascript" >
	$(function() {';
        $this->onload_wrapper_end = '	});
        </script>';

        $this->onload = (isset($params['onload']) ? $params['onload'] :'');
        $this->scripts = (isset($params['scripts']) && is_array($params['scripts']) ? $params['scripts'] :array() );
    }

    function set_path($path) {
        $this->path = $path;
    }

    function set_onload_wrapper($script_start, $script_end) {
        $this->onload_wrapper_start = $script_start;
        $this->onload_wrapper_end = $script_end;
    }

    function append_onload($js) {
        $this->onload .= $js;
    }

    function onload_html() {
        return $this->onload_wrapper_start."\n".$this->onload."\n".$this->onload_wrapper_end;
    }

    function add_script($script_name, $path=null, $external = false) {
        if (is_null($path)) $path = $this->path;

        if (!$external) {
            $path = base_url().$path;
        }
        $this->scripts[] = array($path, $script_name);
    }

    function script_html() {
        $html = '';
        foreach ( $this->scripts as $s) {
             $html .= '  <script type="text/javascript" src="'.$s[0].'/'.$s[1].'"></script>'."\n";
        }//foreach
        return $html;
    }

}

/* End of file Pixie_javascript.php */
/* Location: ./application/libraries/Pixie_javascript.php */