<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * m_Api
*
* @uses     CI_Model
*
* @category Client
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
class m_Api extends CI_Model {
    private $table_name = 'clients';
    private $table_api_keys = 'x_api_keys';
    private $table_api_logs = 'x_api_logs';

    function __construct() {
        parent::__construct();
    }

    function get_key_list($start = 0, $limit = null, $order_by = null, $order_direction = 'asc') {
        $data = array();
        if ($order_by !== null && $order_by) {
            $this->db->order_by($order_by, $order_direction);
        }
        $query = $this->db->get($this->table_api_keys, $limit, $start);
        if ($query && $query->num_rows() > 0) {
            return $query->result_array();
        }
        return $data;
    }

    function get_log_list($start = 0, $limit = null, $order_by = null, $order_direction = 'asc') {
        $data = array();
        if ($order_by !== null && $order_by) {
            $this->db->order_by($order_by, $order_direction);
        }
        $query = $this->db->get($this->table_api_logs, $limit, $start);
        if ($query && $query->num_rows() > 0) {
            return $query->result_array();
        }
        return $data;
    }



    function generate_key() {
        $this->load->helper('security');
        do {
            $salt = do_hash(time().mt_rand());
            $new_key = substr($salt, 0, config_item('rest_key_length'));
        }
        while ($this->key_exists($new_key));
        return $new_key;
    }

    /**
     * get_key
     *
     * @param string $key API key
     *
     * @access public
     *
     * @return boolean
     */
    function get_key($key) {
        return $this->db->where(config_item('rest_key_column'), $key)->get(config_item('rest_keys_table'))->row();
    }

    /**
     * key_exists
     *
     * @param string $key API key
     *
     * @access public
     *
     * @return boolean
     */
    function key_exists($key) {
        return $this->db->where(config_item('rest_key_column'), $key)->count_all_results(config_item('rest_keys_table')) > 0;
    }

    /**
     * insert_key
     *
     * @param string $key API key
     * @param mixed $data Description.
     *
     * @access public
     *
     * @return boolean
     */
    function insert_key($key, $data) {
        $data[config_item('rest_key_column')] = $key;
        $data['date_created'] = function_exists('now') ? now() : time();
        //$data['name'] = ''; uncomment to resolve null value error on database as field is set to not allow nulls
        return $this->db->set($data)->insert(config_item('rest_keys_table'));
    }

    /**
     * update_key
     *
     * @param string $key API key
     * @param mixed $data Description.
     *
     * @access public
     *
     * @return boolean
     */
    function update_key($key, $data) {
        return $this->db->where(config_item('rest_key_column'), $key)->update(config_item('rest_keys_table'), $data);
    }

    /**
     * delete_key
     *
     * @param string $key API key
     *
     * @access public
     *
     * @return boolean
     */
    function delete_key($key) {
        return $this->db->where(config_item('rest_key_column'), $key)->delete(config_item('rest_keys_table'));
    }


}

/* End of file m_api.php */
/* Location: ./application/models/m_api.php */