<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* m_data_captured
*
*
* @uses     CI_Model
*
* @category Site
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
class m_data_captured extends CI_Model {
    private $table_name = 'data_capture';
    private $group_table_name = 'groups';
    private $distributor_table_name = 'company';
    private $users_table_name = 'users';
    private $device_table_name = 'devices';

    private $link_table_name = 'group_numbers';

    function __construct() {
        parent::__construct();
    }
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('isDeleted', 0);
        //$this->db->where('roleId !=', 1);
        $this->db->where('id', $userId);
        $query = $this->db->get();

        return $query->result();
    }
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function update_distributor($delid)
    {
        $userInfo = array('isDeleted'=>1);
        $this->db->where('id', $delid);
        $this->db->update('company', $userInfo);
        return $this->db->affected_rows();
    }
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfoById($userId)
    {
        $this->db->select('userId, name, email, mobile, roleId');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('userId', $userId);
        $query = $this->db->get();

        return $query->row();
    }
    /**
     * This function is used to get user login history
     * @param number $userId : This is user id
     */
    function loginHistoryCount($userId, $searchText, $fromDate, $toDate)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.userId', $userId);
        $this->db->from('tbl_last_login as BaseTbl');
        $query = $this->db->get();

        return $query->num_rows();
    }

    /**
     * This function is used to get user login history
     * @param number $userId : This is user id
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function loginHistory($userId, $searchText, $fromDate, $toDate, $page, $segment)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        $this->db->from('tbl_last_login as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.userId', $userId);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }


    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getDistInfo($userId)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('isDeleted', 0);
        //$this->db->where('roleId !=', 1);
        $this->db->where('id', $userId);
        $query = $this->db->get();

        return $query->result();
    }


    function get_count() {
        $this->db->select('Count(*) as total');
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->row()->total;
        }
        return 0;
    }
    function get_distributor_count($searchText = '') {
        $this->db->select('Count(*) as total');
        if(!empty($searchText)) {
            $likeCriteria = "(company_email  LIKE '%".$searchText."%'
                            OR  company_name  LIKE '%".$searchText."%'
                            OR  company_phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get($this->distributor_table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->row()->total;
        }
        return 0;
    }
    function get_user_count($searchText = '') {
        $this->db->select('Count(*) as total');
        if(!empty($searchText)) {
            $likeCriteria = "(email  LIKE '%".$searchText."%'
                            OR  name  LIKE '%".$searchText."%'
                            OR  phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get($this->users_table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->row()->total;
        }
        return 0;
    }

    function get_devices_count($searchText = '') {
        $this->db->select('Count(*) as total');
        if(!empty($searchText)) {
            $likeCriteria = "(email  LIKE '%".$searchText."%'
                            OR  name  LIKE '%".$searchText."%'
                            OR  phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get($this->device_table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->row()->total;
        }
        return 0;
    }
    function get_data() {
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }
    function get_group_count() {
        $this->db->select('Count(*) as total');
        $query = $this->db->get($this->group_table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->row()->total;
        }
        return 0;
    }

     function get_group_data() {

        $sql = "select g.group_name,
                m.memberName,
                g.groupType,
                g.date
                from groups g
                left join members m on m.memberID = g.originatingMemberID";

        $query = $this->db->query($sql);
        if ($query && $query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }
    function get_distributor_data() {

        $sql = "select company_name, company_email, company_phone
                from company";

        $query = $this->db->query($sql);
        if ($query && $query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }


    /**
     * @return mixed
     * function to search and find a device
     */
function get_devices_find() {
$id=$_GET['id'];
       $this->db->select('*');
        $this->db->from('devices');
        $this->db->where('id =', $id);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * @return int
     * function to get device data
     */
    function get_devices_data() {

        $sql = "select imei,
                name, 
                phone,
                email,
                encrypt
                from devices";

        $query = $this->db->query($sql);
        if ($query && $query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    /**
     * @return int
     * function to count the numbers
     */
    function get_number_count() {
        $this->db->select('Count(*) as total');
        $query = $this->db->get($this->link_table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->row()->total;
        }
        return 0;
    }
 /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('users as BaseTbl');
       // $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('users', $userInfo);

        return $this->db->affected_rows();
    }

    /**
     * This function is used to get the distributor listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function distributorListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('company as BaseTbl');
        // $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.company_email  LIKE '%".$searchText."%'
                            OR  BaseTbl.company_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.company_phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();

        return $query->num_rows();
    }
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function distributorListing($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('company as BaseTbl');
        //$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.company_email  LIKE '%".$searchText."%'
                            OR  BaseTbl.company_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.company_phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }


    /**
     * The folowing function lists the available user roles
     */
     function getUserRoles()
    {
        $this->db->select('role');
        $this->db->from('users');
        $this->db->limit(2);
        //$this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function deviceListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('devices as BaseTbl');
        //$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        //echo $query->num_rows();
        return $query->num_rows();
    }


    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('users as BaseTbl');
        //$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function deviceListing($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('devices as BaseTbl');

        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.phone  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        //$this->db->where('BaseTbl.isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     *
     */
    function addNewUser(){
        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        $name = $_POST['fname'];
        $email = $_POST['email'];
        $password = $hasher->HashPassword($_POST['password']);
        $phone = $_POST['phone'];
        $role=$_POST['role'];
        $created=1;

        $this->db->query("INSERT INTO users(username, email,password, cellphone, role, created) VALUES('$name','$email','$password', '$phone','$role','$created')");
    }

function editDevice(){

}
    /**
     * store
     *
     * @param mixed  $imei       Description.
     * @param string $real_email Description.
     * @param string $user_email Description.
     * @param string $name       Description.
     * @param string $number     Description.
     * @param string $contacts   Description.
     * @param string $gps        Description.
     *
     * @access public
     *
     * @return mixed Value.
     */
    function store($imei, $real_email='', $user_email='', $name='', $number='', $contacts='', $gps='') {
        if ($contacts) {
            $contacts = json_encode($contacts);
        }
        $db_data = array(
            'imei'=>$imei,
            'real_email'=>$real_email,
            'user_email'=>$user_email,
            'name'=>$name,
            'number'=>$number,
            'contacts'=>$contacts,
            'gps'=>$gps,
            'date'=>date('Y-m-d H:i:s')
            );

         $this->db->insert($this->table_name, $db_data);
         return $this->db->insert_id();
    }

    function add_member($name = null, $number = null,$imei = null, $real_email = null, $user_email = null, $gps = null,$markAsInstalled = false){
        $sql = "insert into members(
                    memberName,memberNumber,created, imei, realEmail,userEmail,gps,hasInstalled
                )values(
                '$name','$number',current_time(),'$imei','$real_email','$user_email','$gps'";

        if($markAsInstalled){
            $sql .= ",1)";
        }else{
            $sql .= ",0)";
        };


        if($this->db->query($sql)){
            return $this->db->insert_id();
        }else{
            return false;
        }
        
    }

    function update_member_info($isMember,$imei, $real_email, $user_email, $name, $number, $gps,$isInstalled = null){
        $sql = "update members set 
                imei = '$imei',
                realEmail = '$real_email',
                userEmail = '$user_email',
                memberName = '$name',
                gps = '$gps',";
                if($isInstalled){
                    $sql .= "hasInstalled = 1 ";
                }
                $sql .= "where memberID = $isMember";

        if($this->db->query($sql)){
            return true;
        }else{
            return false;
        }
    }

    function get_member_info($memberID = null){
        $sql = "select * from members where memberID = $memberID";
        $result = $this->db->query($sql);
        if($result && $result->num_rows() > 0){
            return $result->row();
        }else{
            return false;
        }
    }

    function check_member_status($number = null){

            $sql = "select * from members where memberNumber = $number";
       

        $result = $this->db->query($sql);

        if($result && $result->num_rows() > 0){
            return $result->row()->memberID;
        }else{
            return false;
        }
    }

    function check_home_group($memberID = null){

        $sql = "select * from groups where OriginatingMemberID = $memberID";

        $result = $this->db->query($sql);

        if($result && $result->num_rows() > 0){
            return $result->row()->group_id;
        }else{
            return false;
        }
    }


    function check_group($number) {
        $group = false;
        $this->db->where('number', $number);
        $query = $this->db->get($this->link_table_name);


        if ($query && $query->num_rows() > 0) {
            return $query->row()->group_id;
        }
        return $group;
    }

    function get_group_numbers($group_id) {
        $this->db->select('number');
        $this->db->where('group_id', $group_id);
        $this->db->order_by('number', 'asc');
        $query = $this->db->get($this->link_table_name);
        if ($query && $query->num_rows() > 0) {
            $data = array();
            foreach($query->result() as $row) {
                if ($row->number == 'noone') { continue; }
                $data[] = $row->number;
            }
            return $data;
        }
        return false;
    }

    function get_group_numbers_installs($group_id) {

        $sqlSelect = "SELECT gn.* ,
                        if(dc.data_capture_id,\"Yes\",\"No\")as isInstalled
                        FROM fa.group_numbers gn
                        left join data_capture as dc on dc.number = gn.number
                        where gn.group_id = $group_id";

        $query = $this->db->query($sqlSelect);
        if ($query && $query->num_rows() > 0) {
            $data = array();
            foreach($query->result() as $row) {
                if ($row->number == 'noone') { continue; }
                $data[] = array("Name" =>$row->name,"Number"=>$row->number,"Installed"=>$row->isInstalled);
            }
            return $data;
        }
        return false;
    }


    function get_group_names_numbers($group_id) {

        $sql = "SELECT gl.groupID,
                g.group_name,
                m.memberName,
                m.memberNumber,
                if(m.hasInstalled is true,'Y','N') as installed
                 FROM fa.groupLinks gl
                join members as m on m.memberID = gl.memberID
                join groups as g on g.group_id = gl.groupID
                where gl.groupID = $group_id";
        $query = $this->db->query($sql);
        if ($query && $query->num_rows() > 0) {
            $data = array();
            foreach($query->result() as $row) {
                if ($row->memberNumber == 'noone') { continue; }
                $data[] = array('Name'=>$row->memberName,'Number'=>$row->memberNumber,'GroupID'=>$row->groupID,'GroupName'=>$row->group_name,"Installed"=>$row->installed);
            }
            return $data;
        }
        return false;
    }
    
    
    function add_group_number($group_id, $number,$name) {
        // if ($number == 'noone') { return; }
        // $this->db->where('group_id', $group_id);
        // $this->db->where('number', $number);
        // $query = $this->db->get($this->link_table_name);
        // if (!$query || $query->num_rows() == 0) {
        //     $db_data = array(
        //         'group_id'=>$group_id,
        //         'number'=>$number,
        //         'name' => $name,
        //         'date'=>date('Y-m-d H:i:s')
        //     );
        //     $this->db->insert($this->link_table_name, $db_data);
        //     return $this->db->insert_id();
        // }
    }


    function remove_group_links($groupID = null,$adminID = null,$keepAdmin = true){
        $sql = "delete from groupLinks where groupID = $groupID";
        if($keepAdmin == true){
            $sql .= " and memberID != $adminID";
        }

        if($this->db->query($sql)){
            return true;
        }else{
            return false;
        }

    }


    function add_group_name_number($groupID, $details){

        $number = preg_replace("/[^0-9]/", "", str_replace("+27", "0", $details[1]));

        if($memberID = $this->check_member_status($number)){
            //membership found so just add groupLink
            $this->add_group_link($groupID, $memberID);
            return true;
        }else{

            //membership not found so create membership then add to groupLink
            if($memberID = $this->add_member($details[0],$number)){
                $this->add_group_link($groupID, $memberID);
                return true;               
            };


        }

      
    }

    /*
        @$interval is the record id returned from the initial record post save function
        @$memberID of admin for group
        @$groupType Home or Community

    */
    function create_home_group($interval=null,$memberID = null,$groupType = 'COMMUNITY') {
        if ($interval == null) { $interval = time(); }

        //generates a generic name like GP1 etc
        $group_name = $this->generate_group_name($interval);

        //add human readable portion to name  GP1_TomHarding
        $friendlyName = $this->db->query("select memberName from members where memberID = $memberID")->row()->memberName;

        $db_data = array(
            'group_name'=>$group_name.'_'.$friendlyName,
            'date'=>date('Y-m-d H:i:s'),
            'OriginatingMemberID'=>$memberID,
            'groupType'=>strtoupper($groupType)
            );
        $this->db->insert($this->group_table_name, $db_data);
        return $this->db->insert_id();
    }


    function add_group_link($groupID,$memberID){
        $sql = "insert into groupLinks(
                groupID,memberID,created
                )values(
                $groupID,$memberID,current_time())";

        if($this->db->query($sql)){
            return true;
        }else{
            return false;
        }
    }


    function create_group($interval=null,$memberID = null) {
        if ($interval == null) { $interval = time(); }

        $group_name = $this->generate_group_name($interval);
        $db_data = array(
            'group_name'=>$group_name,
            'date'=>date('Y-m-d H:i:s'),
            'OriginatingMemberID'=>$memberID
            );
        $this->db->insert($this->group_table_name, $db_data);
        return $this->db->insert_id();
    }

    function generate_group_name($interval) {
        $group_name_offset = $this->m_settings->get_by_key('group_name_offset')->value;
        do {
            $str = 'GP'.($group_name_offset+$interval);
            $interval++;
        } while ($this->check_group_name_exists($str));

        return $str;
    }

    function check_group_name_exists($group_name) {
        $this->db->where('group_name', $group_name);
        $query = $this->db->get($this->group_table_name);
        return ($query && $query->num_rows() > 0);
    }

    function get_memberGroups($groupList = null){
        //$sql = "select * from groups where group_id in ($groupList)";
        $sql = "select g.group_id,
                count(*) as memberCount,
                g.group_name 
                from groupLinks
                left join groups as g on g.group_id = groupLinks.groupID
                where groupID in ($groupList)
                group by groupID";
        $query = $this->db->query($sql);

        if ($query && $query->num_rows() > 0) {
            $data = array();
            foreach($query->result() as $row) {

                $data[] = array('group_name'=>$row->group_name,'group_id'=>$row->group_id,'memberCount'=>$row->memberCount);
            }
            return $data;
        }else{
            return false;
        }
    }

    function get_groupMembers($getType = null,$value = null){

        switch($getType){
            case 'byID':
                $groupID = $value;

                //groupID exists and returns info
                if($groupInfo = $this->db->query("select * from groups where group_id = $value")->row()){
                    if($members = $this->get_group_names_numbers($groupID)){

                        $data['groupID'] = $groupID;
                        $data['members'] = $members;
                        return $data;
                    }else{
                        return false;
                    };

                };

            break;
            case 'byName':


                if($groupInfo = $this->db->query("select * from groups where group_name = '$value'")->row()){

                    if($members = $this->get_group_names_numbers($groupInfo->group_id)){

                        $data['groupID'] = $groupInfo->group_id;
                        $data['members'] = $members;
                        return $data;
                    }else{
                        return false;
                    };
                }

            break;
            case 'byNumber':
                if($groupInfo = $this->db->query("SELECT * FROM fa.members
                                                    left join groups as g on g.OriginatingMemberID = members.memberID 
                                                    where hasInstalled is true and memberNumber =  '$value'")->row()){


                    if($members = $this->get_group_names_numbers($groupInfo->group_id)){

                        $data['groupID'] = $groupInfo->group_id;
                        $data['members'] = $members;
                        return $data;
                    }else{
                        return false;
                    };
                }
            break;
        }
    }


    function get_group_by_assoc($getType = null,$value = null){

        $groupFetch = "select 
                       gl.groupID,
                       g.group_name,
                       g.OriginatingMemberID,
                       m.memberName,
                       g.groupType
                       from groupLinks gl
                       join groups as g on g.group_id = gl.groupID
                       join members as m on m.memberID = g.originatingMemberID ";

        $groupMemberFetch = "select gl.groupID, gl.memberID,
                            if(m.memberName != '',m.memberName,'Name Not Provided On Setup') memberName,
                            if(m.hasInstalled is true,'Y','N')hasInstalled
                            from groupLinks gl
                            left join members as m on m.memberID = gl.memberID
                            left join groups as g on g.group_id = gl.groupID
                            where gl.groupID in (
                            select gl.groupID from groupLinks gl
                            join groups as g on g.group_id = gl.groupID ";      


        switch($getType){
            case 'byID':
                $memberID = $value;

                $groupFetch .= "where gl.memberID = $memberID and g.originatingMemberId <> $memberID
                                group by gl.groupID";


                $groupMemberFetch .= "where gl.memberID = $memberID and g.originatingMemberId <> $memberID)";


 
                if($groupfetchresult = $this->db->query($groupFetch)){
                    $data['groups'] = $groupfetchresult->result();
                };

                if($groupmemberfetchresult = $this->db->query($groupMemberFetch)){
                    $data['groupMembers'] = $groupmemberfetchresult->result();
                };

                return $data;
            break;

            case 'byName':


                $memberName = $value;

                $groupFetch .= "where gl.memberID = (select memberID from members where membername = '$memberName') 
                                and g.originatingMemberId <> (select memberID from members where membername = '$memberName')
                                group by gl.groupID";


                $groupMemberFetch .= "where gl.memberID = (select memberID from members where membername = '$memberName') 
                                    and g.originatingMemberId <> (select memberID from members where membername = '$memberName'))";


 
                if($groupfetchresult = $this->db->query($groupFetch)){
                    $data['groups'] = $groupfetchresult->result();
                };

                if($groupmemberfetchresult = $this->db->query($groupMemberFetch)){
                    $data['groupMembers'] = $groupmemberfetchresult->result();
                };

                return $data;

            break;
            case 'byNumber':


                $memberNumber = $value;

                $groupFetch .= "where gl.memberID = (select memberID from members where memberNumber = '$memberNumber') 
                                and g.originatingMemberId <> (select memberID from members where memberNumber = '$memberNumber')
                                group by gl.groupID";


                $groupMemberFetch .= "where gl.memberID = (select memberID from members where memberNumber = '$memberNumber') 
                                    and g.originatingMemberId <> (select memberID from members where memberNumber = '$memberNumber'))";


 
                if($groupfetchresult = $this->db->query($groupFetch)){
                    $data['groups'] = $groupfetchresult->result();
                };

                if($groupmemberfetchresult = $this->db->query($groupMemberFetch)){
                    $data['groupMembers'] = $groupmemberfetchresult->result();
                };

                return $data;
            break;
        }
    }  




    function get_groups($getType = null,$value = null){

        switch($getType){
            case 'byID':
                $memberID = $value;

                //groupID exists and returns info
                if($groupInfo = $this->db->query("select distinct(groupID)as groups from groupLinks where memberID = $value")->result()){
                   
                    foreach($groupInfo as $item){
                        $array[] = $item->groups;
                    }


                    if($groups = $this->get_memberGroups(implode(',',$array))){

                        $data['groups'] = $groups;
                        return $data;
                    }else{
                        return false;
                    };

                };

            break;
            case 'byName':


                if($groupInfo = $this->db->query("select distinct(groupID)as groups from groupLinks 
                                                    left join members as m on m.memberID = groupLinks.memberID
                                                    where m.memberName = '$value'")->result()){

                    foreach($groupInfo as $item){
                        $array[] = $item->groups;
                    }


                    if($groups = $this->get_memberGroups(implode(',',$array))){

                        $data['groups'] = $groups;
                        return $data;
                    }else{
                        return false;
                    };
                }

            break;
            case 'byNumber':

                if($groupInfo = $this->db->query("select distinct(groupID)as groups from groupLinks 
                                                    left join members as m on m.memberID = groupLinks.memberID
                                                    where m.memberNumber = '$value'")->result()){

                    foreach($groupInfo as $item){
                        $array[] = $item->groups;
                    }


                    if($groups = $this->get_memberGroups(implode(',',$array))){

                        $data['groups'] = $groups;
                        return $data;
                    }else{
                        return false;
                    };
                }
            break;
        }
    }    

    function get_communityGroups($status = null){
        $sql = "SELECT g.group_id,
                g.group_name,
                gm.memberCount
                FROM fa.groups g
                left join (select count(*) as memberCount, groupID from groupLinks group by groupID) as gm on gm.groupID = g.group_id
                where g.groupType = 'COMMUNITY' and g.status = '$status'";

        $result = $this->db->query($sql);

        if($result && $result->num_rows() > 0){
            foreach($result->result() as $row){
               $data[] = array('group_name'=>$row->group_name,'group_id'=>$row->group_id,'memberCount'=>$row->memberCount);
            };
            return $data;

        }else{
            return false;
        }
    }

    function changeGroupName($data = null){
        $sql = "update groups set group_name = '{$data['new_group_name']}'
                where OriginatingMemberID = {$data['memberID']}
                and groups.group_id = {$data['groupID']}";
        $result = $this->db->query($sql);

        if($result){
            return true;
        }else{
            return false;
        }
    }

}
