<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* m_Logger
*
* @uses     CI_Model
*
* @category Data
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
class m_Logger extends CI_Model {
    private $table_name = 'log';

    function __construct() {
        parent::__construct();
    }

    /**
     * log
     *
     * Create a log entry.
     *
     * @param int $user_id
     * @param string $log_type
     * @param string $log_message
     * @param string $log_data    Seriealized PHP array
     * @param int $site_id     (optional)
     * @param int $client_id   (optional)
     *
     * @access public
     *
     * @return void
     */
    function log($user_id, $log_type, $log_message, $log_data, $site_id=null, $client_id=null) {
        $data = array(
            'user_id'=>$user_id,
            'site_id'=>$site_id,
            'client_id'=>$client_id,
            'log_type'=>$log_type,
            'log_message'=>$log_message,
            'log_data'=>  serialize($log_data),
            'date' => time(),
            );
        $this->db->insert($this->table_name, $data);
    }

    /**
     * get_by_id
     *
     * @param int $log_id
     *
     * @access public
     *
     * @return obj
     */
    function get_by_id($log_id) {
        $this->db->where('id', $log_id);
        $query  = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    /**
     * get_list
     *
     * @param int $start  (optional) unixtimestamp
     * @param int $end   (optional) unixtimestamp
     *
     * @access public
     *
     * @return array
     */
    function get_list($start=null, $end=null, $log_type=null, $client_id=null, $site_id=null, $user_id=null) {
        $this->db->select($this->table_name.'.*');
        if (!is_null($start)) {
            $this->db->where('date >', $start);
        }
        if (!is_null($end)) {
            $this->db->where('date <', $end);
        }

        if (!is_null($log_type)) {
            if (is_array($log_type)) {
                if (!empty($log_type)) {
                    $this->db->where_in('log_type', $log_type);
                }
            } else {
                $this->db->where('log_type', $log_type);
            }
        }
        if (!is_null($client_id)) {
            $this->db->where('client_id', $client_id);
        }
        if (!is_null($site_id)) {
            $this->db->where('site_id', $site_id);
        }
        if (!is_null($user_id)) {
            $this->db->where('user_id', $user_id);
        }

        $this->db->select('users.username');
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id', 'left');
        $this->db->select('sites.name as sitename');
        $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id', 'left');
        $this->db->select('clients.name as clientname');
        $this->db->join('clients', 'clients.id = '.$this->table_name.'.client_id', 'left');
        $query = $this->db->get($this->table_name);

        if ($query && $query->num_rows() > 0) {
            return ($query->result());
        } else {
            return array();
        }
    }

    /**
     * get_distinct_log_types
     *
     * @access public
     *
     * @return array
     */
    function get_distinct_log_types() {
        $this->db->distinct();
        $this->db->select('log_type');
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            $data = $query->result();
            sort($data);
            return $data;
        }
        return array();
    }
}

/* End of file m_logger.php */
/* Location: ./application/models/m_logger.php */