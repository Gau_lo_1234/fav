<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* m_Settings
*
*
* @uses     CI_Model
*
* @category Site
* @package  OnlineGuarding
* @author    Elsabe Lessing (http://www.lessink.co.za)
*/
class m_Settings extends CI_Model {
    private $table_name = 'settings';

    function __construct() {
        parent::__construct();
    }

    /**
     * get_by_key
     *
     * Get a specific setting by key.
     *
     * @param string $key Description.
     *
     * @access public
     *
     * @return obj or NULL
     */
    function get_by_key($key) {
        $this->db->select($this->table_name.'.*');
        $this->db->where('key', strtolower($key));
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) return $query->row();
        return NULL;
    }

    /**
     * get_by_group
     *
     * Get list of settings by group.
     *
     * @param string $group Description.
     *
     * @access public
     *
     * @return array
     */
    function get_by_group($group) {
        $this->db->where('group', strtolower($group));
        $query = $this->db->get($this->table_name);
        $data = array();
        if ($query && $query->num_rows() > 0) {
            foreach ( $query->result() as $row ) {
                $data[$row->key] = $row->value;
            }
        }
        return $data;
    }

    /**
     * set_key
     *
     * @param string  $key   Description.
     * @param string  $value Description.
     * @param string $group (optional) default:'default'
     *
     * @access public
     *
     * @return void
     */
    function set_key($key, $value, $group='default') {
        if (is_null($value)) $value = '';
        //check if exists
        if (!is_null($this->get_by_key($key))) {
            //update key
            $this->db->where('key', strtolower($key));
            $this->db->update($this->table_name, array('value'=>$value, 'group'=>$group));
        } else {
            //create a new key.
            $this->db->insert($this->table_name, array('key'=>$key, 'value'=>$value, 'group'=>$group));
        }
    }
}
