<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* Users
*
* This model represents user authentication data. It operates the following tables:
* - user account data,
*
* @package Tank_auth
* @author  Ilya Konyukhov (http://konyukhov.com/soft/)
*/
class Users extends CI_Model {
    private $table_name = 'users';          // user accounts
    private $user_client_table_name = 'users_clients';

    function __construct()    {
        parent::__construct();

        $ci =& get_instance();
        $this->table_name = $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
    }

    /**
    * Get the role of auser id
    */
    function get_user_role($user_id) {
        $this->db->where('id', $user_id);
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) {
            $row = $query->row();
            return $row->role;
        } else {
            return FALSE;
        }
    }

    /**
    * Get user record by Id
    *
    * @param   int
    * @param   bool
    * @return  object
    */
    function get_user_by_id($user_id, $activated)    {
        $this->db->where('id', $user_id);
        $this->db->where('activated', $activated ? 1 : 0);

        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) return $query->row();
        return NULL;
    }

    function get_user($user_id) {
        $this->db->where('id', $user_id);

        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) return $query->row();
        return NULL;
    }

    /**
    * Get user record by login (username or email)
    *
    * @param   string
    * @return  object
    */
    function get_user_by_login($login)    {
        $this->db->where('LOWER(username)=', strtolower($login));
        $this->db->or_where('LOWER(email)=', strtolower($login));

        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) return $query->row();
        return NULL;
    }

    /**
    * Get user record by username
    *
    * @param   string
    * @return  object
    */
    function get_user_by_username($username)    {
        $this->db->where('LOWER(username)=', strtolower($username));

        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) return $query->row();
        return NULL;
    }

    /**
    * Get user record by email
    *
    * @param   string
    * @return  object
    */
    function get_user_by_email($email)    {
        $this->db->where('LOWER(email)=', strtolower($email));

        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) return $query->row();
        return NULL;
    }

    /**
    * Get list of ALL users
    *
    * @return  array
    */
    function get_list($group_by_role=false)    {
        $this->db->order_by('username', 'asc');
        $this->db->join('distributors', 'distributors.id = '.$this->table_name.'.distributor_id', 'left');
        $this->db->select ($this->table_name.".*");
        $this->db->select('distributors.name as distributor_name');
        $query = $this->db->get($this->table_name);
        if ($group_by_role) {
            $this->load->model('m_users_clients');
            foreach ($query->result() as $row) {
                $row->clients = $this->m_users_clients->get_client_names($row->id);
                $result[$row->role][] = $row;
            }
            return $result;
        } else {
            return $query->result();
        }
    }

    function get_list_by_distributor($distributor_id, $group_by_role=false, $role=null) {
        if (!is_null($role)) {
            $this->db->where('role', $role);
        }
        $this->db->where('distributor_id', $distributor_id);
        return $this->get_list($group_by_role);
    }

    /**
    * Get list of users that have access to specific client.
    *
    * @return  array
    */
    function get_list_by_client($client_id)    {
        $this->db->join($this->user_client_table_name, $this->user_client_table_name.'.user_id = '.$this->table_name.'.id');
        $this->db->where($this->user_client_table_name.'.client_id', $client_id);
        $this->db->order_by('username', 'asc');
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    /**
    * Get list of users that have access to a site via a client.
    *
    * @param <type> $site_id
    * @return <type>
    */
    function get_list_by_site($site_id)    {
        $this->load->model('m_site');
        $site = $this->m_site->get_by_id($site_id, False, False);
        if ($site) {
            return $this->get_list_by_client($site->client_id);
        } else {
            return array();
        }
    }

    /**
    * Get a list of users from the clients that the admin
    * has access to.
    *
    * We get clients from users_clients table and then need to get
    * all the user associated wiht each of those clients.
    *
    * @param int $user_id
    * @param string $role
    * @return array
    */
    function get_list_by_admin($user_id, $group_by_role=false)     {
        $client_sql = "SELECT client_id FROM ".$this->user_client_table_name." WHERE user_id=".$user_id;
        $sql = "SELECT distinct(uc.user_id), u.*, d.name as distributor_name FROM ".$this->user_client_table_name." uc
        LEFT JOIN ".$this->table_name." u ON uc.user_id = u.id, distributors d
        WHERE client_id IN (".$client_sql.") AND d.id = u.distributor_id";
        $query = $this->db->query($sql);

        if ($group_by_role) {
            $this->load->model('m_users_clients');
            foreach ($query->result() as $row) {
                if ($row->username && $row->password &&$row->email) {
                    $row->clients = $this->m_users_clients->get_client_names($row->user_id);
                    $result[$row->role][] = $row;
                } else {
                    if ($row->user_id) {
    // There is still a link but no user in the database! So remove the link
                        $this->m_users_clients->remove_user_links($row->user_id);
                    }
                }
            }
            if (isset($result)) return $result;
            else return FALSE;
        } else {
            return $query->result();
        }
    }

    /**
    * Get a list of users that are associated with the current user's
    * clients.
    * similar to get list by admin but only returns users wiht a role
    * of user.
    *
    * @param <type> $user_id
    * @param <type> $group_by_role
    * @return <type>
    */
    function get_list_by_user($user_id, $group_by_role=false) {
        $client_sql = "SELECT client_id FROM ".$this->user_client_table_name." WHERE user_id=".$user_id;
        $sql = "SELECT distinct(uc.user_id), u.*, d.name as distributor_name FROM ".$this->user_client_table_name." uc
        LEFT JOIN ".$this->table_name." u ON uc.user_id = u.id, distributors d
        WHERE d.id = u.distributor_id AND client_id IN (".$client_sql.") ";
        $query = $this->db->query($sql);

        if ($group_by_role) {
            $this->load->model('m_users_clients');
            foreach ($query->result() as $row) {
                if ($row->username && $row->password && $row->email) {
                    $row->clients = $this->m_users_clients->get_client_names($row->user_id);
                    $result[$row->role][] = $row;
                } else {
                    if ($row->user_id) {
    // There is still a link but no user in the database! So remove the link
                        $this->m_users_clients->remove_user_links($row->user_id);
                    }
                }
            }
            if (isset($result)) return $result;
            else return FALSE;
        } else {
            return $query->result();
        }
    }

    function get_list_by_role($role) {
        $this->db->where('role', $role);
        return $this->get_list(false);
    }

    /**
    *
    * @param int $user_id
    * @return bool
    */
    function get_site_email_dropdown_array($client_id, $site_id=0, $ignore=array()){
        if (!$client_id && $site_id > 0) {
            $this->load->model('m_site');
            $site = $this->m_site->get_by_id($site_id, False, False);
            $client_id = $site->client_id;
        }
        $this->db->join($this->user_client_table_name, $this->user_client_table_name.'.user_id = '.$this->table_name.'.id');
        $this->db->where($this->user_client_table_name.'.client_id', $client_id);
        $this->db->where($this->table_name.".activated", 1);
        $this->db->order_by("username", "asc");
        $query = $this->db->get($this->table_name);

        $dropdown=array();
        foreach ($query->result() as $user) {
            if (!in_array($user->id, $ignore))
                $dropdown[$user->id] = $user->username." (".$user->email.")";
        }
        return $dropdown;
    }

    /**
    * Check if username available for registering
    *
    * @param   string
    * @return  bool
    */
    function is_username_available($username) {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(username)=', strtolower($username));

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 0;
    }

    /**
    * Check if email available for registering
    *
    * @param   string
    * @return  bool
    */
    function is_email_available($email) {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(email)=', strtolower($email));
        $this->db->or_where('LOWER(new_email)=', strtolower($email));

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 0;
    }

    /**
    * Create new user record
    *
    * @param   array
    * @param   bool
    * @return  array
    */
    function create_user($data, $activated = TRUE) {
        $data['created'] = time();
        $data['activated'] = $activated ? 1 : 0;

        if ($this->db->insert($this->table_name, $data)) {
            $user_id = $this->db->insert_id();
            return array('user_id' => $user_id);
        }
        return NULL;
    }

    /**
    *
    * @param int $user_id
    * @param string $role
    * @return bool
    */
    function change_role($user_id, $role) {
        $this->db->where('id', $user_id);
        if ($this->db->update($this->table_name, array('role'=>$role))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    *
    * @param int $user_id
    * @param string $lang
    * @return bool
    */
    function change_language($user_id, $lang) {
        $this->db->where('id', $user_id);
        if ($this->db->update($this->table_name, array('lang'=>$lang))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    *
    * @param int $user_id
    * @param string $lang
    * @return bool
    */
    function change_timezone($user_id, $tz) {
        $this->db->where('id', $user_id);
        if ($this->db->update($this->table_name, array('timezone'=>$tz))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    * We're assuming you checked i ft he user name is availabe.
    *
    * @param <type> $user_id
    * @param <type> $role
    * @return <type>
    */
    function change_username($user_id, $username) {
        $this->db->where('id', $user_id);
        if ($this->db->update($this->table_name, array('username'=>$username))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    *
    * @param int $user_id
    * @param int $distributor_id
    * @return bool
    */
    function change_distributor($user_id, $distributor_id) {
        $this->db->where('id', $user_id);
        if ($this->db->update($this->table_name, array('distributor_id'=>$distributor_id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    *
    * @param int $user_id
    * @param string $cellphone
    * @return bool
    */
    function change_cellphone($user_id, $cellphone) {
        $this->db->where('id', $user_id);
        if ($this->db->update($this->table_name, array('cellphone'=>$cellphone))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    * check_activation_key
    *
    * @param int $user_id
    * @param string $activation_key
    * @param bool $activate_by_email
    * @return mixed Value.
    */
    function check_activation_key($user_id, $activation_key, $activate_by_email) {
        $this->db->select('1', FALSE);
        $this->db->where('id', $user_id);
        if ( $activation_key !== $this->config->item('activation_keyphrase') ) {
        // This allows us to skip the need for the activation keys!
            if ($activate_by_email) {
                $this->db->where('new_email_key', $activation_key);
            } else {
                $this->db->where('new_password_key', $activation_key);
            }
        }
        $this->db->where("( activated=0 OR verified=0)");
        $query = $this->db->get($this->table_name);

        return $query->num_rows() == 1;
    }

    /**
    * Activate user if activation key is valid.
    * Can be called for not activated users only.
    *
    * @param   int
    * @param   string
    * @param   bool
    * @return  bool
    */
    function activate_user($user_id, $activation_key, $activate_by_email) {
        $verify = false;
        if ( $activation_key !== $this->config->item('activation_keyphrase') ) {
    //This allows us to skip the need for the activation keys!
            $verify = true;
        }
        if ($this->check_activation_key($user_id, $activation_key, $activate_by_email)) {

            $this->db->set('activated', 1);
            if ($activate_by_email) {
                $this->db->set('new_email_key', NULL);
            }
            if ($verify) {
                $this->db->set('verified', 1);
                $this->db->set('new_email', NULL);
            }
            $this->db->where('id', $user_id);
            $this->db->update($this->table_name);

            return TRUE;
        }
        return FALSE;
    }

    /**
    * Purge table of non-activated users
    *
    * @param   int
    * @return  void
    */
    function purge_na($expire_period = 172800) {
        $this->db->where('activated', 0);
        $this->db->where('created <', time() - $expire_period);
        $this->db->delete($this->table_name);
    }

    /**
    * Delete user record
    *
    * @param   int
    * @return  bool
    */
    function delete_user($user_id) {
        $this->db->where('id', $user_id);
        $this->db->delete($this->table_name);
        if ($this->db->affected_rows() > 0) {
            $this->load->model('m_users_clients');
            $this->m_users_clients->remove_user_links($user_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
    * Delete all users associated to specific client_id that aren't
    * added to other clients.
    *
    * @param   int
    * @return  bool
    */
    function delete_user_by_client($client_id) {
        $return = false;
        $this->load->model('m_users_clients');
    //Get users linked to the dead client
        $users = $this->m_users_clients->get_user_ids($client_id);
        foreach ($users as $u) {
            $c = $this->m_users_clients->get_client_ids($u);
            if (count($c) == 1) {
                $this->db->where('id', $u);
                $this->db->delete($this->table_name);
                $return = true;
            }
        }

        return $return;
    }

    function delete_user_by_distributor($distributor_id) {
        $this->db->where('distributor_id', $distributor_id);
        return  $this->db->delete($this->table_name);
    }

    /**
    * Set new password key for user.
    * This key can be used for authentication when resetting user's password.
    *
    * @param   int
    * @param   string
    * @return  bool
    */
    function set_password_key($user_id, $new_pass_key) {
        $this->db->set('new_password_key', $new_pass_key);
        $this->db->set('new_password_requested', time());
        $this->db->where('id', $user_id);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
    * Check if given password key is valid and user is authenticated.
    *
    * @param   int
    * @param   string
    * @param   int
    * @return  void
    */
    function can_reset_password($user_id, $new_pass_key, $expire_period = 900) {
        $this->db->select('1', FALSE);
        $this->db->where('id', $user_id);
        $this->db->where('new_password_key', $new_pass_key);
        $this->db->where('new_password_requested >', time() - $expire_period);

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 1;
    }

    /**
    * Change user password if password key is valid and user is authenticated.
    *
    * @param   int
    * @param   string
    * @param   string
    * @param   int
    * @return  bool
    */
    function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900) {
        $this->db->set('password', $new_pass);
        $this->db->set('new_password_key', NULL);
        $this->db->set('new_password_requested', NULL);
        $this->db->where('id', $user_id);
        $this->db->where('new_password_key', $new_pass_key);
        $this->db->where('new_password_requested >=', time() - $expire_period);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
    * Change user password
    *
    * @param   int
    * @param   string
    * @return  bool
    */
    function change_password($user_id, $new_pass) {
        $this->db->set('password', $new_pass);
        $this->db->where('id', $user_id);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
    * Set new email for user (may be activated or not).
    * The new email cannot be used for login or notification before it is activated.
    *
    * @param   int
    * @param   string
    * @param   string
    * @param   bool
    * @return  bool
    */
    function set_new_email($user_id, $new_email, $new_email_key, $activated) {
        $this->db->set($activated ? 'new_email' : 'email', $new_email);
        $this->db->set('new_email_key', $new_email_key);
        $this->db->set('verified', 0);
        $this->db->where('id', $user_id);
        $this->db->where('activated', $activated ? 1 : 0);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
    * Activate new email (replace old email with new one) if activation key is valid.
    *
    * @param   int
    * @param   string
    * @return  bool
    */
    function activate_new_email($user_id, $new_email_key) {
        $this->db->set('verified', 1);
        $this->db->set('email', 'new_email', FALSE);
        $this->db->set('new_email', NULL);
        $this->db->set('new_email_key', NULL);
        $this->db->where('id', $user_id);
        $this->db->where('new_email_key', $new_email_key);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
    * Update user login info, such as IP-address or login time, and
    * clear previously generated (but not activated) passwords.
    *
    * @param   int
    * @param   bool
    * @param   bool
    * @return  void
    */
    function update_login_info($user_id, $record_ip, $record_time) {
        $this->db->set('new_password_key', NULL);
        $this->db->set('new_password_requested', NULL);

        if ($record_ip)     $this->db->set('last_ip', $this->input->ip_address());
        if ($record_time)   $this->db->set('last_login', time());

        $this->db->where('id', $user_id);
        $this->db->update($this->table_name);
    }

    /**
    * Ban user
    *
    * @param   int
    * @param   string
    * @return  void
    */
    function ban_user($user_id, $reason = NULL) {
        $this->db->where('id', $user_id);
        $this->db->update($this->table_name, array(
            'banned'        => 1,
            'ban_reason'    => $reason,
            ));
    }

    /**
    * Unban user
    *
    * @param   int
    * @return  void
    */
    function unban_user($user_id) {
        $this->db->where('id', $user_id);
        $this->db->update($this->table_name, array(
            'banned'        => 0,
            'ban_reason'    => NULL,
            ));
    }

    function search($keyword) {
        $this->db->or_like('username', $keyword);
        $this->db->or_like('email', $keyword);
        $this->db->or_like('cellphone', $keyword);
        $query = $this->db->get($this->table_name);
        return $query->result();
    }
}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */
