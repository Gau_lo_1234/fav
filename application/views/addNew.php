
<?php 
include APPPATH."phpqrcode/qrlib.php";
include APPPATH."config/connections.php";
$id=isset($_GET['id']);
 $sql_info = "SELECT * FROM fa.devices WHERE id='$id' LIMIT 1";
            $result = mysqli_query($con, $sql_info);
            $row = mysqli_fetch_array($result);
            $sql_settings=mysqli_query($con, "SELECT * FROM fa.server_settings WHERE company='".$row['company']."'");
            $row_comp=mysqli_fetch_array( $sql_settings);

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Device Management
        <small>Add New User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->

                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->

                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form action="<?php echo base_url() ?>addNewUser" method="post" >
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control required" value="<?php echo set_value('fname'); ?>" id="fname" name="fname" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="text" class="form-control required email" id="email" value="<?php echo set_value('email'); ?>" name="email" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control required" id="password" name="password" maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control required equalTo" id="cpassword" name="cpassword" maxlength="20">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control required digits" id="mobile" value="<?php echo set_value('mobile'); ?>" name="phone" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <select class="form-control required" id="role" name="role">
                                            <option value="0">Select Role</option>
                                            <?php
                                            if(!empty($roles))
                                            {
                                                foreach ($roles as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->role ?>" <?php if($rl->role == set_value('role')) {echo "selected=selected";} ?>><?php echo $rl->role ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>    
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" id="submit" />
                            <button class="btn btn-default">QR Generate</button>

                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    <?php


            if(isset($_GET['generate'])) {
                $PNG_TEMP_DIR = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;

                //html PNG location prefix
                $PNG_WEB_DIR = 'phpqrcode/temp/';
                $filename=md5($_GET['id']).".png";
                $PNG_TEMP_FILE=$PNG_WEB_DIR.$filename;


                $result=mysqli_query($con, $sql_info);
                $response=array();
                $id;
                if($result->num_rows>0){

                    while($row=mysqli_fetch_assoc($result)){
                        $id=$row['id'];
                        array_push($response, array("id"=>$row["id"], "imei"=>$row["imei"], "email"=>$row["email"], "company"=>$row["company"], "phone"=>$row["phone"], "Server_Name"=>$row_comp["server_name"], "Server_IP"=>$row_comp["server_ip"], "Server_Port"=>$row_comp["server_port"]));
                    }
                    //echo json_encode($response);
                     /**
                    * The following query creates a jason object
                    */
                    $res=md5(json_encode($response));
                    $url="http://192.168.2.193/admin/";


                    mysqli_query($con, "UPDATE fa.devices SET encrypt='$res' WHERE id='$id'");

                    QRcode::png($res, $PNG_TEMP_FILE, 'L', 4, 2);
                    echo "
<div class='col-lg-6' style='margin-top: 30px;'><button class='btn btn-primary' type='button' data-toggle='collapse' data-target='#collapseExample1' aria-expanded='false' aria-controls='collapseExample'>Generated QR Code
                        </button> <div class='collapse' id='collapseExample1'>
                <div class='card card-body'>

                    <div id='details'><form method='post' action='mail.php'>
  <div align='center' class='col-lg-6'>
 <div class=\"form-group\" '>
<div class='col-lg-12' align='left' style='margin-top:30px;'><img src='".$PNG_TEMP_FILE."'></div>
<input type='hidden' name='email'>
<br>
<p></p>
<div class='col-lg-12' align='left' style='margin-top:10px;'><button type='text' class='btn btn-default col-m-8'>Send Email</button></div>
</div>
</div>
            
</div>

      </form></div>
</div>
            
</div>";
                }
                else{

                     //QRcode::png($filename, $PNG_TEMP_FILE, 'L', 4, 2);
                    //echo "<img src='".$PNG_TEMP_FILE."'>";
                    // echo mysqli_num_rows($result);
                    echo "ddnt work";
                }
            }

?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
