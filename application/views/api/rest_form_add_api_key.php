<div class="message-inbox no-height">
    <div class="content-box">
        <form method="<?php echo $form_method; ?>" name="rest_form" id="rest_form" action="<?php echo base_url(); ?>API/<?php echo $form_action; ?>" style="width: auto; margin: 50px; width: 640px; padding: 50px; border: 1px solid #eee;">
            <table class="table table-hover text-center">
                <thead style="font-family: arial; text-align: left;">
                    <tr>
                        <th colspan="2">
                            <h1 style="background-color: #efefef; padding: 15px;"><?php echo $page_heading; ?></h1>
                        </th>
                    </tr>
                </thead>
                <tbody style="font-family: arial; font-size: 13px;">
                    <tr>
                        <td style="width: 160px; padding-bottom: 8px;"><strong>Key level (1 - 10)</strong></td>
                        <td style="width: 480px; padding-bottom: 8px;"><input style="font-size: 13px; font-style: italic; width: 360px; padding: 10px; border: 1px solid #eeeeee;" type="text" name="level" id="level"></td>
                    </tr>
                    <tr>
                        <td style="width: 160px; padding-bottom: 8px;"><strong>Ignore limits? (0/1)</strong></td>
                        <td style="width: 480px; padding-bottom: 8px;"><input style="font-size: 13px; font-style: italic; width: 360px; padding: 10px; border: 1px solid #eeeeee;" type="text" name="ignore_limits" id="ignore_limits"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="hidden" name="X-API-KEY" id="X-API-KEY" value="<?php echo $api_key; ?>">
                            <input type="hidden" name="_method" value="PUT">
                            <div style="border: 1px solid #1B74BA; width: 238px;"><input style="width: 100%; cursor: pointer; font-weight: bold; padding: 10px 0; text-align: center; border: 1px solid #ffffff; background-color: #1B74BA; color: #ffffff;" type="submit" name="submit" id="submit" value="<?php echo $form_submit; ?>"></div>
                            <p><a href="<?php echo base_url(); ?>API_forms/">Back to API demo index</a></p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>

