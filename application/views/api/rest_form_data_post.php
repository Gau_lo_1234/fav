<div class="box">
    <div class="box-body">
        <form method="<?php echo $form_method; ?>" name="rest_form" id="rest_form" action="<?php echo base_url(); ?>API/<?php echo $form_action; ?>" style="width: auto; margin: 50px; width: 640px; padding: 50px; border: 1px solid #eee;">
            <table class="table table-bordered">
                <tbody >
                    <tr>
                        <td ><strong>imei</strong></td>
                        <td ><input class="form-control" type="text" value="010000107AD3B1E2" name="imei" id="imei"></td>
                    </tr>
                     <tr>
                        <td ><strong>real_email</strong></td>
                        <td ><input class="form-control" type="text" value="real@lessink.co.za" name="real_email" id="real_email"></td>
                    </tr>
                     <tr>
                        <td ><strong>user_email</strong></td>
                        <td ><input class="form-control" type="text" value="user@lessink.co.za" name="user_email" id="user_email"></td>
                    </tr>
                     <tr>
                        <td ><strong>name</strong></td>
                        <td ><input class="form-control" type="text" value="Lessink" name="name" id="name"></td>
                    </tr>
                     <tr>
                        <td ><strong>number</strong></td>
                        <td ><input class="form-control" type="text" value="0824692527" name="number" id="number"></td>
                    </tr>
                    <tr>
                        <td ><strong>contacts</strong></td>
                        <td ><input class="form-control" type="text" value="1,2,3,4" name="contacts" id="contacts_1"></td>
                    </tr>
                    <!--  <tr>
                        <td ><strong>contacts 1</strong></td>
                        <td ><input class="form-control" type="text" value="0111231234" name="contacts[]" id="contacts_1"></td>
                    </tr>
                     <tr>
                        <td ><strong>contacts 2</strong></td>
                        <td ><input class="form-control" type="text" value="0111231234" name="contacts[]" id="contacts_2"></td>
                    </tr>
                    <tr>
                        <td ><strong>contacts 3</strong></td>
                        <td ><input class="form-control" type="text" value="0111231234" name="contacts[]" id="contacts_3"></td>
                    </tr> -->
                    <tr>
                        <td ><strong>gps 2</strong></td>
                        <td ><input class="form-control" type="text" value="" name="gps" id="gps"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="hidden" name="X-API-KEY" id="X-API-KEY" value="<?php echo $api_key; ?>">
                            <input class="btn btn-info pull-right" type="submit" name="submit" id="submit" value="<?php echo $form_submit; ?>">
                            <p><a href="<?php echo base_url(); ?>API_forms/">Back to API demo index</a></p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>

