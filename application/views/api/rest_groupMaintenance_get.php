<?php

// echo "<pre>".var_dump($groups['groups']);
// echo "<pre>".var_dump($groups['groupMembers']);
?>

<div class="box">
    <div class="box-body">
        <h2>Groups you are listed within (Not created by yourself)</h2>
        <h3> <?php echo $member->memberName;?></h3>
        <form method="<?php echo $form_method; ?>" name="rest_form" id="rest_form" action="<?php echo base_url(); ?>API/<?php echo $form_action; ?>" style="width: auto; margin: 50px; width: 640px; padding: 50px; border: 1px solid #eee;">
    <?php foreach($groups['groups'] as $group):?>
            <table class="table table-bordered">
                <h1><?php echo $group->group_name;?></h1>
                <thead>
                    <tr>
                        <th>Member Name</th><th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($groups['groupMembers'] as $member):?>

                        <?php if($member->groupID == $group->groupID):?>
                            <tr>
                                <td><?php echo $member->memberName?></td><td>Action To Do</td>
                            </tr>
                        <?php endif;?>

                    <?php endforeach;?>
                </tbody>

<!--                 <tbody >
                     <tr>
                        <td ><strong>Member ID</strong></td>
                        <td ><input class="form-control" type="text" value="1" name="member_id" id="member_id" required/></td>
                    </tr>
                    <tr>
                        <td ><strong>AND</strong></td><td></td>  
                        </tr>
                        <tr>                                            
                        <td ><strong>Group ID</strong></td>
                        <td ><input class="form-control" type="text" value="1" name="group_id" id="group_id"/></td>
                    </tr>
                    <tr>
                        <td ><strong>REQUIRED</strong></td><td></td>  
                        </tr>
                        <tr>                                            
                        <td ><strong>New Group Name</strong></td>
                        <td ><input class="form-control" type="text" value="New Group Name" name="new_group_name" id="new_group_name" required/></td>
                    </tr>                 
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="hidden" name="X-API-KEY" id="X-API-KEY" value="<?php echo $api_key; ?>">
                            <input class="btn btn-info pull-right" type="submit" name="submit" id="submit" value="<?php echo $form_submit; ?>">
                            <p><a href="<?php echo base_url(); ?>API_forms/">Back to API demo index</a></p>
                        </td>
                    </tr>
                </tbody> -->
            </table>
    <?php endforeach;?>
        </form>
    </div>
</div>