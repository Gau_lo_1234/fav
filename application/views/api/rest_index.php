<div class="box">
    <div class="box-body">
        <table class="table table-bordered">
            <tbody style="">
                <tr>
                    <td ><strong>API Keys</strong></td>
                    <td>
                        <a href="<?php echo base_url(); ?>API_forms/api_key_add">Add an API key</a><br />
                        <a href="<?php echo base_url(); ?>API_forms/api_key_delete">Delete an API key</a><br />
                        <a href="<?php echo base_url(); ?>API_forms/api_key_level_update">Update an API key level</a><br />
                        <a href="<?php echo base_url(); ?>API_forms/api_key_suspend">Suspend an API key</a><br />
                        <a href="<?php echo base_url(); ?>API_forms/api_key_regenerate">Regenerate an API key</a>
                    </td>
                </tr>
                <tr>
                    <td ><strong>General</strong></td>
                    <td ><a href="<?php echo base_url(); ?>API_forms/app_setup_post">Initial Setup when Installing App</a><br />
                         <a href="<?php echo base_url(); ?>API_forms/app_groupDetails_get">Get Group Details</a><br />
                         <a href="<?php echo base_url(); ?>API_forms/app_groupMemberDetails_get">Get Group Membership Details</a><br />
                         <a href="<?php echo base_url(); ?>API_forms/app_communityGroups_get">Get Community Groups</a><br />                         
                         <a href="<?php echo base_url(); ?>API_forms/app_changeGroupName_post">Change Group Name</a><br />
                        <a href="<?php echo base_url(); ?>API_forms/app_groupMaintenance_post">Group Maintenance (Remove - Suspend)</a><br />
                         <a href="<?php echo base_url(); ?>API_forms/app_groupAddmember_post">Add Group Member</a><br />
                         <a href="<?php echo base_url(); ?>API_forms/app_Search_post">Search MemberList / GroupList</a><br />
                         <a href="<?php echo base_url(); ?>API_forms/app_joinRequests_post">Join Requests (Both Sent and Received)</a><br />                                                                      
<!--                         <a href="<?php echo base_url(); ?>API_forms/api_data_post">Data Post</a><br />
                        <a href="<?php echo base_url(); ?>API_forms/api_data_namepair_post">Data Name Pair Post</a><br />
                        <a href="<?php echo base_url(); ?>API_forms/api_assoc_installs_get">Get Associated Installs</a><br />
                        <a href="<?php echo base_url(); ?>API_forms/api_data_get">Data Get</a><br />         
                        <a href="<?php echo base_url(); ?>API_forms/api_data_name_number_get">Data Name Number Get</a><br />  -->             
                    </td>
                </tr>
            <tr>
                <td><strong>Advanced</strong></td>
                <td><a href="<?php echo base_url(); ?>API_forms/app_device_post">Add Device</a><br />
                <a href="<?php echo base_url(); ?>API_forms/app_setup_post">Add Distributor</a><br />
                <a href="<?php echo base_url(); ?>API_forms/app_setup_post">Add Users</a><br />
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
