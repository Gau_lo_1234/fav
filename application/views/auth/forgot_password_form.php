
<div id="login_box">
    <?php
    $login = array(
            'name'	=> 'login',
            'id'	=> 'login',
            'value' => set_value('login', isset($selected_login) ? $selected_login : ''),
            'maxlength'	=> 80,
            'size'	=> 20,
            'class' => 'text_input',
    );
    if ($this->config->item('use_username', 'tank_auth')) {
            $login_label = 'Email or login';
    } else {
            $login_label = 'Email';
    }
    ?>
        <h2>Reset Password</h2><br/>
    <?php echo form_open($this->uri->uri_string(), 'id="login_form"'); ?>

        <?php echo form_label($login_label, $login['id']); ?>
        <?php echo form_input($login); ?>
                <br class="clrflt" />
        <div class="error">
        <?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
        </div>
        <br class="clrflt" />

    <button type="submit" name="reset" class="fltr"><img src="<?php echo base_url() ?>assets/images/mail.png" /> Get a new password</button>
    <?php echo form_close(); ?>
        <br class="clrflt" />
</div><!--login_box-->


