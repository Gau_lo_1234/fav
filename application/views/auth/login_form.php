<?php
if ($this->database_status->isOnline()) {

    $login = array(
        'name'  => 'login',
        'id'    => 'login',
        'value' => set_value('login', (isset($selected_login) ? $selected_login : '')),
        'maxlength' => 80,
        'size'  => 20,
        'class'=>'text_input'
    );
    if ($login_by_username AND $login_by_email) {
        $login_label = 'Email or login';
    } else if ($login_by_username) {
        $login_label = 'Login';
    } else {
        $login_label = 'Email';
    }
    $password = array(
        'name'  => 'password',
        'id'    => 'password',
        'size'  => 20,
        'class'=>'text_input'
    );
    $remember = array(
        'name'  => 'remember',
        'id'    => 'remember',
        'value' => 1,
        'checked'   => set_value('remember'),
    );
    $captcha = array(
        'name'  => 'captcha',
        'id'    => 'captcha',
        'maxlength' => 8,
    );
    ?>

<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo site_url(); ?>"><b>FA</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">
        <?php if (isset($errors[$login['name']]) || form_error($login['name']) || form_error($password['name']) || isset($errors[$password['name']])) { ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Incorrect username / password.
              </div>
        <?php } else { ?>
            Sign in to start your session
        <?php } ?>
    </p>

     <?php echo form_open($this->uri->uri_string()); ?>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Email" name="<?php echo $login['name'] ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="<?php echo $password['name'] ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

        <?php //*
        if ($show_captcha) {
            echo '<br class="clrflt"/>';
            if ($use_recaptcha) {

                echo $recaptcha_html;

                ?>
                <div class="error"><?php echo form_error('g-recaptcha-response'); ?></div>
            <?php } else { ?>

                <p>Enter the code exactly as it appears:</p>
                <?php echo $captcha_html; ?>
                        <br/>
                <?php echo form_input($captcha); ?>
                <div class="error">
                <?php echo form_error($captcha['name']); ?>
                </div>

            <?php } // use_recaptcha
            } //show_Captcha //*/ ?>

            <?php
            if (isset($return_URL)) {
                echo form_hidden('return_URL', $return_URL);
            }
            ?>

      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="<?php echo $remember['name'] ?>" value="<?php echo $remember['value'] ?>"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close(); ?>

    <!-- <a href="#">I forgot my password</a><br> -->
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<?php } else { ?>
<div class="container">
    <h2>SERVER MAINTENANCE</h2>
    <p>Online guarding is currently offline due to server maintenance. Please check back later.</p>
    <p>Any clocking data sent to the server will be stored and imported when we're back online.</p>
</div>
<?php } ?>