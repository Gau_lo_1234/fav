
<?php if (isset($email_exists) && $email_exists) { ?>
 <script tyle="text/javascript">
$(function() {
    $('#email-already-used').overlay({
        top: 100,
        mask: {
                color: '#fff',
                loadSpeed: 200,
                opacity: 0.5
        },
        closeOnClick: false,
        load: true
    });
});
</script>
<div id="email-already-used" class="overlay-wide">
    <h3>Registration email address "<?php echo set_value('email') ?>" is already registered on the system.</h3>
    <br/>
    <p>To access the email address you're attempting to use, try logging in or using the forgotten password link to reset the user's password.</p>
    <p style="text-align:center;">
        <a class="button " href="<?php echo site_url('/auth/login?login='.set_value('email')) ?>"><img src="<?php echo base_url() ?>assets/images/key.png"/> Login</a>
        <a class="button " href="<?php echo site_url('/auth/forgot_password?login='.set_value('email')) ?>"><img src="<?php echo base_url() ?>assets/images/lock.png" width="16" height="16" /> Reset password</a>
    </p>
    <br/>
    <p>If you are attempting to register a new profile, please supply a different email address.</p>
    <p style="text-align:center;"><button class="close cancel">Register with different email address</button>
</div>
<?php } ?>
 <?php
if ($use_username) {
    $username = array(
        'name'  => 'username',
        'id'    => 'username',
        'value' => set_value('username'),
        'maxlength' => $this->config->item('username_max_length', 'tank_auth'),
        'size'  => 30,
        'class'=>'text_input',
        'title'=>'You can login to the site using either a username or your email address.'
    );
}
if ($use_code) {
    $code = array(
        'name'  => 'code',
        'id'    => 'code',
        'value' => set_value('code'),
        'maxlength' => 80,
        'size'  => 30,
        'class'=>'text_input',
        'title'=>'This online registration code should be available from your distributor.'
    );
}
$email = array(
    'name'  => 'email',
    'id'    => 'email',
    'value' => set_value('email'),
    'maxlength' => 80,
    'size'  => 30,
    'class'=>'text_input',
    'title'=>'Please specify a valid email address. <br/>Your email address needs to be verified<br/> before you can use the system.'
);
$password = array(
    'name'  => 'password',
    'id'    => 'password',
    'value' => set_value('password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size'  => 30,
    'class'=>'text_input',
    'title'=>'Password should be '.$this->config->item('password_min_length', 'tank_auth').' - '.$this->config->item('password_max_length', 'tank_auth').' characters long.<br/> Alpha-numeric characters, underscores and<br/> dashes are allowed. ',
);
$confirm_password = array(
    'name'  => 'confirm_password',
    'id'    => 'confirm_password',
    'value' => set_value('confirm_password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size'  => 30,
    'class'=>'text_input',
    'title'=>''
);
$captcha = array(
    'name'  => 'captcha',
    'id'    => 'captcha',
    'maxlength' => 8,
    'class'=>'text_input'
);
$cellphone = array(
    'name'  => 'cellphone',
    'id'    => 'cellphone',
    'value' => set_value('cellphone'),
    'maxlength' => 20,
    'size'  => 30,
    'class'=>'text_input',
    'title' => "Example: +27 11 123 1234 or 082 123 1234",
);
$client_name = array(
    'name'  => 'client_name',
    'id'    => 'client_name',
    'value' => set_value('client_name'),
    'size'  => 30,
    'class'=>'text_input',
    'title' => "The name of your organization.",
);
$contact_person = array(
    'name'  => 'contact_person',
    'id'    => 'contact_person',
    'size'  => 20,
    'value' => set_value('contact_person'),
    'title' =>  'Name of the contact person at your organization.',
    'class'=>'text_input',
);

$site_name = array(
    'name'  => 'site_name',
    'id'    => 'site_name',
    'size'  => 20,
    'value' => set_value('site_name'),
    'title' => 'Name of your first site. Choose a descriptive name so <br/>that you can easily identify the site. This name can be<br/> changed later.',
    'class'=>'text_input',
);
$magcell_code = array(
    'name'  => 'magcell_code',
    'id'    => 'magcell_code',
    'size'  => 20,
    'value' => set_value('magcell_code'),
    'title' => 'This is a 6 digit number found on the inside of your Magcell.',
    'class'=>'text_input',
);

$report_emails = array(
    'name'  => 'report_emails',
    'id'    => 'report_emails',
    'size'  => 20,
    'value' => set_value('report_emails'),
    'title' => 'Detailed Clocking and Overview Clocking reports will be emailed to the following email addresses at 9:00 everyday.',
    'class'=>'text_input',
);

$additional_emails = array(
    'name'  => 'additional_emails',
    'id'    => 'additional_emails',
    'size'  => 20,
    'value' => set_value('additional_emails'),
    'title' => '',
    'class'=>'text_input',
    'options' => array('0'=>'None')
);
for($i=1; $i<=REGISTER_REPORT_NUMBER; $i++) {
    $additional_emails['options'][] = $i;
}

?>
<div class="container">
<div id="register_box">
    <h2>Online Guarding Registration</h2><br/>
    <p><strong>If you already have a username and password for the system you should go to the <?php echo isset($login_link) ? anchor($login_link, 'login page' ) : anchor( site_url('auth/login/'), 'login page', 'target="_blank"') ?>.</strong></p>
    <br/>

    <?php
    $form_errors = validation_errors();
    if ($form_errors) {
    ?>
    <div class="flash">
        <div class="message error">
            <h3 style="color:red;">There are the following errors on your registration form:</h3>
            <p>

            <?php echo $form_errors; ?>
        </p></div>
    </div>
    <?php } ?>

    <?php echo form_open($this->uri->uri_string(), 'id="register_form"'); ?>

    <?php if ($use_code) { ?>

        <?php echo form_label('Registration Code <span>*</span>', $code['id']); ?>
        <?php echo form_input($code); ?>
        <br class="clrflt"/>
        <div class="error"><?php echo form_error($code['name']); ?><?php echo isset($errors[$code['name']])?$errors[$code['name']]:''; ?></div>
        <br class="clrflt"/>

        <p>
            This online registration code enables you to complete the automatic registration.<br/>
            This code is available from your distributor and is not the same as the Magcell Code.
        </p>
        <br class="clrflt"/>

    <?php } ?>

    <fieldset>
    <legend>Registration Details</legend>

        <?php echo form_label('Organization Name <span>*</span>'); ?>
        <?php echo form_input($client_name); ?>
        <?php if ($client_name['title']) { ?>
            &nbsp;<img  class="form-help-icon" rel="#client_name-help" src="<?php echo base_url() ?>assets/images/help.png" width="16" height="16" />
            <div class="overlay" id="client_name-help"><?php echo $client_name['title'] ?></div>
        <?php } ?>
        <br class="clrfltl"/>
        <div class="error"><?php echo form_error($client_name['name']); ?><?php echo isset($errors[$client_name['name']])?$errors[$client_name['name']]:''; ?></div>
        <br class="clrfltl"/>

        <?php echo form_label($this->lang->line('contact_person').' <span>*</span>', $contact_person['id']); ?>
        <?php echo form_input($contact_person); ?>
        <?php if ($contact_person['title']) { ?>
            &nbsp;<img  class="form-help-icon" rel="#contact_person-help" src="<?php echo base_url() ?>assets/images/help.png" width="16" height="16" />
            <div class="overlay" id="contact_person-help"><?php echo $contact_person['title'] ?></div>
        <?php } ?>
        <br class="clrfltl"/>
        <div class="error">
        <?php echo form_error($contact_person['name']); ?><?php echo isset($errors[$contact_person['name']])?$errors[$contact_person['name']]:''; ?>
        </div>
        <br class="clrfltl"/>

        <?php echo form_label("Email Address <span>*</span>", $email['id']); ?>
        <?php echo form_input($email); ?>
        <?php if ($email['title']) { ?>
            &nbsp;<img  class="form-help-icon" rel="#client_email-help" src="<?php echo base_url() ?>assets/images/help.png" width="16" height="16" />
            <div class="overlay" id="client_email-help"><?php echo $email['title'] ?></div>
        <?php } ?>
        <br class="clrfltl"/>
        <div class="error">
        <?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>
        </div>
        <br class="clrfltl"/>

        <?php echo form_label('Cellphone <span>*</span>'); ?>
        <?php echo form_input($cellphone); ?>
        <?php if ($cellphone['title']) { ?>
            &nbsp;<img  class="form-help-icon" rel="#cellphone-help" src="<?php echo base_url() ?>assets/images/help.png" width="16" height="16" />
            <div class="overlay" id="cellphone-help"><?php echo $cellphone['title'] ?></div>
        <?php } ?>
        <br class="clrfltl"/>
        <div class="error"><?php echo form_error($cellphone['name']); ?><?php echo isset($errors[$cellphone['name']])?$errors[$cellphone['name']]:''; ?></div>
        <br class="clrfltl"/>


        <?php echo form_label('Password <span>*</span>', $password['id']); ?>
        <?php echo form_password($password); ?>
        <?php if ($password['title']) { ?>
            &nbsp;<img  class="form-help-icon" rel="#password-help" src="<?php echo base_url() ?>assets/images/help.png" width="16" height="16" />
            <div class="overlay" id="password-help"><?php echo $password['title'] ?></div>
        <?php } ?>
        <br class="clrfltl"/>
        <div class="error"><?php echo form_error($password['name']); ?></div>
        <br class="clrfltl"/>

        <?php echo form_label('Confirm Password <span>*</span>', $confirm_password['id']); ?>
        <?php echo form_password($confirm_password); ?>
        <?php if ($confirm_password['title']) { ?>
            &nbsp;<img  class="form-help-icon" rel="#confirm_password-help" src="<?php echo base_url() ?>assets/images/help.png" width="16" height="16" />
            <div class="overlay" id="confirm_password-help"><?php echo $confirm_password['title'] ?></div>
        <?php } ?>
        <br class="clrfltl"/>
        <div class="error"><?php echo form_error($confirm_password['name']); ?></div>
        <br class="clrfltl"/>


        <?php echo form_label('Site Name <span>*</span>', $site_name['id']); ?>
        <?php echo form_input($site_name); ?>
        <?php if ($site_name['title']) { ?>
            &nbsp;<img  class="form-help-icon" rel="#site_name-help" src="<?php echo base_url() ?>assets/images/help.png" width="16" height="16" />
            <div class="overlay" id="site_name-help"><?php echo $site_name['title'] ?></div>
        <?php } ?>
        <br class="clrfltl"/>
        <div class="error"><?php echo form_error($site_name['name']); ?><?php echo isset($errors[$site_name['name']])?$errors[$site_name['name']]:''; ?></div>
        <br class="clrfltl"/>

        <?php echo form_label('Magcell Code <span>*</span>', $magcell_code['id']); ?>
        <?php echo form_input($magcell_code); ?>
        <?php if ($magcell_code['title']) { ?>
            &nbsp;<img  class="form-help-icon" rel="#magcell_code-help" src="<?php echo base_url() ?>assets/images/help.png" width="16" height="16" />
            <div class="overlay" id="magcell_code-help"><?php echo $magcell_code['title'] ?></div>
        <?php } ?>
        <br class="clrfltl"/>
        <div class="error"><?php echo form_error($magcell_code['name']); ?><?php echo isset($errors[$magcell_code['name']])?$errors[$magcell_code['name']]:''; ?></div>
        <br class="clrfltl"/>

    </fieldset>

<?php if (REGISTER_REPORT_NUMBER > 0): ?>
    <br class="clrflt"/>

    <fieldset>
        <legend>Automated Reports</legend>
        <br class="clrfltl"/>
        <p>Detailed Clocking and Overview Clocking reports will be emailed to<br/> the following email addresses at 9:00 everyday.</p>
        <br class="clrfltl"/>

        <?php echo form_label('Number of additional email addresses <span>*</span>', $additional_emails['id'], array('style'=>"width:267px;") ); ?>
        <?php echo form_dropdown($additional_emails['name'], $additional_emails['options'], $additional_emails['value'], ' id="'.$additional_emails['id'].'" '); ?>
        <br class="clrfltl"/>
        <br class="clrfltl"/>

        <div id="email_addresses_box" style="display:none">
            <?php for ($i=1; $i <= REGISTER_REPORT_NUMBER; $i++) { ?>
                <?php
                $tmp = $report_emails;
                $tmp['id'] = $tmp['id'].'_'.$i;
                $tmp['name'] = $tmp['name'].'_'.$i;
                $tmp['value'] = set_value($tmp['name']);
                ?>
                <div id="<?php echo $tmp['id'].'_box' ?>" class="email_field_box" style="display:none;">
                    <?php echo form_label( ($i == 1 ? 'Email Addresses <span>*</span>' : '&nbsp;') ); ?>
                    <?php echo form_input($tmp); ?>
                    <br class="clrfltl"/>
                    <div class="error"><?php echo form_error($tmp['name']); ?><?php echo isset($errors[$tmp['name']])?$errors[$tmp['name']]:''; ?></div>
                    <br class="clrfltl"/>
                </div>
            <?php } ?>
        </div>

    </fieldset>
<?php endif; ?>

    <?php if ($captcha_registration) {
            if ($use_recaptcha) { ?>

                <?php echo $recaptcha_html; ?>
                <div class="error"><?php echo form_error('recaptcha_response_field'); ?></div>
                <br class="clrflt"/>
            <?php } else { ?>

                <div id="captcha">
                    <p>Enter the code exactly as it appears:</p>
                    <?php echo $captcha_html; ?>
                    <br class="clrflt"/>
                </div>
                <br class="clrflt"/>

            <?php echo form_label('Confirmation Code <span>*</span>', $captcha['id']); ?>
            <?php echo form_input($captcha); ?>
            <br class="clrflt"/>
            <div class="error"><?php echo form_error($captcha['name']); ?></div>
    <br class="clrflt"/>
    <?php }
    } ?>

    <button type="submit" class="fltr"><img src="<?php echo base_url() ?>assets/images/key.png" /> Register</button>
    <?php echo form_close(); ?>

            <br class="clrflt"/>
</div>
</div>

 <br class="clrflt"/>

<?php $this->hooks->call('register_screen'); ?>


