
<h2>User Activation</h2>

<p>Your user has not yet been activated. You should have received an email to
    confirm your email address, if you have not received the email please enter the
    correct email address in the box below and we will resend it to you.</p>

<p>If you see this message after the process please contact your application
    administrator to assist.</p>

<?php
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>
<?php echo form_open($this->uri->uri_string()); ?>
<table>
	<tr>
		<td><?php echo form_label('Email Address', $email['id']); ?></td>
		<td><?php echo form_input($email); ?></td>
		<td style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></td>
	</tr>
</table>
<?php echo form_submit('send', 'Send Activation Email'); ?>
<?php echo form_close(); ?>
 
