<?php
$role=$this->tank_auth->get_user_role();
?><?php
if($role == ROLE_ADMIN) {
    ?>

    <div class="col-md-3 col-sm-6 col-xs-12">
  <div class="small-box bg-blue">
      <div class="inner">
        <h3><?php echo isset($request_logs_counts) ? $request_logs_counts : 0 ?></h3>

        <p>Request Logs</p>
      </div>
      <div class="icon">
        <i class="fa fa-files-o"></i>
      </div>
      <a href="<?php echo site_url('requests'); ?>" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo isset($groups_counts) ? $groups_counts : 0 ?></h3>

        <p>Groups</p>
      </div>
      <div class="icon">
        <i class="fa fa-comments-o"></i>
      </div>
      <a href="<?php echo site_url('groups'); ?>" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?php echo isset($distributor_counts) ? $distributor_counts : 0 ?></h3>

                <p>Distributors</p>
            </div>
            <div class="icon">
                <i class="fa fa-lock"></i>
            </div>
            <a href="<?php echo site_url('distributors'); ?>" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
                &nbsp;
                <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
            </a>
        </div>
    </div>
    <?php
}
?>
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?php echo isset($devices_counts) ? $devices_counts : 0 ?></h3>

            <p>Devices</p>
        </div>
        <div class="icon">
            <i class="fa fa-user"></i>
        </div>
        <a href="<?php echo site_url('devices'); ?>" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
            &nbsp;
            <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
        </a>
    </div>
</div>
<?php
if($role == ROLE_DISTRIBUTOR) {
    ?>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-purple">
            <div class="inner">
                <h3><?php echo isset($devices_counts) ? $devices_counts : 0 ?></h3>

                <p>Users</p>
            </div>
            <div class="icon">
                <i class="fa fa-user"></i>
            </div>
            <a href="<?php echo site_url('devices'); ?>" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
                &nbsp;
                <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
            </a>
        </div>
    </div>
    <?php

}