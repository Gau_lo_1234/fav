<!-- CSS -->
<link href="<?php echo site_url() ?>assets/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url() ?>assets/css/style.css?v=0510162" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url() ?>assets/css/jquery_ui_themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
<?php echo $this->pixie_css->html(); ?>

<!-- Javascript -->
<!--[if IE]>
<script type="text/javascript">
    isIE = true;
</script>
<![endif]-->
<script type="text/javascript">
var site_url = "<?php echo site_url() ?>";
var asset_url = "<?php echo ASSET_URL ?>";
var current_user = "<?php echo $this->tank_auth->get_user_id() ?>";
var current_page = "<?php echo $this->uri->uri_string() ?>";
</script>
<script type="text/javascript" src="<?php echo site_url() ?>assets/js/jquery-1.7.1.min.js?v=111213"></script>
<script type="text/javascript" src="<?php echo site_url() ?>assets/js/jquery.tools.min.js?v=111213"></script>

<script type="text/javascript" src="<?php echo site_url() ?>assets/js/jquery-ui-1.9.1.custom.min.js?v=111213"></script>
<script type="text/javascript" src="<?php echo site_url() ?>assets/js/jquery.bgiframe-2.1.2.js?v=111213"></script>
<script type="text/javascript" src="<?php echo site_url() ?>assets/js/jquery-ui-timepicker-addon.js?v=111213"></script>
<?php /* ?><script type="text/javascript" src="<?php echo site_url() ?>assets/js/jquery.tablesorter.js"></script> <?pjp //*/ ?>
<script type="text/javascript" src="<?php echo site_url() ?>assets/js/tablesort.custom.js?v=111213"></script>
<script type="text/javascript" src="<?php echo site_url() ?>assets/js/select2.min.js?v=250516"></script>
<script type="text/javascript" src="<?php echo site_url() ?>assets/js/jquery.onerror.js?v=111213"></script>
<script type="text/javascript" src="<?php echo site_url() ?>assets/js/js.cookie.js?v=300817"></script>



<script type="text/javascript" src="<?php echo site_url() ?>assets/js/custom/main.js?v=220115"></script>
<?php echo $this->pixie_javascript->script_html(); ?>

<?php echo $this->pixie_javascript->onload_html(); ?>