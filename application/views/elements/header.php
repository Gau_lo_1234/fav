
<?php
$role=$this->tank_auth->get_user_role();
?><!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Top Navigation</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/css/font.css" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

    <link rel="stylesheet" href="<?php echo site_url() ?>assets/css/morris.css" type="text/css"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script src="<?php echo site_url() ?>assets/js/jquery2.0.3.min.js"></script>
    <script src="<?php echo site_url() ?>assets/js/raphael-min.js"></script>
    <script src="<?php echo site_url() ?>assets/js/morris.js"></script>
</head>





<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="skin-blue sidebar-mini" style="background: #000000;">
<div class="wrapper">
    <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>MG</b>TC</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>MagTouch Electronics</b>TC</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->

        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <?php $segment = $this->uri->segment(1); ?>
                    <li <?php echo ($segment == 'requests' ? 'class="active"' : '') ?>><a href="<?php echo site_url('requests'); ?>">Requests Logged</a></li>
                    <li <?php echo ($segment == 'groups' ? 'class="active"' : '') ?>><a href="<?php echo site_url('groups'); ?>">Groups</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">API Management <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li <?php echo ($segment == 'API_forms' ? 'class="active"' : '') ?>><a href="<?php echo site_url('API_forms'); ?>">API Test Forms</a></li>
                            <li <?php echo ($segment == 'API_forms' ? 'class="active"' : '') ?>><a href="<?php echo site_url('api_management/keys'); ?>">API Keys</a></li>
                            <li <?php echo ($segment == 'API_forms' ? 'class="active"' : '') ?>><a href="<?php echo site_url('api_management/logs'); ?>">API Logs</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo site_url('apidoc'); ?>">API Documentation</a></li>
                </ul>
                <!-- <form class="navbar-form navbar-left" role="search">
                  <div class="form-group">
                    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                  </div>
                </form> -->
            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-history"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header"> Last Login : <i class="fa fa-clock-o"></i> <?= empty($last_login) ? "First Time Login" : $last_login; ?></li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>
                            <span class="hidden-xs"><?php echo $this->tank_auth->get_username() ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image" />
                                <p>
                                    <?php echo $this->tank_auth->get_username() ?>
                                    <small><?php //echo $role_text; ?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?php echo base_url(); ?>loadChangePass" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Change Password</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-custom-menu -->
    </header>

<!-- /.container-fluid -->
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar" style="background: #000000;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"  style="background: #000000;">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu"  style="background: #000000;">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="<?php echo base_url(); ?>dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
                </a>
            </li>
            <li class="treeview">
                <a href="<?php echo base_url(); ?>reports">
                    <i class="fa fa-plane"></i>
                    <span>Reports</span>
                </a>
            </li>
            <?php
            if($role == ROLE_ADMIN) { ?>
                <li class="treeview">
                    <a href="<?php echo base_url(); ?>api_management/keys">
                        <i class="fa fa-lock"></i>
                        <span>API Keys</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="<?php echo base_url(); ?>distributors">
                        <i class="fa fa-ticket"></i>
                        <span>Distributors</span>
                    </a>
                </li>
                <?php
            }
            if($role == ROLE_ADMIN || $role == ROLE_DISTRIBUTOR)
            {
                ?>
                <li class="treeview">
                    <a href="<?php echo base_url(); ?>devices">
                        <i class="fa fa-thumb-tack"></i>
                        <span>Devices</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="<?php echo base_url(); ?>users" >
                        <i class="fa fa-upload"></i>
                        <span>Users</span>
                    </a>
                </li>
                <?php

            }
            if($role == ROLE_ADMIN)
            {
                ?>
                <li class="treeview">
                    <a href="<?php echo base_url(); ?>settings">
                        <i class="fa fa-users"></i>
                        <span>Settings</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="<?php echo base_url(); ?>accounts">
                        <i class="fa fa-files-o"></i>
                        <span>Accounts</span>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Full Width Column -->
<div class="content-wrapper" >
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header" >
            <h1>
                <?php echo isset($page_heading) && $page_heading ? $page_heading : '' ?>
            </h1>
        </section>


        <!-- Main content -->
        <section class="content" style="width:95%;">
            <!-- /.content -->


