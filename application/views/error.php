<!DOCTYPE html>
<html>
<head>
  <title>Error! | Online Guarding</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css" />
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/jquery_ui_themes/base/jquery.ui.all.css" />
  <script type="text/javascript" src="<?php echo ASSET_URL ?>js/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="<?php echo ASSET_URL ?>js/jquery-ui-1.9.1.custom.min.js"></script>
  <script>
  $(function () {
    $('.button').button();
  });
  </script>
</head>
<body>
  <div id="wrapper">
    <div id="header">
	<a href="/"><img src="<?php echo base_url() ?>assets/images/logo.png"/></a>
    </div><!--header-->

    <div id="master_container">
        <div id="content">
            <br class="clrflt"/>

<?php echo $error_content; ?>

            </div><!--content-->

        <br class="clrflt"/>
        <br class="clrflt"/>
    </div>
    <div id="footer">

	<p><?php echo sprintf($this->lang->line('copyright'),date('Y')); ?></p>
    </div><!--footer-->
  </div><!-- wrapper -->

</body>
</html>