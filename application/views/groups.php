<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <?php if (!empty($group_data)) { ?>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <?php
                  $first = reset($group_data);
                  $headers = array_keys($first);
                  foreach($headers as $header) {
                    echo '<th>'.$header.'</th>';
                  }
                  ?>
                </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($group_data as $data) {
                    echo '<tr>';
                    foreach($data as $field_name =>$field) {
                      if ($field_name == 'contacts') {
                        echo '<td>';
                        $contacts = json_decode($field);
                        $x = 0;
                        foreach($contacts as $c) {
                          echo ($x > 0 ? '<br>' : '').$c;
                          $x++;
                        }
                        echo '</td>';
                      } else {
                        echo '<td>'.$field.'</td>';
                      }
                    }
                    echo '</tr>';
                  }
                  ?>
                </tbody>
                <tfoot>
                <tr>
                  <?php
                  $first = reset($group_data);
                  $headers = array_keys($first);
                  foreach($headers as $header) {
                    echo '<th>'.$header.'</th>';
                  }
                  ?>
                </tr>
                </tfoot>
              </table>
              <?php } else { ?>
                  No Data.
              <?php } ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>