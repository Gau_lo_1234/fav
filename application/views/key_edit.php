<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <!-- /.box-header -->
      <div class="box-body">

        <?php echo form_open($this->uri->uri_string()); ?>

        <div class="form-group">
          <label>Level</label>
          <input type="text" class="form-control" name="level" value="<?php echo $key->level ?>">
        </div>
        <div class="form-group">
          <label>Ignore Limits</label>
          <input type="text" class="form-control" name="ignore_limits"  value="<?php echo $key->ignore_limits ?>">
        </div>
        <button type="submit" class="btn btn-info pull-right">Update</button>

        <?php echo form_close(); ?>
    </div>
    <!-- /.box -->
  </div>
</div>