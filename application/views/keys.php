<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
<!--              <a href="<?php echo  site_url('api_management/generate_key') ?>" class="btn btn-default btn-flat pull-right">Generate New Key</a>-->
                
                 <a href="<?php echo  site_url('API_forms/api_key_add') ?>" class="btn btn-default btn-flat pull-right">Generate New Key</a>
              <?php if (!empty($api_keys)) { ?>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <?php
                  $first = reset($api_keys);
                  $headers = array_keys($first);
                  foreach($headers as $header) {
                    echo '<th>'.$header.'</th>';
                  }
                  ?>
                </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($api_keys as $data) {
                    echo '<tr>';
                    foreach($data as $field_name =>$field) {
                      if ($field_name == 'date_created') {
                        echo '<td>'.date('Y-m-d H:i:s', $field).'</td>';
                      } else {
                        echo '<td>'.$field.'</td>';
                      }
                    }
                    echo '<td><a href="'.site_url('api_management/edit_key/'.$data['key']).'">Edit</a></td>';
                    echo '</tr>';
                  }
                  ?>
                </tbody>
                <tfoot>
                <tr>
                  <?php
                  $first = reset($api_keys);
                  $headers = array_keys($first);
                  foreach($headers as $header) {
                    echo '<th>'.$header.'</th>';
                  }
                  ?>
                </tr>
                </tfoot>
              </table>
              <?php } else { ?>
                  No Data.
              <?php } ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>