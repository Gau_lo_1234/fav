<?php 
if (!isset($header_data)) $header_data = array();
$this->load->view('elements/header', $header_data);

echo $content_for_layout;

$this->load->view('elements/footer');
