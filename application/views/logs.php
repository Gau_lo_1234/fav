<?php

   // disp_dump($api_logs);

?>




<div class="row">
        <div class="col-xs-12">
          <div  class="box">

            <div class="box-body" style="overflow:auto;">
              <?php if (!empty($api_logs)) { ?>
             <table id="example2" class="table table-bordered table-hover">

                <thead>
                <tr>
                  <?php
                  $first = reset($api_logs);
                  $headers = array_keys($first);
                  foreach($headers as $header) {
                    echo '<th>'.$header.'</th>';
                  }
                  ?>
                </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($api_logs as $data) {
                    echo '<tr>';
                    
                    foreach($data as $field_name =>$field) {
                      if ($field_name == 'params' || $field_name == 'response') {
                        echo '<td>';

                        $params = json_decode($field);
                        $x = 0;
                        if ($params) {
                          foreach($params as $key=>$c) {
                            if (is_array($c)) {
                              echo ($x > 0 ? '<br>' : '').'<b>'.$key.'</b>'.':';
                              foreach($c as $cc) {
                                echo ($x > 0 ? '<br>' : '').$cc;
                              }
                            } else {
                              if (is_object($c)) {
                                echo 'obj';
                              } else {
                                echo ($x > 0 ? '<br>' : '').'<b>'.$key.'</b>'.': '.$c;
                              }
                            }
                            $x++;
                          }
                        }
                        echo '</td>';
                      } else {
                        echo '<td>'.$field.'</td>';
                      }
                    }
                    echo '</tr>';
                  }
                  ?>
                </tbody>
                <tfoot>
                <tr>
                  <?php
                  $first = reset($api_logs);
                  $headers = array_keys($first);
                  foreach($headers as $header) {
                    echo '<th>'.$header.'</th>';
                  }
                  ?>
                </tr>
                </tfoot>
              </table>
              <?php } else { ?>
                  No Data.
              <?php } ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>