<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="small-box bg-blue">
        <div class="inner">
            <h3><?php echo isset($request_logs_counts) ? $request_logs_counts : 0 ?></h3>

            <p>Request Logs</p>
        </div>
        <div class="icon">
            <i class="fa fa-files-o"></i>
        </div>
        <a href="<?php echo site_url('requests'); ?>" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="small-box bg-green">
        <div class="inner">
            <h3><?php echo isset($groups_counts) ? $groups_counts : 0 ?></h3>

            <p>Groups</p>
        </div>
        <div class="icon">
            <i class="fa fa-comments-o"></i>
        </div>
        <a href="<?php echo site_url('groups'); ?>" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>

<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="small-box bg-red">
        <div class="inner">
            <h3><?php echo isset($distributor_counts) ? $distributor_counts : 0 ?></h3>

            <p>Distributors</p>
        </div>
        <div class="icon">
            <i class="fa fa-lock"></i>
        </div>
        <a href="<?php echo site_url('distributors'); ?>" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
            &nbsp;
            <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
        </a>
    </div>
</div>

<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?php echo isset($devices_counts) ? $devices_counts : 0 ?></h3>

            <p>Devices</p>
        </div>
        <div class="icon">
            <i class="fa fa-user"></i>
        </div>
        <a href="<?php echo site_url('devices'); ?>" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
            &nbsp;
            <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
        </a>
    </div>
</div>

<div class="row">
    <div class="panel-body">
        <div class="col-md-12 w3ls-graph">
        <!--agileinfo-grap-->
        <div class="agileinfo-grap">
            <div class="agileits-box">
                <header class="agileits-box-header clearfix">
                <h3>Visitor Statistics</h3>
                <div class="toolbar">


                </div>
                </header>
                <div class="agileits-box-body clearfix">
                <div id="hero-area"></div>
            </div>
        </div>
    </div>
    <!--//agileinfo-grap-->

</div>
</div>
</div>
<div class="market-updates">
    <div class="col-md-3 market-update-gd">
        <div class="small-bx bg-blue">
    <div class="col-md-4 market-update-right">
    <i class="fa fa-eye"> </i>
</div>
<div class="col-md-8 market-update-left">
<h4>Distributors</h4>
<h3>13,500</h3>
</div>
            <a href="<?php echo site_url('devices'); ?>" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
                &nbsp;
                <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
            </a>
</div>

</div>
<div class="col-md-3 market-update-gd">
    <div class="small-bx bg-blue">
<div class="col-md-4 market-update-right">
<i class="fa fa-users" ></i>
</div>
<div class="col-md-8 market-update-left">
<h4>Devices</h4>
<h3>1,250</h3>
</div>
<br>
        <a href="<?php echo site_url('devices'); ?>" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
            &nbsp;
            <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
        </a>
</div>
</div>
<div class="col-md-3 market-update-gd">
    <div class="small-bx bg-blue">
<div class="col-md-4 market-update-right">
<i class="fa fa-usd"></i>
</div>
<div class="col-md-8 market-update-left">
<h4>Users</h4>
<h3>1,500</h3>

</div>
<a href='' class=''>
    More info <i class="fa fa-arrow-circle-right"></i>
</a>
</div>
</div>
<div class="col-md-3 market-update-gd">
<div class="small-bx bg-blue">
<div class="col-md-4 market-update-right">
<i class="fa fa-shopping-cart" aria-hidden="true"></i>
</div>
<div class="col-md-8 market-update-left">
<h4>Reports</h4>
<h3>1,500</h3>

</div>
<a href='' class=''>
    More info <i class="fa fa-arrow-circle-right"></i>
</a>
</div>

</div>
    <div class="agil-info-calendar">
        <!-- calendar -->
        <div class="col-md-6 agile-calendar">
        <div class="calendar-widget">
            <div class="panel-heading ui-sortable-handle">
            <span class="panel-icon">
                      <i class="fa fa-calendar-o"></i>
                    </span>
            <span class="panel-title"> Calendar Widget</span>
        </div>
        <!-- grids -->
        <div class="agile-calendar-grid">
            <div class="page">

                <div class="w3l-calendar-left">
                    <div class="calendar-heading">

                    </div>
                    <div class="monthly" id="mycalendar"></div>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>
<!-- //calendar -->


<div class="clearfix"> </div>
</div>
    <!-- tasks -->
    <div class="agile-last-grids">
        <div class="col-md-4 agile-last-left">
        <div class="agile-last-grid">
            <div class="area-grids-heading">
                <h3>Monthly</h3>
            </div>
            <div id="graph7"></div>
            <script>
                // This crosses a DST boundary in the UK.
                Morris.Area({
                    element: 'graph7',
                    data: [
                        {x: '2013-03-30 22:00:00', y: 3, z: 3},
                        {x: '2013-03-31 00:00:00', y: 2, z: 0},
                        {x: '2013-03-31 02:00:00', y: 0, z: 2},
                        {x: '2013-03-31 04:00:00', y: 4, z: 4}
                    ],
                    xkey: 'x',
                    ykeys: ['y', 'z'],
                    labels: ['Y', 'Z']
                });
            </script>

        </div>
    </div>
    <div class="col-md-4 agile-last-left agile-last-middle">
    <div class="agile-last-grid">
        <div class="area-grids-heading">
            <h3>Daily</h3>
        </div>
        <div id="graph8"></div>
        <script>
            /* data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type */
            var day_data = [
                {"period": "2016-10-01", "licensed": 3407, "sorned": 660},
            {"period": "2016-09-30", "licensed": 3351, "sorned": 629},
                {"period": "2016-09-29", "licensed": 3269, "sorned": 618},
                    {"period": "2016-09-20", "licensed": 3246, "sorned": 661},
                        {"period": "2016-09-19", "licensed": 3257, "sorned": 667},
                            {"period": "2016-09-18", "licensed": 3248, "sorned": 627},
                                {"period": "2016-09-17", "licensed": 3171, "sorned": 660},
                                    {"period": "2016-09-16", "licensed": 3171, "sorned": 676},
                                        {"period": "2016-09-15", "licensed": 3201, "sorned": 656},
                                            {"period": "2016-09-10", "licensed": 3215, "sorned": 622}
                                            ];
                                                Morris.Bar({
                                                    element: 'graph8',
                                                    data: day_data,
                                                    xkey: 'period',
                                                    ykeys: ['licensed', 'sorned'],
                                                    labels: ['Licensed', 'SORN'],
                                                    xLabelAngle: 60
                                                });
        </script>
    </div>
</div>
<div class="col-md-4 agile-last-left agile-last-right">
<div class="agile-last-grid">
    <div class="area-grids-heading">
        <h3>Yearly</h3>
    </div>
    <div id="graph9"></div>
    <script>
        var day_data = [
            {"elapsed": "I", "value": 34},
        {"elapsed": "II", "value": 24},
            {"elapsed": "III", "value": 3},
                {"elapsed": "IV", "value": 12},
                    {"elapsed": "V", "value": 13},
                        {"elapsed": "VI", "value": 22},
                            {"elapsed": "VII", "value": 5},
                                {"elapsed": "VIII", "value": 26},
                                    {"elapsed": "IX", "value": 12},
                                        {"elapsed": "X", "value": 19}
                                        ];
                                            Morris.Line({
                                                element: 'graph9',
                                                data: day_data,
                                                xkey: 'elapsed',
                                                ykeys: ['value'],
                                                labels: ['value'],
                                                parseTime: false
                                            });
    </script>

</div>
</div>
<div class="clearfix"> </div>
</div>

    <!-- //tasks -->
    <div class="agileits-w3layouts-stats">
        <div class="col-md-4 stats-info widget">
        <div class="stats-info-agileits">
            <div class="stats-title">
                <h4 class="title">Browser Stats</h4>
            </div>
            <div class="stats-body">
                <ul class="list-unstyled">
                    <li>GoogleChrome <span class="pull-right">85%</span>
                        <div class="progress progress-striped active progress-right">
                        <div class="bar green" style="width:85%;"></div>
        </div>
        </li>
        <li>Firefox <span class="pull-right">35%</span>
            <div class="progress progress-striped active progress-right">
            <div class="bar yellow" style="width:35%;"></div>
</div>
</li>
<li>Internet Explorer <span class="pull-right">78%</span>
    <div class="progress progress-striped active progress-right">
    <div class="bar red" style="width:78%;"></div>
    </div>
</li>
<li>Safari <span class="pull-right">50%</span>
    <div class="progress progress-striped active progress-right">
    <div class="bar blue" style="width:50%;"></div>
    </div>
</li>
<li>Opera <span class="pull-right">80%</span>
    <div class="progress progress-striped active progress-right">
    <div class="bar light-blue" style="width:80%;"></div>
    </div>
</li>
<li class="last">Others <span class="pull-right">60%</span>
    <div class="progress progress-striped active progress-right">
    <div class="bar orange" style="width:60%;"></div>
    </div>
</li>
</ul>
</div>
</div>
</div>
<div class="col-md-8 stats-info stats-last widget-shadow">
<div class="stats-last-agile">
    <table class="table stats-table ">
    <thead>
    <tr>
        <th>S.NO</th>
        <th>PRODUCT</th>
        <th>STATUS</th>
        <th>PROGRESS</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">1</th>
        <td>Lorem ipsum</td>
        <td><span class="label label-success">In progress</span></td>
        <td><h5>85% <i class="fa fa-level-up"></i></h5></td>
    </tr>
    <tr>
        <th scope="row">2</th>
        <td>Aliquam</td>
        <td><span class="label label-warning">New</span></td>
        <td><h5>35% <i class="fa fa-level-up"></i></h5></td>
    </tr>
    <tr>
        <th scope="row">3</th>
        <td>Lorem ipsum</td>
        <td><span class="label label-danger">Overdue</span></td>
        <td><h5 class="down">40% <i class="fa fa-level-down"></i></h5></td>
    </tr>
    <tr>
        <th scope="row">4</th>
        <td>Aliquam</td>
        <td><span class="label label-info">Out of stock</span></td>
        <td><h5>100% <i class="fa fa-level-up"></i></h5></td>
    </tr>
    <tr>
        <th scope="row">5</th>
        <td>Lorem ipsum</td>
        <td><span class="label label-success">In progress</span></td>
        <td><h5 class="down">10% <i class="fa fa-level-down"></i></h5></td>
    </tr>
    <tr>
        <th scope="row">6</th>
        <td>Aliquam</td>
        <td><span class="label label-warning">New</span></td>
        <td><h5>38% <i class="fa fa-level-up"></i></h5></td>
    </tr>
    </tbody>
    </table>
</div>
</div>
<div class="clearfix"> </div>
</div>
    <?php echo "gaulo".site_url(); ?>