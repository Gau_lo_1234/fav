<div class="card p-2 mb-5">

    <!--Grid row-->
    <div class="row">

        <!--Grid column-->
        <div class="col-lg-3 col-md-12">



        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6">



        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6">



        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6">

            <form class="form-inline mt-2 ml-2">
                <input class="form-control my-0 py-0" type="text" placeholder="Search" style="max-width: 150px;">
                <button class="btn btn-sm btn-primary ml-2 px-1"><i class="fa fa-search"></i>  </button>
            </form>

        </div>
        <!--Grid column-->

    </div>
    <!--Grid row-->

</div>
<!--Top Table UI-->

<div class="card card-cascade narrower">

    <!--Card image-->
    <div class="view gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">


        <a href="" class="white-text mx-3">Activated modules</a>

        <div>
            <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i class="fa fa-pencil mt-0"></i></button>
            <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i class="fa fa-remove mt-0"></i></button>
            <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2"><i class="fa fa-info-circle mt-0"></i></button>
        </div>

    </div>
    <!--/Card image-->

    <div class="px-4">

        <div class="table-wrapper">
            <!--Table-->
            <table class="table table-hover mb-0">

                <!--Table head-->
                <thead>
                <tr>
                    <th>
                        <input type="checkbox" id="checkbox">
                        <label for="checkbox" class="mr-2 label-table"></label>
                    </th>
                    <th class="th-lg"><a>Module Name </a></th>
                    <th class="th-lg"><a href="">Description</a></th>

                </tr>
                </thead>
                <!--Table head-->

                <!--Table body-->
                <tbody>
                <tr>
                    <th scope="row">
                        <input type="checkbox" id="checkbox1" checked>
                        <label for="checkbox1" class="label-table"></label>
                    </th>
                    <td>Push to Talk</td>
                    <td>This feature allow employees to engage to a verbal dialog in groups</td>

                </tr>
                <tr>
                    <th scope="row">
                        <input type="checkbox" id="checkbox2" checked>
                        <label for="checkbox2" class="label-table"></label>
                    </th>
                    <td>Control room</td>
                    <td>Enabling of the Control allow a live surveilance of every device connected</td>

                </tr>
                <tr>
                    <th scope="row">
                        <input type="checkbox" id="checkbox3" checked>
                        <label for="checkbox3" class="label-table"></label>
                    </th>
                    <td>Advanced Alert</td>
                    <td>This module allows, fire, and other signals to be sent</td>

                </tr>
                <tr>
                    <th scope="row">
                        <input type="checkbox" id="checkbox4" checked>
                        <label for="checkbox4" class="label-table"></label>
                    </th>
                    <td>Armed response</td>
                    <td>This module allows emergency service to be contacted</td>

                </tr>
                <tr>
                    <th scope="row">
                        <input type="checkbox" id="checkbox5" checked>
                        <label for="checkbox5" class="label-table"></label>
                    </th>
                    <td>SMS alert</td>
                    <td>This module allows you to send customised sms to group member</td>

                </tr>
                </tbody>
                <!--Table body-->
            </table>
            <!--Table-->
        </div>

        <hr class="my-0">

        <!--Bottom Table UI-->
        <div class="d-flex justify-content-between">


            <!--Pagination -->
            <nav class="my-4">
                <ul class="pagination pagination-circle pg-blue mb-0">

                    <!--First-->
                    <li class="page-item disabled"><a class="page-link">First</a></li>

                    <!--Arrow left-->
                    <li class="page-item disabled">
                        <a class="page-link" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>

                    <!--Numbers-->
                    <li class="page-item active"><a class="page-link">1</a></li>
                    <li class="page-item"><a class="page-link">2</a></li>
                    <li class="page-item"><a class="page-link">3</a></li>
                    <li class="page-item"><a class="page-link">4</a></li>
                    <li class="page-item"><a class="page-link">5</a></li>

                    <!--Arrow right-->
                    <li class="page-item">
                        <a class="page-link" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>

                    <!--First-->
                    <li class="page-item"><a class="page-link">Last</a></li>

                </ul>
            </nav>
            <!--/Pagination -->

        </div>
        <!--Bottom Table UI-->

    </div>
</div>
