<?php
/**
* @author       Asim Zeeshan
* @modified    Elsabe Lessing (http://www.lessink.co.za/)
* @web         http://www.asim.pk/
* @date     13th May, 2009
* @copyright    No Copyrights, but please link back in any way
*/

if (!isset($_SERVER['argc'])) {
    // so a browser user won't see it...
    // guess its a bit of a hack but it works =P
    //should be blocked via apache too me thinks.
    header('Location: /index.php');
}

/*
|---------------------------------------------------------------
| CASTING argc AND argv INTO LOCAL VARIABLES
|---------------------------------------------------------------
|
*/

$argc = isset($_SERVER['argc']) ? $_SERVER['argc'] : 0;
$argv = isset($_SERVER['argv']) ? $_SERVER['argv'] : array();

// INTERPRETTING INPUT
if ($argc > 1 && isset($argv[1])) {
    $_SERVER['PATH_INFO']   = $argv[1];
    $_SERVER['REQUEST_URI'] = $argv[1];
    if (isset($argv[2])) {
        // make sure theres a slash at the end of the url given
        // Used for fetching images to be added to reports generated on the cron
        $argv[2] = rtrim($argv[2], '/') . '/';
        define('CMD_BASE_URL', $argv[2]);
    }
} else {
    $_SERVER['PATH_INFO']   = '/crons/index';
    $_SERVER['REQUEST_URI'] = '/crons/index';
}

$base_directory = dirname(__FILE__);
define('BASEDIR', $base_directory);
define('TMPDIR', $base_directory.'/tmp');

define('CRON', 1);


/*
|---------------------------------------------------------------
| PHP SCRIPT EXECUTION TIME ('0' means Unlimited)
|---------------------------------------------------------------
|
*/
set_time_limit(0);

/* the next bit is pretty much everything from index.php */
	error_reporting(E_ALL);
	$system_path = $base_directory."/system";
	$application_folder = $base_directory."/application";
	if (realpath($system_path) !== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}
	// ensure there's a trailing slash
	$system_path = rtrim($system_path, '/').'/';
	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
	// The PHP file extension
	define('EXT', '.php');
	// Path to the system folder
	define('BASEPATH', str_replace("\\", "/", $system_path));
	// Path to the front controller (this file)
	define('FCPATH', str_replace(SELF, '', __FILE__));
	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));
	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$application_folder.'/'))
		{
			exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}
		define('APPPATH', BASEPATH.$application_folder.'/');
	}

/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go... but we're using a hacked version here so that
 * the cron script can run the "protected" functions.
 *
 */
require_once BASEPATH.'core/CodeIgniterCron'.EXT;


/* End of file cron.php */
/* Location: ./cron.php */