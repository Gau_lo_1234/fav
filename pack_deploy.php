#!/usr/bin/php
<?php
/**
 * This is a script to run through a bunch of modules folders
 * contianing git repositories and packing them into tar.gz folders!
 *
 * @author Elsabe Lessing (http://www.lessink.co.za)
 */

$options = getopt("r:q:v:b:p:hc");

if (isset($options['h'])) {
    echo "\n Onlineguarding Pack and Deploy Script\n";
    echo "****************************************\n";
    echo "This script will create an archive of the latest git repositories\n";
    echo "and create compressed archives for deployment. After which\n";
    echo "it will be moved to the server using scp if a path is provided.\n";
    echo "\nUsage Examples: \n";
    echo "\tTo pack latest commit: ./".basename(__FILE__)." \n";
    echo "\tTo pack and deploy:  ./".basename(__FILE__)." -r deploy@mag-touch.com:/tmp/.\n";
    echo "\nOptions: \n";
    echo "\t-r SSH connection string and path. \n\t   Example 'user@your-domain.com:/path/on/server/.' \n";
    echo "\t-q SSH port if no 22. \n";
    echo "\t-p Build path. Default: '../' \n";
    echo "\t-v String to add to the version number.\n\t   Example: '2' with result in a version number of x.x.x-2\n";
    echo "\t-b Git branch to archive, default is master\n";
    echo "\t-c Git push to the remote origin. \n";
    echo "\n";
    exit(0);
}

$fh = fopen("version", "r");
if ($fh) {
    $version = preg_replace('/\s*/','',fgets($fh));
    $release_date = fgets($fh);
    fclose($fh);
} else {
    echo "ERROR: No version file found.\n";
    exit(1);
}
if (isset($options['v'])) {
    $version .= '-'.$options['v'];
}

$remote_path = isset($options['r']) ? $options['r'] : "";
$ssh_port = isset($options['q']) ? $options['q'] : null;

$branch = isset($options['b']) ? $options['b'] : "master";
$build_path = isset($options['p']) ? $options['p'] : "../";
$git_push = isset($options['c']);

$compress = true;
$build_name = "mach_ii";

echo "Version: ".$version."\nRelease Date: ".$release_date."\n----------\n";

$archive_name = $build_name.($version ? "_".$version : "" ).".tar";

echo "Tag build\n";
exec('git tag "v'.$version.'"');

echo "Run git archive\n";
// echo 'git archive --format=tar --output='.$build_path.$archive_name.' '.$branch."\n";
exec('git archive --format=tar --output='.$build_path.$archive_name.' '.$branch);

if (file_exists($build_path.$archive_name)) {
    echo "Created File: ".$archive_name."\n";
    if ($compress) {
        // compress it...
        // echo 'gzip -f '.$archive_name."\n";
        exec ('gzip -f '.$build_path.$archive_name);
        echo " Compressed\n";
        $archive_name .= '.gz';
    }
    if ($remote_path !== '') {
        echo "Deployment: ".$remote_path."\n";
        echo 'scp '.($ssh_port != null ? '-P '.$ssh_port.' ' : '').$build_path.$archive_name.' '.$remote_path;
        exec('scp '.($ssh_port != null ? '-P '.$ssh_port.' ' : '').$build_path.$archive_name.' '.$remote_path);
        echo " Deployed\n";
    }
} else {
    echo "ERROR: No file created.\n";
    exit(1);
}

if ($git_push) {
    echo "Git Push\n";
    exec('git push origin master');
}

echo "\nDone.\n";
